package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.adapter.TwoTextArrayAdapter;
import com.aapbd.worldrelaytune.adapter.TwoTextArrayAdapter.RowType;
import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.CategoryCountryList;
import com.aapbd.worldrelaytune.model.CategoryGenreList;
import com.aapbd.worldrelaytune.model.CategoryLanguageList;
import com.aapbd.worldrelaytune.model.CategoryList;
import com.aapbd.worldrelaytune.model.CategoryTypeInfo;
import com.aapbd.worldrelaytune.model.Item;
import com.aapbd.worldrelaytune.model.ListItem;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CategoryFlipper {

	ViewFlipper viewFlipper;
	Context con;
	Activity newActivty;

	Typeface t3;
	TextView categoryTitile;
	ListView categoryList;
	// CustomAdapter customAdapter;
	Typeface tf;
	ImageView categorySearch, settingImg;
	Dialog dd;
	EditText et_search;
	Typeface tf3;
	BusyDialog busy;
	String result = "";
	List<CategoryList> categoryTypeList;
	List<CategoryGenreList> categoryGenreList;
	List<CategoryCountryList> categoryCountryLists;
	List<CategoryLanguageList> categoryLanguageList;
	DatabaseHandler db;
	int position = 0;

	public CategoryFlipper(ViewFlipper viewFlipper, Context con,
			Activity newActivty) {

		this.viewFlipper = viewFlipper;
		this.con = con;
		this.newActivty = newActivty;
		viewFlipper.setDisplayedChild(1);
		db = new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(newActivty, "Categories_all", "");
		AnalyticsTracker.sendEventData(newActivty, "Categories_all" , "action", "label");
//		MyApplication.getInstance().trackEvent("Categories", "", "");
		initUi();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		categorySearch = (ImageView) newActivty.findViewById(R.id.categorySearch);
		settingImg = (ImageView) newActivty.findViewById(R.id.settingImg);
		categoryList = (ListView) newActivty.findViewById(R.id.categoryList);
		categoryTitile = (TextView) newActivty.findViewById(R.id.categoryTitile);
		categoryTitile.setText("Categories");
		tf = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Bold.ttf");
		categoryTitile.setTypeface(tf);
		tf3 = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Light.ttf");
		// DummyData();

		getCategoryTypeData(AllURL.getCategoryType());

		// publicChannelBtn.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// AppConstant.categoryType="All";
		// AppConstant.categoryList=db.getAllChannelInfo();
		// new CategoryItemFlipper(viewFlipper, con, newActivty);
		// }
		// });

		categorySearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchDialoge();
			}
		});
		settingImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartActivity.toActivity(con, SettingsActivity.class);
			}
		});
		// categoryType.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// AppConstant.categoryType="Type";
		// AppConstant.clickCategoryType=true;
		// new CategoryListFlipper(viewFlipper, con, newActivty);
		// }
		// });

	}

	private void getCategoryTypeData(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + result);

					Gson g = new Gson();
					Type fooType = new TypeToken<ArrayList<CategoryList>>() {
					}.getType();
					categoryTypeList = g.fromJson(new String(result), fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				newActivty.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						// if (busy != null) {
						// busy.dismis();
						// }

						if (categoryTypeList.get(0).getValues().size() > 0) {
							AppConstant.categoryTypeList = categoryTypeList
									.get(0).getValues();

							// customAdapter=new CustomAdapter(con);
							// categoryList.setAdapter(customAdapter);
							// customAdapter.notifyDataSetChanged();

							getGenreData(AllURL.getGenre());

						} else {
							AlertMessage.showMessage(con, "Status",
									"No Category Found");
						}

					}

				});
			}
		});

		thread.start();

	}

	private void getGenreData(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		// busy = new BusyDialog(con, true,false);
		// busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + result);

					Gson g = new Gson();
					Type fooType = new TypeToken<ArrayList<CategoryGenreList>>() {
					}.getType();
					categoryGenreList = g.fromJson(new String(result), fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				newActivty.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						// if (busy != null) {
						// busy.dismis();
						// }

						if (categoryGenreList.get(0).getValues().size() > 0) {
							AppConstant.categoryGenreList = categoryGenreList
									.get(0).getValues();
							getCountryData(AllURL.getCountries());

						} else {
							AlertMessage.showMessage(con, "Status",
									"No Category Found");
						}

					}

				});
			}
		});

		thread.start();

	}

	private void getCountryData(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		// busy = new BusyDialog(con, true,false);
		// busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + result);

					Gson g = new Gson();
					Type fooType = new TypeToken<ArrayList<CategoryCountryList>>() {
					}.getType();
					categoryCountryLists = g.fromJson(new String(result),
							fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				newActivty.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						// if (busy != null) {
						// busy.dismis();
						// }

						if (categoryCountryLists.get(0).getValues().size() > 0) {
							AppConstant.categoryCountryList = categoryCountryLists
									.get(0).getValues();
							getLanguageData(AllURL.getLanguage());

						} else {
							AlertMessage.showMessage(con, "Status",
									"No Category Found");
						}

					}

				});
			}
		});

		thread.start();

	}

	private void getLanguageData(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		// busy = new BusyDialog(con, true,false);
		// busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + result);

					Gson g = new Gson();
					Type fooType = new TypeToken<ArrayList<CategoryLanguageList>>() {
					}.getType();
					categoryLanguageList = g.fromJson(new String(result),
							fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				newActivty.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (categoryLanguageList.get(0).getValues().size() > 0) {
							AppConstant.categoryLanguageList = categoryLanguageList.get(0).getValues();
							List<Item> items = new ArrayList<Item>();
							items.add(new Header("ALL CHANNELS", "", con, viewFlipper, newActivty));
							CategoryTypeInfo info = new CategoryTypeInfo();
							info.setTitle("All Channels");
							info.setType("ALL CHANNELS");
							items.add(new ListItem(info, con, viewFlipper, newActivty));
							items.add(new Header("TYPES", "", con, viewFlipper, newActivty));
							int i = 0;
							for (CategoryTypeInfo pinfo : categoryTypeList.get(0).getValues()) {
								pinfo.setType("TYPES");
								items.add(new ListItem(pinfo, con, viewFlipper, newActivty));
								i++;
								if (i == 4) {
									i = 0;
									break;
								}
							}
							items.add(new Header("MORE", "TYPES", con, viewFlipper, newActivty));
							items.add(new Header("GENRES", "", con, viewFlipper, newActivty));
							for (CategoryTypeInfo pinfo : categoryGenreList.get(0).getValues()) {
								pinfo.setType("GENRES");
								items.add(new ListItem(pinfo, con, viewFlipper, newActivty));
								i++;
								if (i == 4) {
									i = 0;
									break;
								}
							}
							items.add(new Header("MORE", "GENRES", con, viewFlipper, newActivty));

							items.add(new Header("LANGUAGES", "", con, viewFlipper, newActivty));
							for (CategoryTypeInfo pinfo : categoryLanguageList.get(0).getValues()) {
								pinfo.setType("LANGUAGES");
								items.add(new ListItem(pinfo, con, viewFlipper,
										newActivty));
								i++;
								if (i == 4) {
									i = 0;
									break;
								}
							}
							items.add(new Header("MORE", "LANGUAGES", con, viewFlipper, newActivty));
							items.add(new Header("COUNTRIES", "", con, viewFlipper, newActivty));
							for (CategoryTypeInfo pinfo : categoryCountryLists.get(0).getValues()) {
								pinfo.setType("COUNTRIES");
								items.add(new ListItem(pinfo, con, viewFlipper, newActivty));
								i++;
								if (i == 4) {
									i = 0;
									break;
								}
							}
							items.add(new Header("MORE", "COUNTRIES", con, viewFlipper, newActivty));
							TwoTextArrayAdapter adapter = new TwoTextArrayAdapter(con, items);
							// ((ListActivity)
							// newActivty).setListAdapter(adapter);
							categoryList.setAdapter(adapter);
							adapter.notifyDataSetChanged();

						} else {
							AlertMessage.showMessage(con, "Status",
									"No Category Found");
						}

					}

				});
			}
		});

		thread.start();

	}

	public void searchDialoge() {
		dd = new Dialog(con);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.search_dialoge_cancel_search);
		dd.show();

		et_search = (EditText) dd.findViewById(R.id.et_search);
		ViewGroup btn_cancel = (ViewGroup) dd.findViewById(R.id.btn_cancel);
		ViewGroup btn_search = (ViewGroup) dd.findViewById(R.id.btn_search);
		TextView btn_sign = (TextView) dd.findViewById(R.id.btn_sign);
		TextView searchTv = (TextView) dd.findViewById(R.id.searchTv);

		btn_sign.setTypeface(tf3);
		et_search.setTypeface(tf3);
		searchTv.setTypeface(tf3);
		btn_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (AppConstant.searchList != null) {
					AppConstant.searchList.clear();
				}
				String searchTXT = "";
				searchTXT = et_search.getText().toString();
				Log.e("searchTXT", ">>" + searchTXT);
				if (TextUtils.isEmpty(searchTXT)) {
					StartActivity.toActivity(con, SearchResultActivity.class);
					// AlertMessage.showMessage(con,
					// getString(R.string.app_name),
					// "Please enter channel name or number");
					dd.dismiss();
				} else {
					AppConstant.searchList = db.getAllSearchData(searchTXT);
					Log.e("searchVector Size",
							">>" + AppConstant.searchList.size());

					if (AppConstant.searchList.size() > 0) {
						StartActivity.toActivity(con,
								SearchResultActivity.class);
					} else {

						AlertMessage
								.showMessage(con, "Nothing Found",
										"The search did not find any matching channels.");
					}

					dd.dismiss();
				}
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dd.dismiss();
			}
		});

	}

	private class Header implements Item {
		public String name;
		public Context con;
		DatabaseHandler db;
		ViewFlipper viewFlipper;
		Activity newActivity;
		String type = "";

		public Header(String name, String headerType, Context con,
				ViewFlipper viewFlipper, Activity newActivity) {
			this.name = name;
			this.con = con;
			this.newActivity = newActivity;
			this.viewFlipper = viewFlipper;
			this.type = headerType;
			db = new DatabaseHandler(con);
		}

		@Override
		public int getViewType() {
			return RowType.HEADER_ITEM.ordinal();
		}

		@Override
		public View getView(LayoutInflater inflater, View convertView,
				final int pos) {
			View view;
			if (convertView == null) {
				view = inflater.inflate(R.layout.header, null);
				// Do some initialization
			} else {
				view = convertView;
			}

			TextView text = (TextView) view.findViewById(R.id.separator);
			text.setText(name);
			if (name.equalsIgnoreCase("MORE")) {
				text.setBackgroundResource(R.drawable.more);
			} else {
				text.setBackgroundResource(R.drawable.white_bar_fat);
			}
			Log.e("name", ">> " + name);
			Log.e("type", ">> " + type);
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					position = pos;
					Log.e("name", ">> " + name);
					if (name.equalsIgnoreCase("ALL CHANNELS")) {
						AppConstant.categoryType = "All Channels";
						AppConstant.tempPublicList = db
								.getAllPublicChannelInfo();
						new CategoryItemFlipper(viewFlipper, con, newActivity);
					} else if (name.equalsIgnoreCase("TYPES")) {
						AppConstant.typeCheck = "TYPES";
						AppConstant.categoryType = "TYPES";
						new CategoryListFlipper(viewFlipper, con, newActivity);
					} else if (name.equalsIgnoreCase("GENRES")) {
						AppConstant.typeCheck = "GENRES";
						AppConstant.categoryType = "GENRES";
						new CategoryListFlipper(viewFlipper, con, newActivity);

					} else if (name.equalsIgnoreCase("COUNTRIES")) {
						AppConstant.typeCheck = "COUNTRIES";
						AppConstant.categoryType = "COUNTRIES";
						new CategoryListFlipper(viewFlipper, con, newActivity);
					} else if (name.equalsIgnoreCase("LANGUAGES")) {
						AppConstant.typeCheck = "LANGUAGES";
						AppConstant.categoryType = "LANGUAGES";
						new CategoryListFlipper(viewFlipper, con, newActivity);
					} else if (name.equalsIgnoreCase("MORE")) {
//						List<Item> temp = arrangeList(type);
//						TwoTextArrayAdapter adapter = new TwoTextArrayAdapter(
//								con, temp);
//						categoryList.setAdapter(adapter);
//						adapter.notifyDataSetChanged();
//						categoryList.setSelection(position);
						if (type.equalsIgnoreCase("TYPES")) {

							AppConstant.typeCheck = "TYPES";
							AppConstant.categoryType = "TYPES";
						} else if (type.equalsIgnoreCase("GENRES")) {
							AppConstant.typeCheck = "GENRES";
							AppConstant.categoryType = "GENRES";

						} else if (type.equalsIgnoreCase("COUNTRIES")) {
							AppConstant.typeCheck = "COUNTRIES";
							AppConstant.categoryType = "COUNTRIES";

						} else if (type.equalsIgnoreCase("LANGUAGES")) {
							AppConstant.typeCheck = "LANGUAGES";
							AppConstant.categoryType = "LANGUAGES";
						}

						new CategoryListFlipper(viewFlipper, con, newActivity);
					}
				}
			});

			return view;
		}

		public List<Item> arrangeList(String type) {

			List<Item> items = new ArrayList<Item>();
			if (items != null) {
				items.clear();
			}
			if (type.equalsIgnoreCase("TYPES")) {
				items.add(new Header("ALL CHANNELS", "", con, viewFlipper,
						newActivity));
				CategoryTypeInfo info = AppConstant.categoryTypeList.get(0);
				info.setType("ALL CHANNELS");
				items.add(new ListItem(info, con, viewFlipper, newActivity));
				AppConstant.categoryTypeList.remove(0);
				items.add(new Header("TYPES", "", con, viewFlipper, newActivity));
				int i = 0;
				for (CategoryTypeInfo pinfo : AppConstant.categoryTypeList) {
					pinfo.setType("TYPES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));

				}
				items.add(new Header("GENRES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryGenreList) {
					pinfo.setType("GENRES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "GENRES", con, viewFlipper,
						newActivity));
				items.add(new Header("COUNTRIES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryCountryList) {
					pinfo.setType("COUNTRIES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "COUNTRIES", con, viewFlipper,
						newActivity));
				items.add(new Header("LANGUAGES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryLanguageList) {
					pinfo.setType("LANGUAGES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "LANGUAGES", con, viewFlipper,
						newActivity));

			} else if (type.equalsIgnoreCase("GENRES")) {
				items.add(new Header("ALL CHANNELS", "", con, viewFlipper,
						newActivity));
				CategoryTypeInfo info = AppConstant.categoryTypeList.get(0);
				info.setType("ALL CHANNELS");
				items.add(new ListItem(info, con, viewFlipper, newActivity));
				AppConstant.categoryTypeList.remove(0);
				items.add(new Header("TYPES", "TYPES", con, viewFlipper,
						newActivity));
				int i = 0;
				for (CategoryTypeInfo pinfo : AppConstant.categoryTypeList) {
					pinfo.setType("TYPES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "TYPES", con, viewFlipper,
						newActivity));
				items.add(new Header("GENRES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryGenreList) {
					pinfo.setType("GENRES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));

				}
				items.add(new Header("COUNTRIES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryCountryList) {
					pinfo.setType("COUNTRIES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "COUNTRIES", con, viewFlipper,
						newActivity));
				items.add(new Header("LANGUAGES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryLanguageList) {
					pinfo.setType("LANGUAGES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "LANGUAGES", con, viewFlipper,
						newActivity));

			} else if (type.equalsIgnoreCase("COUNTRIES")) {
				items.add(new Header("ALL CHANNELS", "", con, viewFlipper,
						newActivity));
				CategoryTypeInfo info = AppConstant.categoryTypeList.get(0);
				info.setType("ALL CHANNELS");
				items.add(new ListItem(info, con, viewFlipper, newActivity));
				AppConstant.categoryTypeList.remove(0);
				items.add(new Header("TYPES", "", con, viewFlipper, newActivity));
				int i = 0;
				for (CategoryTypeInfo pinfo : AppConstant.categoryTypeList) {
					pinfo.setType("TYPES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "TYPES", con, viewFlipper,
						newActivity));
				items.add(new Header("GENRES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryGenreList) {
					pinfo.setType("GENRES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "GENRES", con, viewFlipper,
						newActivity));
				items.add(new Header("COUNTRIES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryCountryList) {
					pinfo.setType("COUNTRIES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));

				}
				items.add(new Header("LANGUAGES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryLanguageList) {
					pinfo.setType("LANGUAGES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "LANGUAGES", con, viewFlipper,
						newActivity));

			} else if (type.equalsIgnoreCase("LANGUAGES")) {
				items.add(new Header("ALL CHANNELS", "", con, viewFlipper,
						newActivity));
				CategoryTypeInfo info = AppConstant.categoryTypeList.get(0);
				info.setType("ALL CHANNELS");
				items.add(new ListItem(info, con, viewFlipper, newActivity));
				AppConstant.categoryTypeList.remove(0);
				items.add(new Header("TYPES", "", con, viewFlipper, newActivity));
				int i = 0;
				for (CategoryTypeInfo pinfo : AppConstant.categoryTypeList) {
					pinfo.setType("TYPES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORES", "TYPE", con, viewFlipper,
						newActivity));
				items.add(new Header("GENRES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryGenreList) {
					pinfo.setType("GENRES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "GENRES", con, viewFlipper,
						newActivity));
				items.add(new Header("COUNTRIES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryCountryList) {
					pinfo.setType("COUNTRIES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
					i++;
					if (i == 4) {
						i = 0;
						break;
					}
				}
				items.add(new Header("MORE", "COUNTRIES", con, viewFlipper,
						newActivity));
				items.add(new Header("LANGUAGES", "", con, viewFlipper,
						newActivity));
				for (CategoryTypeInfo pinfo : AppConstant.categoryLanguageList) {
					pinfo.setType("LANGUAGES");
					items.add(new ListItem(pinfo, con, viewFlipper, newActivity));
				}

			}

			return items;

		}

	}

}
