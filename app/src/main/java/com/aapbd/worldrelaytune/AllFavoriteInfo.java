package com.aapbd.worldrelaytune;

import java.util.Vector;

import com.aapbd.worldrelaytune.model.Favorite;

public class AllFavoriteInfo {

	public static Vector<Favorite> allFavoriteInfo = new Vector<Favorite>();

	public static Vector<Favorite> getAllFavoriteInfo() {
		return allFavoriteInfo;
	}

	public static void setAllFavoriteInfo(Vector<Favorite> allFavoriteInfo) {
		AllFavoriteInfo.allFavoriteInfo = allFavoriteInfo;
	}

	public static Favorite getFavoriteInfo(int pos) {
		return allFavoriteInfo.elementAt(pos);
	}

	public static void setFavoriteInfo(Favorite _allFavoriteInfo) {
		AllFavoriteInfo.allFavoriteInfo.addElement(_allFavoriteInfo);
	}

	public static void removeAllFavoriteInfo() {
		AllFavoriteInfo.allFavoriteInfo.removeAllElements();
	}

}
