package com.aapbd.worldrelaytune;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.PersistData;

public class MyAccountActivity extends Activity implements OnClickListener {
	Context con;

	TextView accountname, accountid;
	Typeface tf3;
	LinearLayout accountBack;
TextView accountTitle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.account_dialogue);

		con = this;
		//AnalyticsTracker.sendTrackData(this, R.string.app_name);
		initUi();
	}

	private void initUi() {
		// TODO Auto-generated method stub
		accountTitle=(TextView)findViewById(R.id.accountTitle);
		accountTitle.setText(R.string.myaccount);
		accountBack=(LinearLayout)findViewById(R.id.accountBack);
		accountBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		accountname = (TextView) findViewById(R.id.accountname);
		accountid = (TextView) findViewById(R.id.accountid);

		accountid.setText(PersistData.getStringData(con, AppConstant.urerId));
		accountname.setText(PersistData.getStringData(con,
				AppConstant.user_name));

		// Typeface tf = Typeface.createFromAsset(getAssets(),
		// "fonts/OpenSans-Bold.ttf");
		Typeface tf2 = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		accountname.setTypeface(tf2);
		accountid.setTypeface(tf2);
		//actionBarSetup();

	}

	private void actionBarSetup() {
		// TODO Auto-generated method stub

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.login_top);

		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
				R.id.signtitle);
		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
				R.id.backImg);
		signtitle.setText(R.string.myaccount);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");

		signtitle.setTypeface(tf);

		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
