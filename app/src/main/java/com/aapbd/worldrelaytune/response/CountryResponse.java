package com.aapbd.worldrelaytune.response;

import java.util.ArrayList;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllCountryInfo;

public class CountryResponse {

	public List<AllCountryInfo> posts = new ArrayList<AllCountryInfo>();

	public List<AllCountryInfo> getPosts() {
		return posts;
	}

	public void setPosts(List<AllCountryInfo> posts) {
		this.posts = posts;
	}

}
