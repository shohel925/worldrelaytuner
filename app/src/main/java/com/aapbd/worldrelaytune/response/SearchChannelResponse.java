package com.aapbd.worldrelaytune.response;

import java.util.ArrayList;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllSearchChannelTypeInfo;

public class SearchChannelResponse {

	public List<AllSearchChannelTypeInfo> posts = new ArrayList<AllSearchChannelTypeInfo>();

	public List<AllSearchChannelTypeInfo> getPosts() {
		return posts;
	}

	public void setPosts(List<AllSearchChannelTypeInfo> posts) {
		this.posts = posts;
	}

}
