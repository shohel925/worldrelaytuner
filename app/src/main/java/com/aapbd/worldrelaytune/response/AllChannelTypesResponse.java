package com.aapbd.worldrelaytune.response;

import java.util.ArrayList;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllChannelTypesInfo;

public class AllChannelTypesResponse {

	public List<AllChannelTypesInfo> posts = new ArrayList<AllChannelTypesInfo>();

	public List<AllChannelTypesInfo> getPosts() {
		return posts;
	}

	public void setPosts(List<AllChannelTypesInfo> posts) {
		this.posts = posts;
	}

}
