package com.aapbd.worldrelaytune.response;

import java.util.ArrayList;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllFavoritesInfo;

public class AllFavoritesResponse {

	public List<AllFavoritesInfo> posts = new ArrayList<AllFavoritesInfo>();

	public List<AllFavoritesInfo> getPosts() {
		return posts;
	}

	public void setPosts(List<AllFavoritesInfo> posts) {
		this.posts = posts;
	}

}
