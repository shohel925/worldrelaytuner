package com.aapbd.worldrelaytune.response;

import java.util.ArrayList;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllChannelGeneInfo;

public class AllChannelGenerResponse {

	public List<AllChannelGeneInfo> posts = new ArrayList<AllChannelGeneInfo>();

	public List<AllChannelGeneInfo> getPosts() {
		return posts;
	}

	public void setPosts(List<AllChannelGeneInfo> posts) {
		this.posts = posts;
	}

}
