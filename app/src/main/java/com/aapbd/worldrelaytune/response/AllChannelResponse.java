package com.aapbd.worldrelaytune.response;

import java.util.ArrayList;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllChannelInfo;

public class AllChannelResponse {

	public List<AllChannelInfo> posts = new ArrayList<AllChannelInfo>();

	public List<AllChannelInfo> getPosts() {
		return posts;
	}

	public void setPosts(List<AllChannelInfo> posts) {
		this.posts = posts;
	}

}
