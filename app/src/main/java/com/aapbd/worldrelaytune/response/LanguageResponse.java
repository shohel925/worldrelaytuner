package com.aapbd.worldrelaytune.response;

import java.util.ArrayList;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllLanguageInfo;

public class LanguageResponse {

	public List<AllLanguageInfo> posts = new ArrayList<AllLanguageInfo>();

	public List<AllLanguageInfo> getPosts() {
		return posts;
	}

	public void setPosts(List<AllLanguageInfo> posts) {
		this.posts = posts;
	}

}
