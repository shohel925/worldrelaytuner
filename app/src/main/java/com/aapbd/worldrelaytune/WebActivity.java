package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.aapbd.worldrelaytune.utils.AppConstant;

public class WebActivity extends Activity implements OnClickListener {

	Context con;
	WebView webview;
	ProgressBar progress;

	LinearLayout profileBtn;
	LinearLayout link_right_side_manu;
	LinearLayout webBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.web);

		con = this;

		initUi();
	}

	private void initUi() {
		// TODO Auto-generated method stub
		webBack = (LinearLayout) findViewById(R.id.webBack);
		webview = (WebView) findViewById(R.id.webview);
		progress = (ProgressBar) findViewById(R.id.progress);
		webBack.setOnClickListener(this);

		getWebView(AppConstant.webUrl);

		// //---- For Profile Info-------------------
	}

	public void getWebView(String url) {

		final CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
		CookieSyncManager.createInstance(this);

		// homebtn=(Button)findViewById(R.id.homebtn);
		webview = (WebView) findViewById(R.id.webview);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);
		webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		webview.getSettings().setDomStorageEnabled(true);
		// webview.getSettings().setUserAgentString("silly_that_i_have_to_do_this");
		// webview.loadUrl("http://www.horoscop.co.il/app/n/get_new1.php?id=1&type=3");

		if (AppConstant.webUrl != "") {
			webview.loadUrl(AppConstant.webUrl);
		}

		webview.setWebViewClient(new WebViewClient() {

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				// Toast.makeText(activity, "Oh no! " + description,
				// Toast.LENGTH_LONG).show();
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				progress.setVisibility(View.VISIBLE);

				// Toast.makeText(con, "page start URL : "+url, 2000).show();
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progress.setVisibility(View.INVISIBLE);

				AppConstant.webUrl = url;

				Log.d("Finish URL  ", ": " + url);

			}

			/*
			 * (non-Javadoc)
			 *
			 * @see
			 * android.webkit.WebViewClient#shouldOverrideUrlLoading(android
			 * .webkit.WebView, java.lang.String)
			 */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				CookieSyncManager.getInstance().sync();

				AppConstant.webUrl = url;

				Log.d("WebApp", "URL is:" + url);
				// Toast.makeText(con, "URL : "+url, 2000).show();
				// homebtn.setBackgroundResource(R.drawable.backbtn);

				if (url.startsWith("tel:")) {

					Log.d("Where : ", "Tel");

					final Intent intent = new Intent(Intent.ACTION_DIAL, Uri
							.parse(url));
					startActivity(intent);
					return true;
				}

				view.loadUrl(url);
				return true;
			}

		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.getId() == R.id.webBack) {
			finish();
		}

	}

}
