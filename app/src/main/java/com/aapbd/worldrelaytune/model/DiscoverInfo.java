package com.aapbd.worldrelaytune.model;

import java.util.ArrayList;
import java.util.List;

public class DiscoverInfo {
	String Title = "";
	String Image = "";
	List<String> Channels = new ArrayList<String>();

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

	public List<String> getChannels() {
		return Channels;
	}

	public void setChannels(List<String> channels) {
		Channels = channels;
	}

}
