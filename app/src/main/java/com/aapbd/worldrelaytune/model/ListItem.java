package com.aapbd.worldrelaytune.model;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.CategoryItemFlipper;
import com.aapbd.worldrelaytune.R;
import com.aapbd.worldrelaytune.adapter.TwoTextArrayAdapter.RowType;
import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.squareup.picasso.Picasso;

public class ListItem implements Item {
	private CategoryTypeInfo publicInfo = null;
	Context con;
	ViewFlipper viewFlipper;
	Activity newActivity;
	DatabaseHandler db;

	public ListItem(CategoryTypeInfo publicInfo, Context con,
			ViewFlipper viewFlipper, Activity newActivity) {
		this.publicInfo = publicInfo;
		this.con = con;
		this.viewFlipper = viewFlipper;
		this.newActivity = newActivity;
		db = new DatabaseHandler(con);
	}

	@Override
	public int getViewType() {
		return RowType.LIST_ITEM.ordinal();
	}

	@Override
	public View getView(LayoutInflater inflater, View convertView, int pos) {
		View view;
		if (convertView == null) {
			view = inflater.inflate(R.layout.category_rows, null);
			// Do some initialization
		} else {
			view = convertView;
		}

		TextView categoryChannelName = (TextView) view.findViewById(R.id.categoryChannelName);
		ImageView category_row_img = (ImageView) view.findViewById(R.id.category_row_img);
		categoryChannelName.setText(publicInfo.getTitle());
		if(publicInfo.getTitle().equalsIgnoreCase("All Channels")){
			category_row_img.setImageResource(R.drawable.mig);
		}else{
			Picasso.with(con).load(publicInfo.getImage()).error(R.drawable.default_img).into(category_row_img);
		}

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("index", "" + publicInfo.getIndex());
				Log.e("type", "" + publicInfo.getType());
				if (publicInfo.getType().equalsIgnoreCase("ALL CHANNELS")) {

					AppConstant.categoryType = "All Channels";
					AppConstant.tempPublicList = db.getAllPublicChannelInfo();
					new CategoryItemFlipper(viewFlipper, con, newActivity);

				} else if (publicInfo.getType().equalsIgnoreCase("TYPES")) {
					AppConstant.categoryType = publicInfo.getTitle();
					AppConstant.categoryList = db
							.getAllChannelByType(publicInfo.getIndex());
					if (AppConstant.categoryList.size() > 0) {

						new CategoryItemFlipper(viewFlipper, con, newActivity);

					} else {
						AlertMessage.showMessage(con,
								con.getString(R.string.app_name),
								con.getString(R.string.noContentYet));
					}

				} else if (publicInfo.getType().equalsIgnoreCase("GENRES")) {
					AppConstant.categoryType = publicInfo.getTitle();
					AppConstant.categoryList = db
							.getAllChannelByGenre(publicInfo.getIndex());
					if (AppConstant.categoryList.size() > 0) {

						new CategoryItemFlipper(viewFlipper, con, newActivity);

					} else {
						AlertMessage.showMessage(con,
								con.getString(R.string.app_name),
								con.getString(R.string.noContentYet));
					}

				} else if (publicInfo.getType().equalsIgnoreCase("COUNTRIES")) {
					AppConstant.categoryType = publicInfo.getTitle();
					AppConstant.categoryList = db
							.getAllChannelByCountry(publicInfo.getTitle());
					if (AppConstant.categoryList.size() > 0) {

						new CategoryItemFlipper(viewFlipper, con, newActivity);

					} else {
						AlertMessage.showMessage(con,
								con.getString(R.string.app_name),
								con.getString(R.string.noContentYet));
					}

				} else if (publicInfo.getType().equalsIgnoreCase("LANGUAGES")) {
					AppConstant.categoryType = publicInfo.getTitle();
					AppConstant.categoryList = db
							.getAllChannelByLanguage(publicInfo.getTitle());
					if (AppConstant.categoryList.size() > 0) {

						new CategoryItemFlipper(viewFlipper, con, newActivity);

					} else {
						AlertMessage.showMessage(con,
								con.getString(R.string.app_name),
								con.getString(R.string.noContentYet));
					}

				}

			}
		});
		return view;
	}

}
