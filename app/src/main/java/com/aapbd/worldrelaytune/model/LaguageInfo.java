package com.aapbd.worldrelaytune.model;

public class LaguageInfo {
	public String id = "";
	public String language = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
