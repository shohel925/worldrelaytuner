package com.aapbd.worldrelaytune.model;

import java.util.ArrayList;
import java.util.List;

public class YoutubesResponse {

	List<YoutubeSnippet> items = new ArrayList<YoutubeSnippet>();

	public List<YoutubeSnippet> getItems() {
		return items;
	}

	public void setItems(List<YoutubeSnippet> items) {
		this.items = items;
	}

}
