package com.aapbd.worldrelaytune.model;

public class TunerInfo {
	public String id = "";
	public String channerlName = "";
	public String channerlImage = "";
	public String channerlNumber = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChannerlName() {
		return channerlName;
	}

	public void setChannerlName(String channerlName) {
		this.channerlName = channerlName;
	}

	public String getChannerlImage() {
		return channerlImage;
	}

	public void setChannerlImage(String channerlImage) {
		this.channerlImage = channerlImage;
	}

	public String getChannerlNumber() {
		return channerlNumber;
	}

	public void setChannerlNumber(String channerlNumber) {
		this.channerlNumber = channerlNumber;
	}

}
