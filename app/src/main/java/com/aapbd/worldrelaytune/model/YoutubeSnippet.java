package com.aapbd.worldrelaytune.model;

public class YoutubeSnippet {
	SnippetInfo snippet = new SnippetInfo();

	public SnippetInfo getSnippet() {
		return snippet;
	}

	public void setSnippet(SnippetInfo snippet) {
		this.snippet = snippet;
	}

}
