package com.aapbd.worldrelaytune.model;

public class FavoritesInfo {
	public String channel_id = "";
	public String user_id = "";
	public String long_desc = "";
	public String description = "";
	public String language = "";
	public String channel_name = "";
	public String country = "";
	public String icon_image = "";
	public String channel_number = "";
	public String genre = "";
	public String type = "";
	public String id = "";

	public String website_link = "";

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getLong_desc() {
		return long_desc;
	}

	public void setLong_desc(String long_desc) {
		this.long_desc = long_desc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIcon_image() {
		return icon_image;
	}

	public void setIcon_image(String icon_image) {
		this.icon_image = icon_image;
	}

	public String getChannel_number() {
		return channel_number;
	}

	public void setChannel_number(String channel_number) {
		this.channel_number = channel_number;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWebsite_link() {
		return website_link;
	}

	public void setWebsite_link(String website_link) {
		this.website_link = website_link;
	}

}
