package com.aapbd.worldrelaytune.model;

public class VimeoInfo {
	String id = "";
	String title = "";
	String description = "";
	String upload_date = "";
	String thumbnail_medium = "";
	String user_name = "";
	String duration = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUpload_date() {
		return upload_date;
	}

	public void setUpload_date(String upload_date) {
		this.upload_date = upload_date;
	}

	public String getThumbnail_medium() {
		return thumbnail_medium;
	}

	public void setThumbnail_medium(String thumbnail_medium) {
		this.thumbnail_medium = thumbnail_medium;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
