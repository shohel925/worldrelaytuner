package com.aapbd.worldrelaytune.model;

public class SnippetInfo {

	YoutubesImage thumbnails = new YoutubesImage();
	public String title = "";
	public String channelTitle = "";
	YoutubesVideoId resourceId = new YoutubesVideoId();

	public String getChannelTitle() {
		return channelTitle;
	}

	public void setChannelTitle(String channelTitle) {
		this.channelTitle = channelTitle;
	}

	public YoutubesVideoId getResourceId() {
		return resourceId;
	}

	public void setResourceId(YoutubesVideoId resourceId) {
		this.resourceId = resourceId;
	}

	public YoutubesImage getThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(YoutubesImage thumbnails) {
		this.thumbnails = thumbnails;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
