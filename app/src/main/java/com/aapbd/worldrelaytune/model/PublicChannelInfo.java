package com.aapbd.worldrelaytune.model;

public class PublicChannelInfo {

	String provider_id = "";
	String countrycode = "";
	String tag1 = "";
	String tag2 = "";
	String content_focus = "";
	String visual_content_format = "";
	String visual_content_type = "";
	String stream_vod_url = "";
	String channel_format = "";
	String visual_content_location = "";
	String type = "";
	String audio_location = "";
	String stream_type = "";
	String active = "";
	String stream_vod_user = "";
	String genre0 = "";
	String subscription_plan = "";
	String wrid = "";
	String name = "";
	String language = "";
	String url = "";
	String stream_vod_password = "";
	String audio_location_2 = "";
	String visual_content_location_2 = "";
	String channel_info = "";
	String channel_image_file = "";
	String favorites = "";
	String searchData = "";

	public String getFavorites() {
		return favorites;
	}

	public void setFavorites(String favorites) {
		this.favorites = favorites;
	}

	public String getSearchData() {
		return searchData;
	}

	public void setSearchData(String searchData) {
		this.searchData = searchData;
	}

	public String getProvider_id() {
		return provider_id;
	}

	public void setProvider_id(String provider_id) {
		this.provider_id = provider_id;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public String getTag1() {
		return tag1;
	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}

	public String getTag2() {
		return tag2;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}

	public String getContent_focus() {
		return content_focus;
	}

	public void setContent_focus(String content_focus) {
		this.content_focus = content_focus;
	}

	public String getVisual_content_format() {
		return visual_content_format;
	}

	public void setVisual_content_format(String visual_content_format) {
		this.visual_content_format = visual_content_format;
	}

	public String getVisual_content_type() {
		return visual_content_type;
	}

	public void setVisual_content_type(String visual_content_type) {
		this.visual_content_type = visual_content_type;
	}

	public String getStream_vod_url() {
		return stream_vod_url;
	}

	public void setStream_vod_url(String stream_vod_url) {
		this.stream_vod_url = stream_vod_url;
	}

	public String getChannel_format() {
		return channel_format;
	}

	public void setChannel_format(String channel_format) {
		this.channel_format = channel_format;
	}

	public String getVisual_content_location() {
		return visual_content_location;
	}

	public void setVisual_content_location(String visual_content_location) {
		this.visual_content_location = visual_content_location;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAudio_location() {
		return audio_location;
	}

	public void setAudio_location(String audio_location) {
		this.audio_location = audio_location;
	}

	public String getStream_type() {
		return stream_type;
	}

	public void setStream_type(String stream_type) {
		this.stream_type = stream_type;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStream_vod_user() {
		return stream_vod_user;
	}

	public void setStream_vod_user(String stream_vod_user) {
		this.stream_vod_user = stream_vod_user;
	}

	public String getGenre0() {
		return genre0;
	}

	public void setGenre0(String genre0) {
		this.genre0 = genre0;
	}

	public String getSubscription_plan() {
		return subscription_plan;
	}

	public void setSubscription_plan(String subscription_plan) {
		this.subscription_plan = subscription_plan;
	}

	public String getWrid() {
		return wrid;
	}

	public void setWrid(String wrid) {
		this.wrid = wrid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStream_vod_password() {
		return stream_vod_password;
	}

	public void setStream_vod_password(String stream_vod_password) {
		this.stream_vod_password = stream_vod_password;
	}

	public String getAudio_location_2() {
		return audio_location_2;
	}

	public void setAudio_location_2(String audio_location_2) {
		this.audio_location_2 = audio_location_2;
	}

	public String getVisual_content_location_2() {
		return visual_content_location_2;
	}

	public void setVisual_content_location_2(String visual_content_location_2) {
		this.visual_content_location_2 = visual_content_location_2;
	}

	public String getChannel_info() {
		return channel_info;
	}

	public void setChannel_info(String channel_info) {
		this.channel_info = channel_info;
	}

	public String getChannel_image_file() {
		return channel_image_file;
	}

	public void setChannel_image_file(String channel_image_file) {
		this.channel_image_file = channel_image_file;
	}

}
