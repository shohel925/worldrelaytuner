package com.aapbd.worldrelaytune.model;

public class ChannelInfo {
	public String long_desc = "";
	public String description = "";
	public String language = "";
	public String channel_name = "";
	public String country = "";
	public String icon_image = "";
	public String channel_number = "";
	public String genre = "";
	public String type = "";
	public String id = "";
	public String website_link = "";

	// public String format="";
	// public String group="";
	// public String stream="";
	// public String status="";
	// public String added_by="";
	// public String visibility_type="";
	// public String streaming_link="";
	// public String admin_status="";
	// public String streaming_url="";
	// public String created_date="";
	//

	public String getLong_desc() {
		return long_desc;
	}

	public void setLong_desc(String long_desc) {
		this.long_desc = long_desc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIcon_image() {
		return icon_image;
	}

	public void setIcon_image(String icon_image) {
		this.icon_image = icon_image;
	}

	public String getChannel_number() {
		return channel_number;
	}

	public void setChannel_number(String channel_number) {
		this.channel_number = channel_number;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWebsite_link() {
		return website_link;
	}

	public void setWebsite_link(String website_link) {
		this.website_link = website_link;
	}
	// public String getFormat() {
	// return format;
	// }
	// public void setFormat(String format) {
	// this.format = format;
	// }
	// public String getGroup() {
	// return group;
	// }
	// public void setGroup(String group) {
	// this.group = group;
	// }
	// public String getStream() {
	// return stream;
	// }
	// public void setStream(String stream) {
	// this.stream = stream;
	// }
	// public String getStatus() {
	// return status;
	// }
	// public void setStatus(String status) {
	// this.status = status;
	// }
	// public String getAdded_by() {
	// return added_by;
	// }
	// public void setAdded_by(String added_by) {
	// this.added_by = added_by;
	// }
	// public String getVisibility_type() {
	// return visibility_type;
	// }
	// public void setVisibility_type(String visibility_type) {
	// this.visibility_type = visibility_type;
	// }
	// public String getStreaming_link() {
	// return streaming_link;
	// }
	// public void setStreaming_link(String streaming_link) {
	// this.streaming_link = streaming_link;
	// }
	// public String getAdmin_status() {
	// return admin_status;
	// }
	// public void setAdmin_status(String admin_status) {
	// this.admin_status = admin_status;
	// }
	// public String getStreaming_url() {
	// return streaming_url;
	// }
	// public void setStreaming_url(String streaming_url) {
	// this.streaming_url = streaming_url;
	// }
	// public String getCreated_date() {
	// return created_date;
	// }
	// public void setCreated_date(String created_date) {
	// this.created_date = created_date;
	// }
	//
	//
	//
}
