package com.aapbd.worldrelaytune.model;

import java.util.ArrayList;
import java.util.List;

public class CategoryCountryList {

	String categoryGroup = "";

	List<CategoryTypeInfo> values = new ArrayList<CategoryTypeInfo>();

	public String getCategoryGroup() {
		return categoryGroup;
	}

	public void setCategoryGroup(String categoryGroup) {
		this.categoryGroup = categoryGroup;
	}

	public List<CategoryTypeInfo> getValues() {
		return values;
	}

	public void setValues(List<CategoryTypeInfo> values) {
		this.values = values;
	}

}
