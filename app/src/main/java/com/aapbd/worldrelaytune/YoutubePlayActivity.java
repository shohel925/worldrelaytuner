package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.aapbd.worldrelaytune.model.YoutubesResponse;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class YoutubePlayActivity extends YouTubeFailureRecoveryActivity {
	Context con;
	Activity act;

	Dialog dd;
	TextView mainTitle, youtubeBack, youtubeTitle;
	Typeface tf3;

	YoutubesResponse youtubesResponse;
	boolean flag = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.youtube_play);
		con = this;
		act = this;
		AnalyticsTracker.sendTrackData(this,"Youtube",
				AppConstant.channelInfo.getWrid());
		initUI();

	}

	private void initUI() {
		youtubeTitle = (TextView) findViewById(R.id.youtubeTitle);
		youtubeBack = (TextView) findViewById(R.id.youtubeBack);
		tf3 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
		youtubeTitle.setText(AppConstant.channelName);
		final YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view1);
		youTubeView.initialize(DeveloperKey.DEVELOPER_KEY, this);

		youtubeBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider,
			YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			Log.e("Call", "Call");
			if (flag) {
				player.cueVideo("" + AppConstant.youtubeVideoUrl);

			}
		}
	}

	@Override
	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerView) findViewById(R.id.youtube_view1);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub

		super.onConfigurationChanged(newConfig);
		Log.e("ONCONFIGCHANGE", "CALLED");
		flag = false;
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

		} else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

		}
	}

}
