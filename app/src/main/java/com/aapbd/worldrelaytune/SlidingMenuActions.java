package com.aapbd.worldrelaytune;//package com.aapbd.worldrelaytune;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.graphics.Typeface;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.aapbd.utils.nagivation.StartActivity;
//import com.aapbd.utils.storage.PersistData;
//import com.aapbd.worldrelaytune.utils.AppConstant;
//import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
//
//public class SlidingMenuActions {
//
//	Dialog dialog;
//	LinearLayout account, sheduler, future_order, past_order;
//	Context context;
//	SlidingMenu menu;
//	Activity activity;
//	TextView welcomeTv, name, userEmail, accountTv, createChannel, aboutTv,
//			logoutTv, allChannel, ok_acc;
//	LinearLayout myAccountLay, createChannelLay, aboutLay, logoutLay,
//			allChannelLay;
//
//	public SlidingMenuActions(Context con, Activity act, SlidingMenu menu,
//			TextView welcomeTv, TextView name, TextView userEmail,
//			TextView accountTv, TextView createChannel, TextView aboutTv,
//			TextView logoutTv, LinearLayout myAccountLay,
//			LinearLayout createChannelLay, LinearLayout aboutLay,
//			LinearLayout logoutLay, TextView allChannel,
//			LinearLayout allChannelLay) {
//		this.context = con;
//		this.activity = act;
//		this.menu = menu;
//		this.welcomeTv = welcomeTv;
//		this.name = name;
//		this.userEmail = userEmail;
//		this.accountTv = accountTv;
//		this.createChannel = createChannel;
//		this.aboutTv = aboutTv;
//		this.logoutTv = logoutTv;
//		this.myAccountLay = myAccountLay;
//		this.createChannelLay = createChannelLay;
//		this.aboutLay = aboutLay;
//		this.logoutLay = logoutLay;
//		this.allChannel = allChannel;
//		this.allChannelLay = allChannelLay;
//
//		initUI();
//
//	}
//
//	// public SlidingMenuActions(
//	// Context con,
//	// SlidingMenu menu,
//	//
//	// LinearLayout account,
//	// LinearLayout sheduler,
//	// LinearLayout future_order,
//	// LinearLayout past_order
//	//
//	// )
//	// {
//	// this.context = con;
//	// this.menu = menu;
//	// this.context = con;
//	// this.account = account;
//	// this.sheduler = sheduler;
//	// this.future_order = future_order;
//	// this.past_order = past_order;
//	//
//	//
//	// initUI();
//	//
//	// }
//
//	private void initUI() {
//		myAccountLay = (LinearLayout) menu.findViewById(R.id.myAccountLay);
//		createChannelLay = (LinearLayout) menu
//				.findViewById(R.id.createChannelLay);
//		aboutLay = (LinearLayout) menu.findViewById(R.id.aboutLay);
//		logoutLay = (LinearLayout) menu.findViewById(R.id.logoutLay);
//		welcomeTv = (TextView) menu.findViewById(R.id.welcomeTv);
//		name = (TextView) menu.findViewById(R.id.name);
//		userEmail = (TextView) menu.findViewById(R.id.userEmail);
//		accountTv = (TextView) menu.findViewById(R.id.accountTv);
//		logoutTv = (TextView) menu.findViewById(R.id.logoutTv);
//		createChannel = (TextView) menu.findViewById(R.id.createChannel);
//		aboutTv = (TextView) menu.findViewById(R.id.aboutTv);
//		allChannel = (TextView) menu.findViewById(R.id.allChannel);
//		allChannelLay = (LinearLayout) menu.findViewById(R.id.allChannelLay);
//
//		Typeface tf = Typeface.createFromAsset(activity.getAssets(),
//				"fonts/OpenSans-Bold.ttf");
//
//		Typeface tf3 = Typeface.createFromAsset(activity.getAssets(),
//				"fonts/OpenSans-Light.ttf");
//		welcomeTv.setTypeface(tf);
//		name.setTypeface(tf);
//		userEmail.setTypeface(tf3);
//		accountTv.setTypeface(tf3);
//		createChannel.setTypeface(tf3);
//		aboutTv.setTypeface(tf3);
//		logoutTv.setTypeface(tf3);
//		allChannel.setTypeface(tf3);
//
//		myAccountLay.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				StartActivity.toActivity(context, MyAccountActivity.class);
//			}
//		});
//
//		createChannelLay.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				dialog = new Dialog(context);
//				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//				dialog.setContentView(R.layout.create_channel_dialoge);
//				TextView ok_create_chanel = (TextView) dialog
//						.findViewById(R.id.ok_create_chanel);
//				TextView createchannel = (TextView) dialog
//						.findViewById(R.id.createchannel);
//				Typeface tf = Typeface.createFromAsset(context.getAssets(),
//						"fonts/OpenSans-Bold.ttf");
//				Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
//						"fonts/OpenSans-Light.ttf");
//
//				ok_create_chanel.setTypeface(tf);
//				createchannel.setTypeface(tf2);
//
//				ok_create_chanel.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//					}
//				});
//				dialog.show();
//			}
//		});
//		allChannelLay.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				StartActivity.toActivity(context,
//						AllPublicChannelActivity.class);
//
//			}
//		});
//		aboutLay.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				dialog = new Dialog(context);
//				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//				dialog.setContentView(R.layout.about_dialogue);
//				TextView ok_about = (TextView) dialog
//						.findViewById(R.id.ok_about);
//				TextView version = (TextView) dialog.findViewById(R.id.version);
//
//				Typeface tf = Typeface.createFromAsset(context.getAssets(),
//						"fonts/OpenSans-Bold.ttf");
//				Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
//						"fonts/OpenSans-Light.ttf");
//
//				ok_about.setTypeface(tf);
//				version.setTypeface(tf2);
//
//				ok_about.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//					}
//				});
//				dialog.show();
//			}
//		});
//
//		logoutLay.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//
//				dialog = new Dialog(context);
//				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//				dialog.setContentView(R.layout.signout_dialogue);
//				TextView ok = (TextView) dialog.findViewById(R.id.ok);
//				TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
//				TextView logoutTitle = (TextView) dialog
//						.findViewById(R.id.logoutTitle);
//
//				Typeface tf = Typeface.createFromAsset(context.getAssets(),
//						"fonts/OpenSans-Bold.ttf");
//				Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
//						"fonts/OpenSans-Light.ttf");
//
//				ok.setTypeface(tf);
//				cancel.setTypeface(tf);
//				logoutTitle.setTypeface(tf2);
//
//				logoutTitle.setText("Sign out from this account, "
//						+ PersistData.getStringData(context,
//								AppConstant.user_name) + "?");
//				cancel.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//					}
//				});
//
//				ok.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						StartActivity.toActivity(context, SinInActivity.class);
//						activity.finish();
//					}
//				});
//
//				dialog.show();
//
//			}
//
//		});
//
//	}
//
// }
