//package com.aapbd.worldrelaytune;
//
//import java.lang.reflect.Type;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.http.Header;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.app.ActionBar;
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.ArrayAdapter;
//import android.widget.EditText;
//import android.widget.GridView;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.aapbd.worldrelaytune.radio.RadioMainActivity;
//import com.aapbd.worldrelaytune.radio.StaticRadioArray;
//import com.aapbd.worldrelaytune.db.DatabaseHandler;
//import com.aapbd.worldrelaytune.model.AllChannelInfo;
//import com.aapbd.worldrelaytune.model.ChannelInfo;
//import com.aapbd.worldrelaytune.model.PublicChannelInfo;
//import com.aapbd.worldrelaytune.response.AllChannelResponse;
//import com.aapbd.worldrelaytune.utils.AlertMessage;
//import com.aapbd.worldrelaytune.utils.AllURL;
//import com.aapbd.worldrelaytune.utils.AppConstant;
//import com.aapbd.worldrelaytune.utils.BusyDialog;
//import com.aapbd.worldrelaytune.utils.NetInfo;
//import com.aapbd.worldrelaytune.utils.StartActivity;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import com.loopj.android.http.AsyncHttpClient;
//import com.loopj.android.http.AsyncHttpResponseHandler;
//import com.squareup.picasso.Picasso;
//
//public class AllPublicChannelActivity extends Activity {
//	Context con;
//	TextView bottomview;
//	TextView channelTypes, channelGenres, allCountries, allLanguage;
//	Activity act;
//	GridView fevoriteGridView;
//	GridAdapter gGridAdapter;
//	ListView listview;
//	// CountryListAdapter countryListAdapter;
//	// LanguageListAdapter languageListAdapter;
//	// GenereListAdapter genereListAdapter;
//	// ChannelTypeListAdapter channelTypeListAdapter;
//
//	Dialog dd;
//	TextView mainTitle;
//	Typeface tf3;
//	AllChannelResponse allChannelResponse;
//	List<AllChannelInfo> allList;
//	List<ChannelInfo> tempList;
//	List<PublicChannelInfo> allPublicChannelList;
//
//	DatabaseHandler db;
//	JSONArray jArray;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.all_channel);
//		con = this;
//		act = this;
//		db = new DatabaseHandler(con);
//		initUI();
//
//	}
//
//	private void initUI() {
//		channelTypes = (TextView) findViewById(R.id.channelTypes);
//		channelGenres = (TextView) findViewById(R.id.channelGenres);
//		allCountries = (TextView) findViewById(R.id.allCountries);
//		allLanguage = (TextView) findViewById(R.id.allLanguage);
//
//		actionBarSetup();
//
//		getChannelInfo(AllURL.getAllPublicChannelUrl());
//		channelTypes.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				changeBg("Channel Types");
//
//				// fevoriteGridView.setVisibility(View.GONE);
//				// listview.setVisibility(View.VISIBLE);
//				//
//				// channelTypeListAdapter = new ChannelTypeListAdapter(con);
//				// listview.setAdapter(channelTypeListAdapter);
//				// channelTypeListAdapter.notifyDataSetChanged();
//
//			}
//		});
//		channelGenres.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				changeBg("Channel Genres");
//
//				// fevoriteGridView.setVisibility(View.GONE);
//				// listview.setVisibility(View.VISIBLE);
//				//
//				// genereListAdapter = new GenereListAdapter(con);
//				// listview.setAdapter(genereListAdapter);
//				// genereListAdapter.notifyDataSetChanged();
//
//			}
//		});
//		allCountries.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				changeBg("All Countries");
//				// fevoriteGridView.setVisibility(View.GONE);
//				// listview.setVisibility(View.VISIBLE);
//				//
//				// countryListAdapter = new CountryListAdapter(con);
//				// listview.setAdapter(countryListAdapter);
//				// countryListAdapter.notifyDataSetChanged();
//			}
//		});
//		allLanguage.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				changeBg("All Languages");
//				//
//				// fevoriteGridView.setVisibility(View.GONE);
//				// listview.setVisibility(View.VISIBLE);
//				//
//				// languageListAdapter = new LanguageListAdapter(con);
//				// listview.setAdapter(languageListAdapter);
//				// languageListAdapter.notifyDataSetChanged();
//			}
//		});
//
//		fevoriteGridView = (GridView) findViewById(R.id.fevoriteGridView);
//		listview = (ListView) findViewById(R.id.listview);
//
//		// DummyData();
//
//		Typeface tf = Typeface.createFromAsset(getAssets(),
//				"fonts/OpenSans-Bold.ttf");
//		Typeface tf2 = Typeface.createFromAsset(getAssets(),
//				"fonts/OpenSans-LightItalic.ttf");
//		channelTypes.setTypeface(tf2);
//		channelGenres.setTypeface(tf2);
//		allCountries.setTypeface(tf2);
//		allLanguage.setTypeface(tf2);
//		tf3 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
//
//	}
//
//	public void searchImg(View v) {
//		searchDialoge();
//	}
//
//	public void searchDialoge() {
//		dd = new Dialog(con);
//		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dd.setContentView(R.layout.search_dialoge_cancel_search);
//		dd.show();
//
//		EditText et_search = (EditText) dd.findViewById(R.id.et_search);
//		ViewGroup btn_cancel = (ViewGroup) dd.findViewById(R.id.btn_cancel);
//		ViewGroup btn_search = (ViewGroup) dd.findViewById(R.id.btn_search);
//		TextView btn_sign = (TextView) dd.findViewById(R.id.btn_sign);
//		TextView searchTv = (TextView) dd.findViewById(R.id.searchTv);
//
//		btn_sign.setTypeface(tf3);
//		et_search.setTypeface(tf3);
//		searchTv.setTypeface(tf3);
//		btn_search.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//			}
//		});
//
//		btn_cancel.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				dd.dismiss();
//			}
//		});
//
//	}
//
//	private void actionBarSetup() {
//		// TODO Auto-generated method stub
//
//		ActionBar actionBar = getActionBar();
//		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//		actionBar.setCustomView(R.layout.login_top);
//
//		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
//				R.id.signtitle);
//		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
//				R.id.backImg);
//		signtitle.setText("All Channel");
//
//		Typeface tf = Typeface.createFromAsset(getAssets(),
//				"fonts/OpenSans-Bold.ttf");
//
//		signtitle.setTypeface(tf);
//
//		backImg.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				finish();
//			}
//		});
//
//	}
//
//	protected void changeBg(String buttonName) {
//		// TODO Auto-generated method stub
//		if (buttonName.equalsIgnoreCase("Channel Types")) {
//			channelTypes.setTextColor(Color.WHITE);
//			channelGenres.setTextColor(Color.BLACK);
//			allCountries.setTextColor(Color.BLACK);
//			allLanguage.setTextColor(Color.BLACK);
//			channelTypes.setBackgroundResource(R.drawable.bar_hov);
//			channelGenres.setBackgroundResource(R.drawable.bar_s);
//			allCountries.setBackgroundResource(R.drawable.bar_s);
//			allLanguage.setBackgroundResource(R.drawable.bar_r_hov);
//
//			return;
//
//		} else if (buttonName.equalsIgnoreCase("Channel Genres")) {
//			channelTypes.setTextColor(Color.BLACK);
//			channelGenres.setTextColor(Color.WHITE);
//			allCountries.setTextColor(Color.BLACK);
//			allLanguage.setTextColor(Color.BLACK);
//			channelTypes.setBackgroundResource(R.drawable.bar);
//			channelGenres.setBackgroundResource(R.drawable.bar_s_hov);
//			allCountries.setBackgroundResource(R.drawable.bar_s);
//			allLanguage.setBackgroundResource(R.drawable.bar_r_hov);
//
//			return;
//
//		} else if (buttonName.equalsIgnoreCase("All Countries")) {
//			channelTypes.setTextColor(Color.BLACK);
//			channelGenres.setTextColor(Color.BLACK);
//			allCountries.setTextColor(Color.WHITE);
//			allLanguage.setTextColor(Color.BLACK);
//			channelTypes.setBackgroundResource(R.drawable.bar);
//			channelGenres.setBackgroundResource(R.drawable.bar_s);
//			allCountries.setBackgroundResource(R.drawable.bar_s_hov);
//			allLanguage.setBackgroundResource(R.drawable.bar_r_hov);
//
//			return;
//
//		} else if (buttonName.equalsIgnoreCase("All Languages")) {
//			channelTypes.setTextColor(Color.BLACK);
//			channelGenres.setTextColor(Color.BLACK);
//			allCountries.setTextColor(Color.BLACK);
//			allLanguage.setTextColor(Color.WHITE);
//			channelTypes.setBackgroundResource(R.drawable.bar);
//			channelGenres.setBackgroundResource(R.drawable.bar_s);
//			allCountries.setBackgroundResource(R.drawable.bar_s);
//			allLanguage.setBackgroundResource(R.drawable.bar_r);
//
//			return;
//		}
//
//	}
//
//	private void getChannelInfo(final String url) {
//
//		if (!NetInfo.isOnline(con)) {
//			AlertMessage.showMessage(con, getString(R.string.Status),
//					getString(R.string.checkInternet));
//			return;
//		}
//
//		final BusyDialog busyNow = new BusyDialog(con, true, false);
//		busyNow.show();
//
//		// Log.e("login URL", url + "");
//
//		final AsyncHttpClient client = new AsyncHttpClient();
//		client.get(url, new AsyncHttpResponseHandler() {
//
//			@Override
//			public void onStart() {
//				// called before request is started
//			}
//
//			@Override
//			public void onSuccess(int statusCode, Header[] headers,
//					byte[] response) {
//				// called when response HTTP status is "200 OK"
//
//				if (busyNow != null) {
//					busyNow.dismis();
//				}
//				Log.e("response", new String(response));
//				Gson g = new Gson();
//
//				Type fooType = new TypeToken<ArrayList<PublicChannelInfo>>() {
//				}.getType();
//				allPublicChannelList = g
//						.fromJson(new String(response), fooType);
//
//				if (allPublicChannelList.size() > 0) {
//
//					gGridAdapter = new GridAdapter(con);
//					fevoriteGridView.setAdapter(gGridAdapter);
//					gGridAdapter.notifyDataSetChanged();
//				} else {
//
//					AlertMessage.showMessage(con, "Status",
//							"Server Problem Please Retry");
//
//				}
//
//			}
//
//			@Override
//			public void onFailure(int statusCode, Header[] headers,
//					byte[] errorResponse, Throwable e) {
//				// called when response HTTP status is "4XX" (eg. 401, 403, 404)
//
//				if (busyNow != null) {
//					busyNow.dismis();
//				}
//
//				// Log.e("response",new String(errorResponse));
//			}
//
//			@Override
//			public void onRetry(int retryNo) {
//				// called when request is retried
//
//			}
//		});
//
//	}
//
//	private void getChannelName(final String url) {
//
//		if (!NetInfo.isOnline(con)) {
//			AlertMessage.showMessage(con, getString(R.string.Status),
//					getString(R.string.checkInternet));
//			return;
//		}
//
//		final BusyDialog busyNow = new BusyDialog(con, true, false);
//		busyNow.show();
//
//		// Log.e("login URL", url + "");
//
//		final AsyncHttpClient client = new AsyncHttpClient();
//		client.get(url, new AsyncHttpResponseHandler() {
//
//			@Override
//			public void onStart() {
//				// called before request is started
//			}
//
//			@Override
//			public void onSuccess(int statusCode, Header[] headers,
//					byte[] response) {
//				// called when response HTTP status is "200 OK"
//
//				if (busyNow != null) {
//					busyNow.dismis();
//				}
//				Log.e("response", new String(response));
//
//				try {
//					JSONObject job = new JSONObject(new String(response));
//					jArray = job.getJSONArray("items");
//					JSONObject jholder = jArray.getJSONObject(0);
//					JSONObject jholder2 = jholder
//							.getJSONObject("contentDetails");
//					JSONObject jholder3 = jholder2
//							.getJSONObject("relatedPlaylists");
//					AppConstant.youtubeName = jholder3.optString("uploads");
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				if (jArray.length() > 0) {
//
//					StartActivity
//							.toActivity(con, YoutubePlayListActivity.class);
//				} else {
//
//					AlertMessage.showMessage(con, "Status",
//							"Server Problem Please Retry");
//
//				}
//
//			}
//
//			@Override
//			public void onFailure(int statusCode, Header[] headers,
//					byte[] errorResponse, Throwable e) {
//				// called when response HTTP status is "4XX" (eg. 401, 403, 404)
//
//				if (busyNow != null) {
//					busyNow.dismis();
//				}
//
//				// Log.e("response",new String(errorResponse));
//			}
//
//			@Override
//			public void onRetry(int retryNo) {
//				// called when request is retried
//
//			}
//		});
//
//	}
//
//	private class GridAdapter extends ArrayAdapter<PublicChannelInfo> {
//		Context context;
//
//		GridAdapter(Context context) {
//			super(context, R.layout.row_favorite, allPublicChannelList);
//
//			this.context = context;
//
//		}
//
//		@Override
//		public View getView(final int position, View convertView,
//				ViewGroup parent) {
//			View v = convertView;
//
//			if (v == null) {
//				final LayoutInflater vi = (LayoutInflater) context
//						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//				v = vi.inflate(R.layout.row_favorite, null);
//
//			}
//
//			if (position < allPublicChannelList.size()) {
//
//				final PublicChannelInfo query = allPublicChannelList
//						.get(position);
//
//				final ImageView neaPeopleName = (ImageView) v
//						.findViewById(R.id.favoriteImg);
//				final TextView channelNumber = (TextView) v
//						.findViewById(R.id.channelNumber);
//				final TextView tvname = (TextView) v.findViewById(R.id.tvname);
//
//				tvname.setText(query.getName());
//				channelNumber.setText(query.getWrid());
//				Picasso.with(con).load(query.getChannel_image_file())
//						.error(R.drawable.ic_launcher).into(neaPeopleName);
//
//				tvname.setTypeface(tf3);
//				channelNumber.setTypeface(tf3);
//
//			}
//
//			v.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//
//					final PublicChannelInfo query = allPublicChannelList
//							.get(position);
//					if (TextUtils.isEmpty(query.getAudio_location())) {
//
//						if (query.getStream_type().equalsIgnoreCase("1")) {
//							if (query.getVisual_content_location().startsWith(
//									"youtubelive")) {
//								AppConstant.youtubeVideoUrl = query
//										.getVisual_content_location()
//										.substring(
//												query.getVisual_content_location()
//														.lastIndexOf("+") + 1);
//								AppConstant.channelName = query.getName();
//								AppConstant.channelInfo = null;
//								StartActivity.toActivity(con,
//										YoutubePlayActivity.class);
//							} else {
//								AppConstant.youtubeName = query
//										.getVisual_content_location();
//								AppConstant.channelName = query.getName();
//								Log.e("Normal Video Name", ">>"
//										+ AppConstant.youtubeName);
//								AppConstant.channelInfo = query;
//								StartActivity.toActivity(con,
//										VideoPlayActivity.class);
//
//							}
//						} else if (query.getStream_type().equalsIgnoreCase("2")) {
//							if (TextUtils.isEmpty(query.getStream_vod_url())) {
//								AppConstant.youtubeVideoUrl = query
//										.getVisual_content_location()
//										.substring(
//												query.getVisual_content_location()
//														.lastIndexOf("+") + 1);
//								Log.e("Youtube video Live Name", ">>"
//										+ AppConstant.youtubeVideoUrl);
//
//							} else {
//								if ((query.getStream_vod_url()
//										.startsWith("http://vimeo"))
//										|| (query.getStream_vod_url()
//												.startsWith("https://vimeo"))) {
//									AppConstant.youtubeName = query
//											.getStream_vod_url()
//											.substring(
//													query.getStream_vod_url()
//															.lastIndexOf("/") + 1);
//									Log.e("Vimeo Name", ">>"
//											+ AppConstant.youtubeName);
//									AppConstant.channelInfo = null;
//									StartActivity.toActivity(con,
//											VimeoPlayListActivity.class);
//								} else if (query.getStream_vod_url()
//										.startsWith("youtube")) {
//
//									if (query.getStream_vod_url().startsWith(
//											"youtubeuser")) {
//										AppConstant.youtubeName = query
//												.getStream_vod_url()
//												.substring(
//														query.getStream_vod_url()
//																.lastIndexOf(
//																		"+") + 1);
//										Log.e("Youtube User", ">>"
//												+ AppConstant.youtubeName);
//										AppConstant.channelName = query
//												.getName();
//										AppConstant.channelInfo = null;
//										getChannelName(AllURL
//												.getAllYoutubeChannelName(AppConstant.youtubeName));
//
//									} else if (query.getStream_vod_url()
//											.startsWith("youtubeplaylist")) {
//										AppConstant.youtubeName = query
//												.getStream_vod_url()
//												.substring(
//														query.getStream_vod_url()
//																.lastIndexOf(
//																		"+") + 1);
//										Log.e("Youtube PlayList", ">>"
//												+ AppConstant.youtubeName);
//										AppConstant.channelName = query
//												.getName();
//										AppConstant.channelInfo = null;
//										StartActivity.toActivity(con,
//												YoutubePlayListActivity.class);
//									}
//
//								} else if (query.getStream_vod_url()
//										.startsWith("vevo")) {
//									// ===== This condition for Vevo ======
//									AppConstant.youtubeName = query
//											.getStream_vod_url();
//									Log.e("Youtube PlayList", ">>"
//											+ AppConstant.youtubeName);
//									Toast.makeText(con, "Working letter", 3000)
//											.show();
//								}
//							}
//
//						} else if (query.getStream_type().equalsIgnoreCase("3")) {
//							// Normal M3u3 video in video view
//							AppConstant.youtubeName = query
//									.getVisual_content_location();
//							AppConstant.channelName = query.getName();
//							Log.e("Normal Video Name", ">>"
//									+ AppConstant.youtubeName);
//							AppConstant.channelInfo = query;
//							StartActivity.toActivity(con,
//									VideoPlayActivity.class);
//						}
//
//					} else {
//						StaticRadioArray.urls[0] = query.getAudio_location();
//						AppConstant.channelName = query.getName();
//						AppConstant.channelInfo = null;
//						AppConstant.visual_content_location = query
//								.getVisual_content_location();
//						getMaxImage(query.getVisual_content_location()
//								+ "maxNumOfImages");
//
//					}
//
//				}
//			});
//
//			return v;
//
//		}
//
//	}
//
//	private void getMaxImage(final String url) {
//
//		if (!NetInfo.isOnline(con)) {
//			AlertMessage.showMessage(con, getString(R.string.Status),
//					getString(R.string.checkInternet));
//			return;
//		}
//
//		final BusyDialog busyNow = new BusyDialog(con, true, false);
//		busyNow.show();
//
//		final AsyncHttpClient client = new AsyncHttpClient();
//		client.get(url, new AsyncHttpResponseHandler() {
//
//			@Override
//			public void onStart() {
//				// called before request is started
//			}
//
//			@Override
//			public void onSuccess(int statusCode, Header[] headers,
//					byte[] response) {
//				// called when response HTTP status is "200 OK"
//
//				if (busyNow != null) {
//					busyNow.dismis();
//				}
//				Log.e("response", new String(response));
//				String imageCount = new String(response);
//				AppConstant.imageSize = Integer.parseInt(imageCount);
//				Log.e("Image Size", ">>" + AppConstant.imageSize);
//				StartActivity.toActivity(con, RadioMainActivity.class);
//			}
//
//			@Override
//			public void onFailure(int statusCode, Header[] headers,
//					byte[] errorResponse, Throwable e) {
//				// called when response HTTP status is "4XX" (eg. 401, 403, 404)
//
//				if (busyNow != null) {
//					busyNow.dismis();
//				}
//
//				// Log.e("response",new String(errorResponse));
//			}
//
//			@Override
//			public void onRetry(int retryNo) {
//				// called when request is retried
//
//			}
//		});
//
//	}
//
//	// private class CountryListAdapter extends ArrayAdapter<AllCountryInfo> {
//	// Context context;
//	//
//	// CountryListAdapter(Context context) {
//	// super(context, R.layout.row_country, AppConstant.allCountryInfo);
//	//
//	// this.context = context;
//	//
//	// }
//	//
//	// @Override
//	// public View getView(final int position, View convertView,
//	// ViewGroup parent) {
//	// View v = convertView;
//	//
//	// if (v == null) {
//	// final LayoutInflater vi = (LayoutInflater) context
//	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//	// v = vi.inflate(R.layout.row_country, null);
//	//
//	// }
//	//
//	// if (position < AppConstant.allCountryInfo.size()) {
//	//
//	// final AllCountryInfo query = AppConstant.allCountryInfo
//	// .get(position);
//	//
//	// // final ImageView neaPeopleName = (ImageView)
//	// // v.findViewById(R.id.favoriteImg);
//	// final TextView country = (TextView) v
//	// .findViewById(R.id.country);
//	// final TextView flag = (TextView) v.findViewById(R.id.flag);
//	//
//	// country.setText(query.getPost().getName() + "");
//	// flag.setText(query.getPost().getFlag() + "");
//	//
//	// country.setTypeface(tf3);
//	// flag.setTypeface(tf3);
//	//
//	// }
//	//
//	// v.setOnClickListener(new OnClickListener() {
//	//
//	// @Override
//	// public void onClick(View v) {
//	// // TODO Auto-generated method stub
//	// if (tempList != null) {
//	// tempList.clear();
//	// }
//	// final AllCountryInfo query = AppConstant.allCountryInfo
//	// .get(position);
//	// tempList = db.getAllChannelByCountry(query.getPost()
//	// .getName());
//	// fevoriteGridView.setVisibility(View.VISIBLE);
//	// listview.setVisibility(View.GONE);
//	// gGridAdapter = new GridAdapter(con);
//	// fevoriteGridView.setAdapter(gGridAdapter);
//	// gGridAdapter.notifyDataSetChanged();
//	//
//	// }
//	// });
//	//
//	// return v;
//	//
//	// }
//	//
//	// }
//	//
//	// private class ChannelTypeListAdapter extends
//	// ArrayAdapter<AllChannelTypesInfo> {
//	// Context context;
//	//
//	// ChannelTypeListAdapter(Context context) {
//	// super(context, R.layout.row_country,
//	// AppConstant.allChannelTypesInfo);
//	//
//	// this.context = context;
//	//
//	// }
//	//
//	// @Override
//	// public View getView(final int position, View convertView,
//	// ViewGroup parent) {
//	// View v = convertView;
//	//
//	// if (v == null) {
//	// final LayoutInflater vi = (LayoutInflater) context
//	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//	// v = vi.inflate(R.layout.row_country, null);
//	//
//	// }
//	//
//	// if (position < AppConstant.allChannelTypesInfo.size()) {
//	//
//	// final AllChannelTypesInfo query = AppConstant.allChannelTypesInfo
//	// .get(position);
//	//
//	// // final ImageView neaPeopleName = (ImageView)
//	// // v.findViewById(R.id.favoriteImg);
//	// final TextView country = (TextView) v
//	// .findViewById(R.id.country);
//	// // final TextView flag = (TextView) v.findViewById(R.id.flag);
//	//
//	// country.setText(query.getPost().getChannel_type() + "");
//	// // flag.setText(query.getPost().getFlag()+"");
//	//
//	// country.setTypeface(tf3);
//	// // flag.setTypeface(tf3);
//	//
//	// }
//	//
//	// v.setOnClickListener(new OnClickListener() {
//	//
//	// @Override
//	// public void onClick(View v) {
//	// // TODO Auto-generated method stub
//	// if (tempList != null) {
//	// tempList.clear();
//	// }
//	// final AllChannelTypesInfo query = AppConstant.allChannelTypesInfo
//	// .get(position);
//	// tempList = db.getAllChannelByType(query.getPost().getId());
//	// fevoriteGridView.setVisibility(View.VISIBLE);
//	// listview.setVisibility(View.GONE);
//	// gGridAdapter = new GridAdapter(con);
//	// fevoriteGridView.setAdapter(gGridAdapter);
//	// gGridAdapter.notifyDataSetChanged();
//	// }
//	// });
//	//
//	// return v;
//	//
//	// }
//	//
//	// }
//	//
//	// private class LanguageListAdapter extends ArrayAdapter<AllLanguageInfo> {
//	// Context context;
//	//
//	// LanguageListAdapter(Context context) {
//	// super(context, R.layout.row_country, AppConstant.allLanguageInfo);
//	//
//	// this.context = context;
//	//
//	// }
//	//
//	// @Override
//	// public View getView(final int position, View convertView,
//	// ViewGroup parent) {
//	// View v = convertView;
//	//
//	// if (v == null) {
//	// final LayoutInflater vi = (LayoutInflater) context
//	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//	// v = vi.inflate(R.layout.row_country, null);
//	//
//	// }
//	//
//	// if (position < AppConstant.allLanguageInfo.size()) {
//	//
//	// final AllLanguageInfo query = AppConstant.allLanguageInfo
//	// .get(position);
//	//
//	// // final ImageView neaPeopleName = (ImageView)
//	// // v.findViewById(R.id.favoriteImg);
//	// final TextView language = (TextView) v
//	// .findViewById(R.id.country);
//	// // final TextView flag = (TextView) v.findViewById(R.id.flag);
//	//
//	// language.setText(query.getPost().getLanguage());
//	//
//	// language.setGravity(Gravity.CENTER);
//	// // flag.setText(query.getPost().getFlag()+"");
//	//
//	// language.setTypeface(tf3);
//	// // flag.setTypeface(tf3);
//	//
//	// }
//	//
//	// v.setOnClickListener(new OnClickListener() {
//	//
//	// @Override
//	// public void onClick(View v) {
//	// // TODO Auto-generated method stub
//	// if (tempList != null) {
//	// tempList.clear();
//	// }
//	// final AllLanguageInfo query = AppConstant.allLanguageInfo
//	// .get(position);
//	// tempList = db.getAllChannelByLanguage(query.getPost()
//	// .getLanguage());
//	// fevoriteGridView.setVisibility(View.VISIBLE);
//	// listview.setVisibility(View.GONE);
//	// gGridAdapter = new GridAdapter(con);
//	// fevoriteGridView.setAdapter(gGridAdapter);
//	// gGridAdapter.notifyDataSetChanged();
//	//
//	// }
//	// });
//	//
//	// return v;
//	//
//	// }
//	//
//	// }
//	//
//	// private class GenereListAdapter extends ArrayAdapter<AllChannelGeneInfo>
//	// {
//	// Context context;
//	//
//	// GenereListAdapter(Context context) {
//	// super(context, R.layout.row_country, AppConstant.allGenereinfo);
//	//
//	// this.context = context;
//	//
//	// }
//	//
//	// @Override
//	// public View getView(final int position, View convertView,
//	// ViewGroup parent) {
//	// View v = convertView;
//	//
//	// if (v == null) {
//	// final LayoutInflater vi = (LayoutInflater) context
//	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//	// v = vi.inflate(R.layout.row_country, null);
//	//
//	// }
//	//
//	// if (position < AppConstant.allGenereinfo.size()) {
//	//
//	// final AllChannelGeneInfo query = AppConstant.allGenereinfo
//	// .get(position);
//	//
//	// // final ImageView neaPeopleName = (ImageView)
//	// // v.findViewById(R.id.favoriteImg);
//	// final TextView language = (TextView) v
//	// .findViewById(R.id.country);
//	// // final TextView flag = (TextView) v.findViewById(R.id.flag);
//	//
//	// language.setText(query.getPost().getGenre() + "");
//	//
//	// // language.setGravity(Gravity.CENTER);
//	// // flag.setText(query.getPost().getFlag()+"");
//	//
//	// language.setTypeface(tf3);
//	// // flag.setTypeface(tf3);
//	//
//	// }
//	//
//	// v.setOnClickListener(new OnClickListener() {
//	//
//	// @Override
//	// public void onClick(View v) {
//	// // TODO Auto-generated method stub
//	// if (tempList != null) {
//	// tempList.clear();
//	// }
//	// final AllChannelGeneInfo query = AppConstant.allGenereinfo
//	// .get(position);
//	// tempList = db.getAllChannelByGenre(query.getPost().getId());
//	// fevoriteGridView.setVisibility(View.VISIBLE);
//	// listview.setVisibility(View.GONE);
//	// gGridAdapter = new GridAdapter(con);
//	// fevoriteGridView.setAdapter(gGridAdapter);
//	// gGridAdapter.notifyDataSetChanged();
//	//
//	// }
//	// });
//	//
//	// return v;
//	//
//	// }
//	//
//	// }
//
//}
