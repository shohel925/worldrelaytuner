package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.StartActivity;

public class BeforloginActivity extends Activity implements OnClickListener {
	Context con;
	TextView sinup, sinin;
	public static int[] imageRSC = { R.drawable.one, R.drawable.two,
			R.drawable.third, R.drawable.fourth, R.drawable.five };
	LinearLayout indicator, signIn_signUp;
	ViewPager fullpageimage;
	TextView loginBtn, signUpBtn;
	public static BeforloginActivity instance;

	Typeface tf3;

	String[] titleArray, desArray;

	public static BeforloginActivity getInstance() {
		return instance;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_layout);
		AnalyticsTracker.sendTrackData(this, "Before Login", "");
		AnalyticsTracker.sendEventData(this, "Before Login", "action", "label");
		con = this;
		instance = this;
		initUi();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		signUpBtn = (TextView) findViewById(R.id.signUpBtn);
		loginBtn = (TextView) findViewById(R.id.loginBtn);
		fullpageimage = (ViewPager) findViewById(R.id.fullpageimage);
		indicator = (LinearLayout) findViewById(R.id.mainIndicator);
		signIn_signUp = (LinearLayout) findViewById(R.id.signIn_signUp);
		titleArray = getResources().getStringArray(R.array.bold_string);
		desArray = getResources().getStringArray(R.array.normal_string);
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		signUpBtn.setTypeface(tf);
		loginBtn.setTypeface(tf);

		fullpageimage.setAdapter(new AllViewImage(con, imageRSC, titleArray,
				desArray));
		fullpageimage.setCurrentItem(0);
		controlDots(0);

		if (PersistData.getBooleanData(con, AppConstant.singinHide)) {

			signIn_signUp.setVisibility(View.VISIBLE);
		}
		loginBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartActivity.toActivity(con, SinInActivity.class);
			}
		});
		signUpBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartActivity.toActivity(con, SinUpActivity.class);
			}
		});

		fullpageimage.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				// TODO Auto-generated method stub

				if (!PersistData.getBooleanData(con, AppConstant.singinHide)) {

					if (pos == 4) {
						PersistData.setBooleanData(con, AppConstant.singinHide,
								true);
						signIn_signUp.setVisibility(View.VISIBLE);
					}

				} else {
					signIn_signUp.setVisibility(View.VISIBLE);
				}

				controlDots(pos);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	protected void controlDots(int newPageNumber) {

		try {
			indicator.removeAllViews(); // simple linear layout

			final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(

			android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

			layoutParams.setMargins(7, 2, 0, 2);

			final ImageView[] b = new ImageView[imageRSC.length];

			// Log.e("Shop size ", ">>>" + db.getAllShoppingName().size());

			for (int i1 = 0; i1 < imageRSC.length; i1++) {

				b[i1] = new ImageView(con);

				b[i1].setId(1000 + i1);

				if (newPageNumber == i1) {

					b[i1].setBackgroundResource(R.drawable.dott);

				} else {

					b[i1].setBackgroundResource(R.drawable.dot);

				}

				b[i1].setLayoutParams(layoutParams);

				indicator.addView(b[i1]);

			}

		} catch (final Exception e) {

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
