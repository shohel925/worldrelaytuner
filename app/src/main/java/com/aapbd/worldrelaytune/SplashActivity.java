package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.TextView;

import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.PersistentUser;
import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends Activity {
	// Note: Your consumer key and secret should be obfuscated in your source
	// code before shipping.

	private static final String TWITTER_KEY = "ONTxecdrmnKTzSkRV7u8yUxx6";

	private static final String TWITTER_SECRET = "pqoipwmJcCeZ6OrALtcs1cRVPp1htwcKt0YyIj4jzSAv9ZVmNk";

	Context con;

	Handler handler = new Handler();
	TextView splashTitle, splashDes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY,
				TWITTER_SECRET);
		Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_screen);
		AnalyticsTracker.sendTrackData(this, "Splash Screen", "");
		AnalyticsTracker.sendEventData(this, "Splash Screen", "action", "label");
		con = this;
		initUi();

		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (PersistentUser.isLogged(con)) {
					final Intent intent = new Intent(con, MainActivity.class);
					startActivity(intent);
					SplashActivity.this.finish();

				} else {
					final Intent in = new Intent(con, BeforloginActivity.class);
					startActivity(in);
					SplashActivity.this.finish();
				}

			}
		}, 3000);

	}

	private void initUi() {
		// TODO Auto-generated method stub
		splashTitle = (TextView) findViewById(R.id.splashTitle);
		splashDes = (TextView) findViewById(R.id.splashDes);
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		splashTitle.setTypeface(tf);
		Typeface tf1 = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");
		splashDes.setTypeface(tf1);
	}

}
