package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.CategoryTypeInfo;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CategoryListFlipper {

	ViewFlipper viewFlipper;
	Context con;
	Activity newActivty;

	Typeface t3;
	TextView categoryTitile;
	ListView categoryList;
	CustomAdapter customAdapter;
	Typeface tf;
	Dialog dd;
	EditText et_search;
	Typeface tf3;
	LinearLayout categoryListBack;
	List<CategoryTypeInfo> temp = new ArrayList<CategoryTypeInfo>();
	DatabaseHandler db;

	public CategoryListFlipper(ViewFlipper viewFlipper, Context con,
			Activity newActivty) {

		this.viewFlipper = viewFlipper;
		this.con = con;
		this.newActivty = newActivty;
		db = new DatabaseHandler(con);
		viewFlipper.setDisplayedChild(3);
		AnalyticsTracker.sendTrackData(newActivty, "Categories", "");
		AnalyticsTracker.sendEventData(newActivty, "Categories", "action", "label");
//		MyApplication.getInstance().trackEvent("Categories", "", "");
		initUi();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		categoryListBack = (LinearLayout) newActivty.findViewById(R.id.categoryListBack);
		categoryList = (ListView) newActivty.findViewById(R.id.categoryListList);
		categoryTitile = (TextView) newActivty.findViewById(R.id.categoryListTitile);
		categoryTitile.setText(AppConstant.categoryType);
		tf = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Bold.ttf");
		categoryTitile.setTypeface(tf);
		tf3 = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Light.ttf");

		if (AppConstant.typeCheck.equalsIgnoreCase("TYPES")) {
			temp = AppConstant.categoryTypeList;

		} else if (AppConstant.typeCheck.equalsIgnoreCase("GENRES")) {
			temp = AppConstant.categoryGenreList;
		} else if (AppConstant.typeCheck.equalsIgnoreCase("COUNTRIES")) {
			temp = AppConstant.categoryCountryList;
		} else if (AppConstant.typeCheck.equalsIgnoreCase("LANGUAGES")) {
			temp = AppConstant.categoryLanguageList;
		}
		AppConstant.fromMore=true;

		customAdapter = new CustomAdapter(con);
		categoryList.setAdapter(customAdapter);
		customAdapter.notifyDataSetChanged();

		categoryListBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				viewFlipper.setDisplayedChild(1);
			}
		});

	}

	private class CustomAdapter extends ArrayAdapter<CategoryTypeInfo> {
		Context context;

		CustomAdapter(Context context) {
			super(context, R.layout.category_rows, temp);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.category_rows, null);

			}

			if (position < temp.size()) {

				final CategoryTypeInfo query = temp.get(position);

				final ImageView category_row_img = (ImageView) v.findViewById(R.id.category_row_img);
				final TextView categoryChannelName = (TextView) v.findViewById(R.id.categoryChannelName);
				Typeface tf3 = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Light.ttf");
				categoryChannelName.setTypeface(tf);

				categoryChannelName.setText(query.getTitle());
				Picasso.with(con).load(query.getImage()).error(R.drawable.default_img).into(category_row_img);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					CategoryTypeInfo publicInfo = temp.get(position);
					if (AppConstant.typeCheck.equalsIgnoreCase("TYPES")) {
						AppConstant.categoryType = publicInfo.getTitle();
						AppConstant.categoryList = db.getAllChannelByType(publicInfo.getIndex());
						if (AppConstant.categoryList.size() > 0) {

							new CategoryItemFlipper(viewFlipper, con, newActivty);

						} else {
							AlertMessage.showMessage(con,
									con.getString(R.string.app_name),
									con.getString(R.string.noContentYet));
						}

					} else if (AppConstant.typeCheck.equalsIgnoreCase("GENRES")) {
						AppConstant.categoryType = publicInfo.getTitle();
						AppConstant.categoryList = db
								.getAllChannelByGenre(publicInfo.getIndex());
						if (AppConstant.categoryList.size() > 0) {

							new CategoryItemFlipper(viewFlipper, con,
									newActivty);

						} else {
							AlertMessage.showMessage(con,
									con.getString(R.string.app_name),
									con.getString(R.string.noContentYet));
						}

					} else if (AppConstant.typeCheck.equalsIgnoreCase(
							"COUNTRIES")) {
						AppConstant.categoryType = publicInfo.getTitle();
						AppConstant.categoryList = db
								.getAllChannelByCountry(publicInfo.getTitle());
						if (AppConstant.categoryList.size() > 0) {

							new CategoryItemFlipper(viewFlipper, con,
									newActivty);

						} else {
							AlertMessage.showMessage(con,
									con.getString(R.string.app_name),
									con.getString(R.string.noContentYet));
						}

					} else if (AppConstant.typeCheck.equalsIgnoreCase("LANGUAGES")) {
						AppConstant.categoryType = publicInfo.getTitle();
						AppConstant.categoryList = db.getAllChannelByLanguage(publicInfo.getTitle());
						if (AppConstant.categoryList.size() > 0) {

							new CategoryItemFlipper(viewFlipper, con,
									newActivty);

						} else {
							AlertMessage.showMessage(con,
									con.getString(R.string.app_name),
									con.getString(R.string.noContentYet));
						}

					}
				}
			});

			return v;

		}

	}

}
