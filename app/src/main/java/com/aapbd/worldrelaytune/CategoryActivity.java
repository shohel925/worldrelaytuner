//package com.aapbd.worldrelaytune;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import android.app.ActionBar;
//import android.app.Activity;
//import android.content.Context;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.EditText;
//import android.widget.GridView;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.aapbd.worldrelaytune.model.PublicChannelInfo;
//import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
//import com.aapbd.worldrelaytune.utils.AppConstant;
//
//public class CategoryActivity extends Activity {
//
//	Context con;
//
//	EditText first_name, last_name, eamil, passworad, confirm_password;
//
//	TextView btn_sin_up;
//	String responses = "";
//	GridView fevoriteGridView;
//	List<PublicChannelInfo> pList = new ArrayList<PublicChannelInfo>();
//	GridAdapter gGridAdapter;
//	Typeface tf3;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.favorites_lay);
//		AnalyticsTracker.sendTrackData(this, R.string.app_name);
//		con = this;
//		initUI();
//
//	}
//
//	private void initUI() {
//		// TODO Auto-generated method stub
//		fevoriteGridView = (GridView) findViewById(R.id.fevoriteGridView);
//		actionBarSetup();
//		DummyData();
//		tf3 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
//		gGridAdapter = new GridAdapter(con);
//		fevoriteGridView.setAdapter(gGridAdapter);
//		gGridAdapter.notifyDataSetChanged();
//	}
//
//	private void DummyData() {
//		// TODO Auto-generated method stub
//		if (pList != null) {
//			pList.clear();
//		}
//		PublicChannelInfo pInfo = new PublicChannelInfo();
//		pInfo.setName("HBO");
//		pInfo.setWrid("123432");
//		pList.add(pInfo);
//
//		PublicChannelInfo pInfo1 = new PublicChannelInfo();
//		pInfo1.setName("HBO");
//		pInfo1.setWrid("123432");
//		pList.add(pInfo1);
//
//		PublicChannelInfo pInfo2 = new PublicChannelInfo();
//		pInfo2.setName("HBO");
//		pInfo2.setWrid("123432");
//		pList.add(pInfo2);
//
//		PublicChannelInfo pInfo3 = new PublicChannelInfo();
//		pInfo3.setName("HBO");
//		pInfo3.setWrid("123432");
//		pList.add(pInfo3);
//
//	}
//
//	private void actionBarSetup() {
//		// TODO Auto-generated method stub
//
//		ActionBar actionBar = getActionBar();
//		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//		actionBar.setCustomView(R.layout.login_top);
//
//		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
//				R.id.signtitle);
//		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
//				R.id.backImg);
//		signtitle.setText(AppConstant.categoryName);
//
//		Typeface tf = Typeface.createFromAsset(getAssets(),
//				"fonts/OpenSans-Bold.ttf");
//
//		signtitle.setTypeface(tf);
//
//		backImg.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				finish();
//			}
//		});
//
//	}
//
//	private class GridAdapter extends ArrayAdapter<PublicChannelInfo> {
//		Context context;
//
//		GridAdapter(Context context) {
//			super(context, R.layout.row_favorite, pList);
//
//			this.context = context;
//
//		}
//
//		@Override
//		public View getView(final int position, View convertView,
//				ViewGroup parent) {
//			View v = convertView;
//
//			if (v == null) {
//				final LayoutInflater vi = (LayoutInflater) context
//						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//				v = vi.inflate(R.layout.row_favorite, null);
//
//			}
//
//			if (position < pList.size()) {
//
//				final PublicChannelInfo query = pList.get(position);
//
//				final ImageView neaPeopleName = (ImageView) v
//						.findViewById(R.id.favoriteImg);
//				final TextView channelNumber = (TextView) v
//						.findViewById(R.id.channelNumber);
//				final TextView tvname = (TextView) v.findViewById(R.id.tvname);
//
//				tvname.setText(query.getName());
//				channelNumber.setText(query.getWrid());
//
//				tvname.setTypeface(tf3);
//				channelNumber.setTypeface(tf3);
//
//			}
//
//			v.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//
//				}
//			});
//
//			return v;
//
//		}
//
//	}
//
//}
