package com.aapbd.worldrelaytune.radio.lastfm.cache;

import java.io.IOException;
import java.util.Collection;

import com.aapbd.worldrelaytune.radio.lastfm.scrobble.Scrobbler;
import com.aapbd.worldrelaytune.radio.lastfm.scrobble.SubmissionData;

/**
 * A <code>ScrobbleCache</code> is able to cache {@link SubmissionData}
 * instances for later submission to the Last.fm servers.
 *
 * @author Janni Kovacs
 */
public interface ScrobbleCache {

	/**
	 * Caches one or more {@link com.aapbd.worldrelaytune.radio.lastfm.scrobble.SubmissionData}
	 * .
	 *
	 * @param submissions
	 *            The submissions
	 */
	public void cacheScrobble(SubmissionData... submissions);

	/**
	 * Caches a collection of {@link SubmissionData}.
	 *
	 * @param submissions
	 *            The submissions
	 */
	public void cacheScrobble(Collection<SubmissionData> submissions);

	/**
	 * Checks if the cache contains any scrobbles.
	 *
	 * @return <code>true</code> if this cache is empty
	 */
	public boolean isEmpty();

	/**
	 * Tries to scrobble all cached scrobbles. If it succeeds the cache will be
	 * empty afterwards. If this method fails an IOException is thrown and no
	 * entries are removed from the cache.
	 *
	 * @param scrobbler
	 *            A {@link Scrobbler} instance
	 * @throws IOException
	 *             on I/O errors
	 * @throws IllegalStateException
	 *             if the {@link Scrobbler} is not fully initialized (i.e. no
	 *             handshake performed)
	 */
	public void scrobble(Scrobbler scrobbler) throws IOException;

	/**
	 * Clears all cached scrobbles from this cache.
	 */
	public void clearScrobbleCache();
}
