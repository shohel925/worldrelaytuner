package com.aapbd.worldrelaytune.radio.lastfm.cache;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is just for testing. You probably don't want to use it in
 * production.
 *
 * @author Janni Kovacs
 */
public class MemoryCache extends Cache {

	private final Map<String, String> data = new HashMap<String, String>();
	private final Map<String, Long> expirations = new HashMap<String, Long>();

	@Override
	public boolean contains(String cacheEntryName) {
		final boolean contains = data.containsKey(cacheEntryName);
		System.out.println("MemoryCache.contains: " + cacheEntryName + " ? "
				+ contains);
		return contains;
	}

	@Override
	public InputStream load(String cacheEntryName) {
		System.out.println("MemoryCache.load: " + cacheEntryName);
		try {
			return new ByteArrayInputStream(data.get(cacheEntryName).getBytes(
					"UTF-8"));
		} catch (final UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void remove(String cacheEntryName) {
		System.out.println("MemoryCache.remove: " + cacheEntryName);
		data.remove(cacheEntryName);
		expirations.remove(cacheEntryName);
	}

	@Override
	public void store(String cacheEntryName, InputStream inputStream,
			long expirationDate) {
		System.out.println("MemoryCache.store: " + cacheEntryName
				+ " Expires at: " + new Date(expirationDate));
		final StringBuilder b = new StringBuilder();
		try {
			final BufferedReader r = new BufferedReader(new InputStreamReader(
					inputStream, "UTF-8"));
			String l;
			while ((l = r.readLine()) != null) {
				b.append(l);
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		data.put(cacheEntryName, b.toString());
		expirations.put(cacheEntryName, expirationDate);
	}

	@Override
	public boolean isExpired(String cacheEntryName) {
		final boolean exp = expirations.get(cacheEntryName) < System
				.currentTimeMillis();
		System.out.println("MemoryCache.isExpired: " + cacheEntryName + " ? "
				+ exp);
		return exp;
	}

	@Override
	public void clear() {
		data.clear();
		expirations.clear();
	}
}
