/*
 * Developed By: Mohammad Zakaria Chow	dhury
 * Company: Webcraft Bangladesh
 * Email: zakaria.cse@gmail.com
 * Website: http://www.webcraftbd.com
 */

package com.aapbd.worldrelaytune.radio;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.Header;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.R;
import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.ChannelInfo;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

public class RadioMainActivity extends Activity implements OnClickListener {

	private final Handler mHandler = new Handler();
	private TelephonyManager telephonyManager;
	private boolean wasPlayingBeforePhoneCall = false;// /false
	private RadioUpdateReceiver radioUpdateReceiver;

	private String STATUS_BUFFERING;
	private static final String TYPE_AAC = "aac";
	private static final String TYPE_MP3 = "mp3";
	private AudioManager leftAm;
	private SeekBar volControl;
	public static int stationID = 0;
	public static boolean isStationChanged = false;
	int loader;
	ViewFlipper radioViewFlipper;
	Activity newActivty;
	Context con;
	public static RadioMainActivity instance;

	public static RadioMainActivity getInstance() {
		return instance;
	}

	LinearLayout right_side_manu, radio_main;
	View blank_sidemanu;

	TextView buffer, audioTitle, audioBack, audioChannelNumber,
			audioChannelName, audioCountry;

	List<String> imageArray;
	ImageView audioImage;
	boolean isOneImage = true;
	int i = 0;
	TextView audioDes, audioFavorites;
	LinearLayout audioFavLayout;
	DatabaseHandler db;
	ImageView audioFavImg, muteButton, unMuteBtn;
	LinearLayout discriptionLayout, topLayout, bottom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.audio_play);
		con = this;
		newActivty = this;
		instance = this;
		db = new DatabaseHandler(con);
		initUi();

		StaticRadioArray.isFromString = false;
		// imgLoader = new ImageLoader(con);
		loader = R.drawable.icon;
		// Bind to the service
		try {

			if (MyService.getMyService() == null) {
				MyService.startService(con, RadioMainActivity.this);
			}

		} catch (final Exception e) {

			Toast.makeText(getApplicationContext(),
					"Exception" + e.getMessage(), Toast.LENGTH_SHORT).show();

		}

		telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if (telephonyManager != null) {
			telephonyManager.listen(phoneStateListener,
					PhoneStateListener.LISTEN_CALL_STATE);
		}

		Log.w("StaticRadioArray.urls", "are " + StaticRadioArray.urls);

		initialize();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		bottom = (LinearLayout) findViewById(R.id.bottom);
		topLayout = (LinearLayout) findViewById(R.id.topLayout);
		discriptionLayout = (LinearLayout) findViewById(R.id.discriptionLayout);
		unMuteBtn = (ImageView) findViewById(R.id.unMuteBtn);
		muteButton = (ImageView) findViewById(R.id.muteButton);
		audioFavImg = (ImageView) findViewById(R.id.audioFavImg);
		audioChannelNumber = (TextView) findViewById(R.id.audioChannelNumber);
		audioChannelName = (TextView) findViewById(R.id.audioChannelName);
		audioCountry = (TextView) findViewById(R.id.audioCountry);
		audioFavLayout = (LinearLayout) findViewById(R.id.audioFavLayout);
		audioFavorites = (TextView) findViewById(R.id.audioFavorites);
		audioDes = (TextView) findViewById(R.id.audioDes);
		audioImage = (ImageView) findViewById(R.id.audioImage);
		audioBack = (TextView) findViewById(R.id.audioBack);
		audioTitle = (TextView) findViewById(R.id.audioTitle);
		audioTitle.setText(AppConstant.channelInfo.getName());
		audioDes.setText(AppConstant.channelInfo.getChannel_info());

		buffer = (TextView) findViewById(R.id.buffer);
		buffer.setVisibility(View.GONE);

		imageArray = new ArrayList<String>();
		for (int i = 0; i < AppConstant.imageSize; i++) {
			imageArray.add(AppConstant.visual_content_location + "Music" + i
					+ ".jpg");
		}
		volControl = (SeekBar) findViewById(R.id.volControl);
		leftAm = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		final int maxVolume = leftAm
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		final int curVolume = leftAm.getStreamVolume(AudioManager.STREAM_MUSIC);

		volControl.setMax(maxVolume);
		volControl.setProgress(curVolume);

		volControl.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				// TODO Auto-generated method stub

				leftAm.setStreamVolume(AudioManager.STREAM_MUSIC, arg1, 0);

			}
		});

		unMuteBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				volControl.setProgress(maxVolume);
				leftAm.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
			}
		});
		muteButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				volControl.setProgress(0);
				leftAm.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
			}
		});

		mHandler.removeCallbacks(mUpdateTimeTask);
		mHandler.postDelayed(mUpdateTimeTask, 100);
		audioBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StopRadio();
				finish();
			}
		});
		if (AppConstant.channelInfo != null) {
			audioChannelNumber.setText(AppConstant.channelInfo.getWrid());
			audioChannelName.setText(AppConstant.channelInfo.getName());
			List<ChannelInfo> tempInfo = db
					.getCountryFromChannelNumber(AppConstant.channelInfo
							.getWrid());
			Log.e("country Size", "" + tempInfo.size());
			audioCountry.setText(tempInfo.get(0).getCountry());
		}
		if (TextUtils.isEmpty(AppConstant.channelInfo.getChannel_info())) {
			discriptionLayout.setVisibility(View.GONE);
		} else {
			audioDes.setVisibility(View.VISIBLE);
			audioDes.setText(Html.fromHtml(AppConstant.channelInfo
					.getChannel_info()));
		}

		if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {
			audioFavImg.setImageResource(R.drawable.star_fill);
		} else {
			audioFavImg.setImageResource(R.drawable.star_fillas);
		}
		audioFavLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {

					removeFavoritesInfo(AllURL.removeFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				} else {
					addFavoritesInfo(AllURL.addFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				}

			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub

		super.onConfigurationChanged(newConfig);
		Log.e("ONCONFIGCHANGE", "CALLED");
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			discriptionLayout.setVisibility(View.VISIBLE);
			bottom.setVisibility(View.VISIBLE);
			// topLayout.setVisibility(View.VISIBLE);
		} else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			discriptionLayout.setVisibility(View.GONE);
			bottom.setVisibility(View.GONE);
			// topLayout.setVisibility(View.GONE);
		}
	}

	public void initialize() {
		try {

			STATUS_BUFFERING = getResources().getString(
					R.string.status_buffering);

			leftAm = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			leftAm.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			leftAm.getStreamVolume(AudioManager.STREAM_MUSIC);

			final Handler playNow = new Handler();
			playNow.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Log.e("StaticRadioArray.urls", "are"
							+ StaticRadioArray.urls[0]);
					Log.e("StaticRadioArray.urls", "are"
							+ StaticRadioArray.urls[0]);

					MyService.getMyService().play();
					Log.e("Service", "" + MyService.getMyService());
					// updateStatus();
					updateMetadata();
					updateAlbum();
					updatePlayTimer();
					// MyService.getMyService().showNotification();

				}
			}, 2000);

			// displayAd();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private void addFavoritesInfo(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				if (new String(response).equalsIgnoreCase("1")) {
					db.updateFavorites(AppConstant.channelInfo.getWrid());
					audioFavImg.setImageResource(R.drawable.star_fill);
					Toast.makeText(con, "Channel added successfully",
							Toast.LENGTH_LONG).show();

				} else if (new String(response).equalsIgnoreCase("2")) {

					Toast.makeText(
							con,
							"Channel  already a Favorite. Did not add it again",
							Toast.LENGTH_LONG).show();
				} else if (new String(response).equalsIgnoreCase("3")) {

					Toast.makeText(con,
							"10-Channel limit reached. Could not add more. ",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void removeFavoritesInfo(final String url) {

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				if (new String(response).equalsIgnoreCase("1")) {
					db.removeFavorites(AppConstant.channelInfo.getWrid());
					audioFavImg.setImageResource(R.drawable.star_fillas);
					Toast.makeText(con, "Channel removed  successfully",
							Toast.LENGTH_LONG).show();

				} else if (new String(response).equalsIgnoreCase("0")) {

					Toast.makeText(
							con,
							"Channel not removed (not on the list of Favorites).",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	public void onStopRecording() {
		final CharSequence[] items = { getString(R.string.Stayinapp),
				getString(R.string.Exit) };
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.ExitApps));
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0:

					return;

				case 1:

					StopRadio();
					RadioMainActivity.this.finish();
					break;

				default:

					return;
				}

				// Toast.makeText(getApplicationContext(), items[item],
				// Toast.LENGTH_SHORT).show();
			}
		});
		builder.show();
		builder.create();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// volControl = (SeekBar) findViewById(R.id.VolumeSeekBar);
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			final int index = volControl.getProgress();
			volControl.setProgress(index + 1);
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			final int index = volControl.getProgress();
			volControl.setProgress(index - 1);
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {

			StopRadio();
			finish();

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	public void updatePlayTimer() {
		// timeTextView.setText(MyService.getMyService().getPlayingTime());

		final Handler handler = new Handler();
		final Timer timer = new Timer();
		final TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						// timeTextView.setText(MyService.getMyService()
						// .getPlayingTime());
					}
				});
			}
		};
		timer.schedule(doAsynchronousTask, 0, 1000);
	}

	public void StopRadio() {
		MyService.getMyService().stop();
		MyService.stopServiceConnection();

		resetMetadata();
		updateDefaultCoverImage();
	}

	public void onClickPauseButton(View v) {
		MyService.getMyService().pause();
	}

	public void updateDefaultCoverImage() {
		try {
			final String mDrawableName = "station_"
					+ (MyService.getMyService().getCurrentStationID() + 1);
			getResources().getIdentifier(mDrawableName, "drawable",
					getPackageName());

			getResources().getIdentifier("station_default", "drawable",
					getPackageName());

			// if (resID > 0)
			// stationImageView.setImageResource(resID);
			// else
			// stationImageView.setImageResource(resID_default);
			//
			// albumTextView.setText("");
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void updateAlbum() {
		MyService.getMyService().getAlbum();
		final String artist = MyService.getMyService().getArtist();
		final String track = MyService.getMyService().getTrack();
		final Bitmap albumCover = MyService.getMyService().getAlbumCover();

		Log.e("Cover Image", "" + albumCover);
		// artistName.setText(artist);
		// songName.setText(track);
		// albumTextView.setText(album);

		if (albumCover == null || artist.equals("") && track.equals("")) {
			updateDefaultCoverImage();

		} else {
			// coverImg.setImageBitmap(albumCover);
			// stationImageView.setImageBitmap(albumCover);
			MyService.getMyService().setAlbum(LastFMCover.album);

			if (MyService.getMyService().getAlbum().length()
					+ MyService.getMyService().getArtist().length() > 50) {
				// albumTextView.setText("");
			}
		}
	}

	String status;

	public void updateMetadata() {
		String artistfull = "";
		String artist = MyService.getMyService().getArtist();
		status = MyService.getMyService().getStatus();
		buffer.setText("" + status);
		artistfull = artist;
		String track = MyService.getMyService().getTrack();
		if (artist.length() > 30) {
			artist = artist.substring(0, 30) + "...";
		}

		artistfull = artistfull.replaceAll(" ", "%20");
		track = track.replaceAll(" ", "%20");

		if (track != "" || artistfull != "") {

		}

	}

	public void resetMetadata() {
		MyService.getMyService().resetMetadata();
		// artistTextView.setText("");
		// albumTextView.setText("");
		// trackTextView.setText("");
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();

		if (telephonyManager != null) {
			telephonyManager.listen(phoneStateListener,
					PhoneStateListener.LISTEN_NONE);
		}

		Runtime.getRuntime().gc();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (radioUpdateReceiver != null) {
			unregisterReceiver(radioUpdateReceiver);
			// finish();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		/* Register for receiving broadcast messages */
		if (radioUpdateReceiver == null) {
			radioUpdateReceiver = new RadioUpdateReceiver();
		}
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_CREATED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_DESTROYED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_STARTED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_CONNECTING));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_START_PREPARING));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_PREPARED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_PLAYING));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_PAUSED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_STOPPED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_COMPLETED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_ERROR));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_BUFFERING_START));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_BUFFERING_END));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_METADATA_UPDATED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(
				RadioService.MODE_ALBUM_UPDATED));

		if (wasPlayingBeforePhoneCall) {
			MyService.getMyService().play();
			wasPlayingBeforePhoneCall = false;
		}

		if (MyService.getMyService() != null) {
			if (isStationChanged) {
				if (stationID != MyService.getMyService().getCurrentStationID()) {
					MyService.getMyService().stop();
					MyService.getMyService().setCurrentStationID(stationID);
					resetMetadata();
					updateDefaultCoverImage();

				}
				if (!MyService.getMyService().isPlaying()) {
					MyService.getMyService().play();

				}
				isStationChanged = false;
			}
		}

	}

	/* Receive Broadcast Messages from RadioService */
	private class RadioUpdateReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(RadioService.MODE_CREATED)) {

			} else if (intent.getAction().equals(RadioService.MODE_DESTROYED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				updateDefaultCoverImage();
				updateMetadata();
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_STARTED)) {

				// pauseButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				Log.e("MODE_STARTED", "MODE_STARTED..............");

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				//
				// playButton.setEnabled(true);

				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_CONNECTING)) {
				// pauseButton.setEnabled(false);
				// playButton.setVisibility(VRiew.VISIBLE);
				// pauseButton.setVisibility(View.GONE);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);

				Log.e("MODE_CONNECTING", "MODE_CONNECTING..............");

				// playButton.setEnabled(false);

				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_START_PREPARING)) {
				// pauseButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);

				// playButton.setEnabled(false);
				// stopButton.setEnabled(true);
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_PREPARED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				// // pauseButton.setImageResource(R.drawable.load);
				// pauseButton.setImageResource(R.drawable.play_btn);

				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_BUFFERING_START)) {

				Log.e("MODE_BUFFERING_START",
						"MODE_BUFFERING_START..............");

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);

				// pauseButton.startAnimation(AnimationUtils.loadAnimation(
				// newActivty, R.anim.rotate));

				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_BUFFERING_END)) {
				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				// pauseButton.setImageResource(R.drawable.play_btn);
				// pauseButton.clearAnimation();

				Log.e("MODE_BUFFERING_END", "MODE_BUFFERING_END..............");

				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_PLAYING)) {
				if (MyService.getMyService().getCurrentStationType()
						.equals(TYPE_AAC)) {
					// playButton.setEnabled(false);
					// stopButton.setEnabled(true);
				} else {
					// playButton.setEnabled(false);
					// pauseButton.setEnabled(true);
					// stopButton.setEnabled(true);
					// playButton.setVisibility(View.GONE);
					// pauseButton.setVisibility(View.VISIBLE);
					// pauseButton.setImageResource(R.drawable.play_btn);
				}
				// pauseButton.clearAnimation();
				// pauseButton.setImageResource(R.drawable.play_btn);
				Log.e("MODE_PLAYING", "MODE_PLAYING..............");
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_PAUSED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(true);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_STOPPED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_COMPLETED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				// MyService.getMyService().setCurrentStationURL2nextSlot();
				// MyService.getMyService().play();
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_ERROR)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				// pauseButton.clearAnimation();
				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_METADATA_UPDATED)) {
				updateMetadata();
				// updateStatus();
				updateDefaultCoverImage();
			} else if (intent.getAction().equals(
					RadioService.MODE_ALBUM_UPDATED)) {
				updateAlbum();
			}
		}
	}

	// /==========hello=
	PhoneStateListener phoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			if (state == TelephonyManager.CALL_STATE_RINGING) {
				wasPlayingBeforePhoneCall = MyService.getMyService()
						.isPlaying();
				MyService.getMyService().stop();
			} else if (state == TelephonyManager.CALL_STATE_IDLE) {
				if (wasPlayingBeforePhoneCall) {
					MyService.getMyService().play();
				}
			} else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
				// A call is dialing,
				// active or on hold
				wasPlayingBeforePhoneCall = MyService.getMyService()
						.isPlaying();
				MyService.getMyService().stop();
			}
			super.onCallStateChanged(state, incomingNumber);
		}
	};

	public void exitFromApp() {
		final CharSequence[] items = { getString(R.string.Stayinapp),
				getString(R.string.Exit) };
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Exit app?");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0:

					return;

				case 1:
					mHandler.removeCallbacks(mUpdateTimeTask);
					onStopRecording();
					RadioMainActivity.this.finish();
					break;

				default:

					return;
				}

			}
		});
		builder.show();
		builder.create();

	}

	String previousTitle, currentTile = "";
	private final Runnable mUpdateTimeTask = new Runnable() {
		@Override
		public void run() {

			setImageInPicaso();

			mHandler.postDelayed(this, 10000);
		}
	};

	public void setImageInPicaso() {
		// TODO Auto-generated method stub
		if (AppConstant.imageSize == 1) {
			if (isOneImage) {
				Picasso.with(con).load(imageArray.get(0)).into(audioImage);
				isOneImage = false;
			}

		} else {

			Picasso.with(con).load(imageArray.get(i)).into(audioImage);
			i++;
			if (i == (imageArray.size())) {
				i = 0;
			}

		}

	}

}