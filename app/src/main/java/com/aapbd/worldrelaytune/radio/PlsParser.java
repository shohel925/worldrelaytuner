package com.aapbd.worldrelaytune.radio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.util.Log;

public class PlsParser {
	private BufferedReader reader;

	public PlsParser(String url) {
		URLConnection urlConnection = null;
		try {
			urlConnection = new URL(url).openConnection();
			reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));
		} catch (final MalformedURLException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> getUrls() {
		final ArrayList<String> urls = new ArrayList<String>();
		while (true) {
			try {
				final String line = reader.readLine();
				if (line == null) {
					break;
				}
				final String url = parseLine(line);
				if (url != null && !url.equals("")) {
					urls.add(url);
				}
			} catch (final IOException e) {
				return null;
				// e.printStackTrace();
			} catch (final Exception e) {
				return null;
			}
		}
		return urls;
	}

	private String parseLine(String line) {
		if (line == null) {
			return null;
		}
		final String trimmed = line.trim();
		if (trimmed.indexOf("http") >= 0) {
			String url = trimmed.substring(trimmed.indexOf("http"));
			final int end = url.indexOf("<");
			if (end > 0) {
				url = url.substring(0, end);
			}

			Log.d("PlsParser", "Playlist Url = " + url);
			return url;
		}
		return "";
	}
}
