package com.aapbd.worldrelaytune.radio.lastfm;

/**
 * @author Janni Kovacs
 */
public enum ImageSize {

	SMALL, MEDIUM, LARGE, LARGESQUARE, HUGE, EXTRALARGE, MEGA, ORIGINAL

}
