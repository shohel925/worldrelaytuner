package com.aapbd.worldrelaytune.radio.lastfm;

import static com.aapbd.worldrelaytune.radio.util.StringUtilities.isMD5;
import static com.aapbd.worldrelaytune.radio.util.StringUtilities.map;
import static com.aapbd.worldrelaytune.radio.util.StringUtilities.md5;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.aapbd.worldrelaytune.radio.xml.DomElement;

/**
 * Provides bindings for the authentication methods of the last.fm API. See <a
 * href="http://www.last.fm/api/authentication">http://www.last.fm/api/
 * authentication</a> for authentication methods.
 *
 * @author Janni Kovacs
 * @see Session
 */
public class Authenticator {

	private Authenticator() {
	}

	/**
	 * Create a web service session for a user. Used for authenticating a user
	 * when the password can be inputted by the user.
	 *
	 * @param username
	 *            last.fm username
	 * @param password
	 *            last.fm password in cleartext or 32-char md5 string
	 * @param apiKey
	 *            The API key
	 * @param secret
	 *            Your last.fm API secret
	 * @return a Session instance
	 * @see Session
	 */
	public static Session getMobileSession(String username, String password,
			String apiKey, String secret) {
		if (!isMD5(password)) {
			password = md5(password);
		}
		final String authToken = md5(username + password);
		final Map<String, String> params = map("api_key", apiKey, "username",
				username, "authToken", authToken);
		final String sig = createSignature("auth.getMobileSession", params,
				secret);
		final Result result = Caller.getInstance().call(
				"auth.getMobileSession", apiKey, "username", username,
				"authToken", authToken, "api_sig", sig);
		final DomElement element = result.getContentElement();
		return Session.sessionFromElement(element, apiKey, secret);
	}

	/**
	 * Fetch an unathorized request token for an API account.
	 *
	 * @param apiKey
	 *            A last.fm API key.
	 * @return a token
	 */
	public static String getToken(String apiKey) {
		final Result result = Caller.getInstance()
				.call("auth.getToken", apiKey);
		return result.getContentElement().getText();
	}

	/**
	 * Fetch a session key for a user.
	 *
	 * @param token
	 *            A token returned by {@link #getToken(String)}
	 * @param apiKey
	 *            A last.fm API key
	 * @param secret
	 *            Your last.fm API secret
	 * @return a Session instance
	 * @see Session
	 */
	public static Session getSession(String token, String apiKey, String secret) {
		final String m = "auth.getSession";
		final Map<String, String> params = new HashMap<String, String>();
		params.put("api_key", apiKey);
		params.put("token", token);
		params.put("api_sig", createSignature(m, params, secret));
		final Result result = Caller.getInstance().call(m, apiKey, params);
		return Session.sessionFromElement(result.getContentElement(), apiKey,
				secret);
	}

	static String createSignature(String method, Map<String, String> params,
			String secret) {
		params = new TreeMap<String, String>(params);
		params.put("method", method);
		final StringBuilder b = new StringBuilder(100);
		for (final Entry<String, String> entry : params.entrySet()) {
			b.append(entry.getKey());
			b.append(entry.getValue());
		}
		b.append(secret);
		return md5(b.toString());
	}
}
