package com.aapbd.worldrelaytune.radio.lastfm.cache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import com.aapbd.worldrelaytune.radio.lastfm.scrobble.Scrobbler;
import com.aapbd.worldrelaytune.radio.lastfm.scrobble.SubmissionData;

/**
 * Standard {@link Cache} implementation which is used by default by the
 * {@link com.aapbd.radio.lastfm.Caller} class. This implementation caches all
 * responses in the file system. In addition to the raw responses it stores a
 * .meta file which contains the expiration date for the specified request.
 *
 * @author Janni Kovacs
 */
@SuppressWarnings({ "all" })
public class FileSystemCache extends Cache implements ScrobbleCache {

	private static final String SUBMISSIONS_FILE = "submissions.txt";

	private File cacheDir;

	public FileSystemCache() {
		this(new File(System.getProperty("user.home") + "/.last.fm-cache"));
	}

	public FileSystemCache(File cacheDir) {
		this.cacheDir = cacheDir;
	}

	@Override
	public boolean contains(String cacheEntryName) {
		return new File(cacheDir, cacheEntryName + ".xml").exists();
	}

	@Override
	public void remove(String cacheEntryName) {
		new File(cacheDir, cacheEntryName + ".xml").delete();
		new File(cacheDir, cacheEntryName + ".meta").delete();
	}

	@Override
	public boolean isExpired(String cacheEntryName) {
		final File f = new File(cacheDir, cacheEntryName + ".meta");
		if (!f.exists()) {
			return false;
		}
		try {
			final Properties p = new Properties();
			p.load(new FileInputStream(f));
			final long expirationDate = Long.valueOf(p
					.getProperty("expiration-date"));
			return expirationDate < System.currentTimeMillis();
		} catch (final IOException e) {
			return false;
		}
	}

	@Override
	public void clear() {
		for (final File file : cacheDir.listFiles()) {
			if (file.isFile()) {
				file.delete();
			}
		}
	}

	@Override
	public InputStream load(String cacheEntryName) {
		try {
			return new FileInputStream(new File(cacheDir, cacheEntryName
					+ ".xml"));
		} catch (final FileNotFoundException e) {
			return null;
		}
	}

	@Override
	public void store(String cacheEntryName, InputStream inputStream,
			long expirationDate) {
		createCache();
		final File f = new File(cacheDir, cacheEntryName + ".xml");
		try {
			final BufferedInputStream is = new BufferedInputStream(inputStream);
			final BufferedOutputStream os = new BufferedOutputStream(
					new FileOutputStream(f));
			int read;
			final byte[] buffer = new byte[4096];
			while ((read = is.read(buffer)) != -1) {
				os.write(buffer, 0, read);
			}
			os.close();
			is.close();
			final File fm = new File(cacheDir, cacheEntryName + ".meta");
			final Properties p = new Properties();
			p.setProperty("expiration-date", Long.toString(expirationDate));
			p.store(new FileOutputStream(fm), null);
		} catch (final IOException e) {
			// we ignore the exception. if something went wrong we just don't
			// cache it.
		}
	}

	private void createCache() {
		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
			if (!cacheDir.isDirectory()) {
				cacheDir = cacheDir.getParentFile();
			}
		}
	}

	@Override
	public void cacheScrobble(Collection<SubmissionData> submissions) {
		cacheScrobble(submissions
				.toArray(new SubmissionData[submissions.size()]));
	}

	@Override
	public void cacheScrobble(SubmissionData... submissions) {
		createCache();
		try {
			final BufferedWriter w = new BufferedWriter(new FileWriter(
					new File(cacheDir, SUBMISSIONS_FILE), true));
			for (final SubmissionData submission : submissions) {
				w.append(submission.toString());
				w.newLine();
			}
			w.close();
		} catch (final IOException e) {
			// huh ?
			// e.printStackTrace();
		}
	}

	@Override
	public boolean isEmpty() {
		final File file = new File(cacheDir, SUBMISSIONS_FILE);
		if (!file.exists()) {
			return true;
		}
		try {
			final BufferedReader r = new BufferedReader(new FileReader(file));
			final String line = r.readLine();
			r.close();
			return line == null || "".equals(line);
		} catch (final IOException e) {
			// huh
			// e.printStackTrace();
		}
		return true;
	}

	@Override
	public void scrobble(Scrobbler scrobbler) throws IOException {
		final File file = new File(cacheDir, SUBMISSIONS_FILE);
		if (file.exists()) {
			final BufferedReader r = new BufferedReader(new FileReader(file));
			final List<SubmissionData> list = new ArrayList<SubmissionData>(50);
			String line;
			while ((line = r.readLine()) != null) {
				final SubmissionData d = new SubmissionData(line);
				list.add(d);
				if (list.size() == 50) {
					scrobbler.submit(list);
					list.clear();
				}
			}
			if (list.size() > 0) {
				scrobbler.submit(list);
			}
			r.close();
			final FileWriter w = new FileWriter(file);
			w.close();
		}
	}

	@Override
	public void clearScrobbleCache() {
		final File file = new File(cacheDir, SUBMISSIONS_FILE);
		file.delete();
	}
}
