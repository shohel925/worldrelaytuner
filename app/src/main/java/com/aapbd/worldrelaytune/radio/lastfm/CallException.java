package com.aapbd.worldrelaytune.radio.lastfm;

/**
 * @author Janni Kovacs
 */
public class CallException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public CallException() {
	}

	public CallException(Throwable cause) {
		super(cause);
	}

	public CallException(String message) {
		super(message);
	}

	public CallException(String message, Throwable cause) {
		super(message, cause);
	}
}
