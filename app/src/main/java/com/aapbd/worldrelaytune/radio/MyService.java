package com.aapbd.worldrelaytune.radio;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.IBinder;
import android.util.Log;

public class MyService {

	private static Context context = null;
	public static Intent bindIntent;
	public static RadioService radioService;

	public static void startService(Context con, Activity nActivity) {
		MyService.context = con;

		// Bind to the service
		bindIntent = new Intent(context, RadioService.class);
		MyService.context.bindService(bindIntent, radioConnection,
				Context.BIND_AUTO_CREATE);
		nActivity.setVolumeControlStream(AudioManager.STREAM_MUSIC);

	}

	public static RadioService getMyService() {
		return MyService.radioService;
	}

	public static void setMyService(RadioService service) {
		MyService.radioService = service;
	}

	// Handles the connection between the service and activity
	private final static ServiceConnection radioConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			radioService = ((RadioService.RadioBinder) service).getService();
			Log.e("at my service", "radio service is connected");
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			radioService = null;
		}
	};

	public static void stopServiceConnection() {
		try {

			MyService.context.unbindService(MyService.radioConnection);
		} catch (final Exception e) {

		}

	}

}
