package com.aapbd.worldrelaytune;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.YoutubeSnippet;
import com.aapbd.worldrelaytune.model.YoutubesResponse;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;

public class YoutubePlayListActivity extends Activity {
	Context con;
	Activity act;
	GridView youtubeGridView;
	GridAdapter gGridAdapter;
	Dialog dd;
	TextView mainTitle;
	Typeface tf3;
	DatabaseHandler db;
	YoutubesResponse youtubesResponse;
	ImageView youtubeFavorites;
	TextView youtubeTitle;
	LinearLayout youtubeBack;
	TextView searchTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.youtube_playlist);
		con = this;
		act = this;
		db = new DatabaseHandler(con);
		//AnalyticsTracker.sendTrackData(this, R.string.app_name);
		initUI();

	}

	private void initUI() {
		youtubeBack=(LinearLayout)findViewById(R.id.youtubeBack);
		youtubeBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		//actionBarSetup();
		youtubeTitle=(TextView)findViewById(R.id.youtubeTitle);
		youtubeTitle.setText(AppConstant.channelName);
		getChannelInfo(AllURL.getAllYoutubePlayList(AppConstant.youtubeName));

		youtubeGridView = (GridView) findViewById(R.id.youtubeGridView);

		tf3 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");

	}

	public void searchImg(View v) {
		searchDialoge();
	}

	public void searchDialoge() {
		dd = new Dialog(con);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.search_dialoge_cancel_search);
		dd.show();

		EditText et_search = (EditText) dd.findViewById(R.id.et_search);
		ViewGroup btn_cancel = (ViewGroup) dd.findViewById(R.id.btn_cancel);
		ViewGroup btn_search = (ViewGroup) dd.findViewById(R.id.btn_search);
		TextView btn_sign = (TextView) dd.findViewById(R.id.btn_sign);
		TextView searchTv = (TextView) dd.findViewById(R.id.searchTv);

		btn_sign.setTypeface(tf3);
		et_search.setTypeface(tf3);
		searchTv.setTypeface(tf3);
		btn_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dd.dismiss();
			}
		});

	}

	private void actionBarSetup() {
		// TODO Auto-generated method stub

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.youtube_top);

		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
				R.id.signtitle);
		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
				R.id.backImg);
		LinearLayout favorite = (LinearLayout) actionBar.getCustomView()
				.findViewById(R.id.favorite);
		youtubeFavorites = (ImageView) actionBar.getCustomView().findViewById(
				R.id.youtubeFavorites);
		signtitle.setText(AppConstant.channelName);

		if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {
			youtubeFavorites.setImageResource(R.drawable.star_fill);
		} else {
			youtubeFavorites.setImageResource(R.drawable.star_fillas);
		}

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");

		signtitle.setTypeface(tf);

		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		favorite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {

					removeFavoritesInfo(AllURL.removeFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				} else {
					addFavoritesInfo(AllURL.addFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				}

			}
		});

	}

	private void removeFavoritesInfo(final String url) {

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				if (new String(response).equalsIgnoreCase("1")) {
					db.removeFavorites(AppConstant.channelInfo.getWrid());
					youtubeFavorites.setImageResource(R.drawable.star_fillas);
					Toast.makeText(con, "Channel removed  successfully",
							Toast.LENGTH_LONG).show();

				} else if (new String(response).equalsIgnoreCase("0")) {

					Toast.makeText(
							con,
							"Channel not removed (not on the list of Favorites).",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void addFavoritesInfo(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				if (new String(response).equalsIgnoreCase("1")) {
					db.updateFavorites(AppConstant.channelInfo.getWrid());
					youtubeFavorites.setImageResource(R.drawable.star_fill);
					Toast.makeText(con, "Channel added successfully",
							Toast.LENGTH_LONG).show();

				} else if (new String(response).equalsIgnoreCase("2")) {

					Toast.makeText(
							con,
							"Channel  already a Favorite. Did not add it again",
							Toast.LENGTH_LONG).show();
				} else if (new String(response).equalsIgnoreCase("3")) {

					Toast.makeText(con,
							"10-Channel limit reached. Could not add more. ",
							Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getChannelInfo(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));
				Gson g = new Gson();
				youtubesResponse = g.fromJson(new String(response),
						YoutubesResponse.class);

				if (youtubesResponse.getItems().size() > 0) {

					for (YoutubeSnippet yInfo : youtubesResponse.getItems()) {
						Log.e("image", ">>"
								+ yInfo.getSnippet().getThumbnails().getHigh()
										.getUrl());
						Log.e("Video id", ">>"
								+ yInfo.getSnippet().getResourceId()
										.getVideoId());
					}

					gGridAdapter = new GridAdapter(con);
					youtubeGridView.setAdapter(gGridAdapter);
					gGridAdapter.notifyDataSetChanged();
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private class GridAdapter extends ArrayAdapter<YoutubeSnippet> {
		Context context;

		GridAdapter(Context context) {
			super(context, R.layout.row_playlist, youtubesResponse.getItems());

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row_playlist, null);

			}

			if (position < youtubesResponse.getItems().size()) {

				final YoutubeSnippet query = youtubesResponse.getItems().get(
						position);

				final ImageView neaPeopleName = (ImageView) v
						.findViewById(R.id.favoriteImg);
				final TextView playName = (TextView) v
						.findViewById(R.id.playName);

				playName.setText(query.getSnippet().getTitle());

				if (!TextUtils.isEmpty(query.getSnippet().getThumbnails()
						.getHigh().getUrl())) {
					Picasso.with(con)
							.load(query.getSnippet().getThumbnails().getHigh()
									.getUrl()).error(R.drawable.ic_launcher)
							.into(neaPeopleName);
				} else {
					neaPeopleName.setImageResource(R.drawable.discover_14);
				}

				playName.setTypeface(tf3);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final YoutubeSnippet query = youtubesResponse.getItems()
							.get(position);

					Log.e("Video id", ""
							+ query.getSnippet().getResourceId().getVideoId());
					AppConstant.youtubeVideoUrl = query.getSnippet()
							.getResourceId().getVideoId();
					AppConstant.channelName = query.getSnippet().getTitle();
					StartActivity.toActivity(con, YoutubePlayActivity.class);
				}
			});

			return v;

		}

	}

}
