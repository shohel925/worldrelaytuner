package com.aapbd.worldrelaytune.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.aapbd.worldrelaytune.R;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

/**
 * HTTPHandler This class handles POST and GET requests and enables you to
 * upload files via post. Cookies are stored in the HttpClient.
 *
 * @author Sander Borgman
 * @url http://www.sanderborgman.nl
 * @version 1
 */
public class HTTPHandler {

	/*
	 *
	 * do log in
	 */

	/*
	 * get https client
	 */
	public static DefaultHttpClient getClient(Context con) {

		DefaultHttpClient ret = null;
		// sets up parameters
		final HttpParams params = new BasicHttpParams();

		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

		HttpProtocolParams.setUserAgent(params, "PokerBuddies"); // new addedr

		HttpProtocolParams.setContentCharset(params, "utf-8");
		params.setBooleanParameter("http.protocol.expect-continue", false);
		// registers schemes for both http and https

		KeyStore trustStore = null;
		try {
			trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			try {
				trustStore.load(null, null);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (CertificateException e) {
				e.printStackTrace();
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		MySSLSocketFactory sf = null;
		final SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));

		try {
			sf = new MySSLSocketFactory(trustStore);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		}
		sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		registry.register(new Scheme("https", createAdditionalCertsSSLSocketFactory(con), 443));
		final ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(
				params, registry);
		ret = new DefaultHttpClient(manager, params);
		return ret;

	}
	public static DefaultHttpClient getClient() {

		DefaultHttpClient ret = null;
		// sets up parameters
		final HttpParams params = new BasicHttpParams();

		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

		HttpProtocolParams.setUserAgent(params, "PokerBuddies"); // new addedr

		HttpProtocolParams.setContentCharset(params, "utf-8");
		params.setBooleanParameter("http.protocol.expect-continue", false);
		// registers schemes for both http and https

		KeyStore trustStore = null;
		try {
			trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			try {
				trustStore.load(null, null);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (CertificateException e) {
				e.printStackTrace();
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		MySSLSocketFactory sf = null;
		final SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));

		try {
			sf = new MySSLSocketFactory(trustStore);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		}
		sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		registry.register(new Scheme("https", sf, 443));
		final ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(
				params, registry);
		ret = new DefaultHttpClient(manager, params);
		return ret;

	}

	public static org.apache.http.conn.ssl.SSLSocketFactory createAdditionalCertsSSLSocketFactory(Context context) {
		try {
			final KeyStore ks = KeyStore.getInstance("BKS");

			// the bks file we generated above
			final InputStream in = context.getResources().openRawResource( R.raw.keydata);
			try {
				// don't forget to put the password used above in strings.xml/mystore_password
				ks.load(in, context.getString( R.string.mystore_password ).toCharArray());
			} finally {
				in.close();
			}

			return new AdditionalKeyStoresSSLSocketFactory(ks);

		} catch( Exception e ) {
			throw new RuntimeException(e);
		}
	}

	public static class AdditionalKeyStoresSSLSocketFactory extends SSLSocketFactory {
		public SSLContext sslContext = SSLContext.getInstance("TLS");

		public AdditionalKeyStoresSSLSocketFactory(KeyStore keyStore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
			super(null, null, null, null, null, null);
			sslContext.init(null, new TrustManager[]{new AdditionalKeyStoresTrustManager(keyStore)}, null);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
			return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
		}

		@Override
		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}

	/*
	 * get data as string
	 */

//	public static String getData(final HttpPost httpost) throws IOException,
//			URISyntaxException {
//
//		String inputLine = "Error";
//		final StringBuffer buf = new StringBuffer();
//
//		{
//
//			InputStream ins = null;
//
//			ins = HTTPHandler.getInputStreamPOST(httpost);
//
//			final InputStreamReader isr = new InputStreamReader(ins);
//			final BufferedReader in = new BufferedReader(isr);
//
//			while ((inputLine = in.readLine()) != null) {
//				buf.append(inputLine);
//			}
//
//			in.close();
//
//		}
//
//		return buf.toString();
//
//	}

	public static InputStream getInputStreamGET(final String url,Context con)
			throws URISyntaxException, ClientProtocolException, IOException {
		final DefaultHttpClient httpClient = HTTPHandler.getClient(con);

		// print.message("URL is ", url+"");

		URI uri;
		uri = new URI(url);
		final HttpGet method = new HttpGet(uri);

		method.addHeader("Accept-Encoding", "gzip");
		method.addHeader("Accept-Encoding", "utf-8");

		/*
		 *
		 * send cookie
		 */

		if (Cookies.getCookies() != null) {

			System.out.println("Cookie is added to client");

			for (final Cookie cok : Cookies.getCookies()) {
				httpClient.getCookieStore().addCookie(cok);
			}
		} else {
			System.out.println("Cookie is empty");
		}

		final HttpResponse res = httpClient.execute(method);

		final List<Cookie> cookies = httpClient.getCookieStore().getCookies();

		if (cookies.isEmpty()) {
			System.out.println("None");
		} else {

			Cookies.setCookies(cookies);

			System.out.println("size of cokies " + cookies.size());

			for (int i = 0; i < cookies.size(); i++) {
				Log.e("- " + cookies.get(i).toString(), "");
			}
		}

		final String code = res.getStatusLine().toString();
		Log.w("status line ", code);

		InputStream instream = res.getEntity().getContent();
		final Header contentEncoding = res.getFirstHeader("Content-Encoding");
		if (contentEncoding != null
				&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
			instream = new GZIPInputStream(instream);
		}

		return instream;
	}
	public static InputStream getInputStreamGET(final String url)
			throws URISyntaxException, ClientProtocolException, IOException {
		final DefaultHttpClient httpClient = HTTPHandler.getClient();

		// print.message("URL is ", url+"");

		URI uri;
		uri = new URI(url);
		final HttpGet method = new HttpGet(uri);

		method.addHeader("Accept-Encoding", "gzip");
		method.addHeader("Accept-Encoding", "utf-8");

		/*
		 *
		 * send cookie
		 */

		if (Cookies.getCookies() != null) {

			System.out.println("Cookie is added to client");

			for (final Cookie cok : Cookies.getCookies()) {
				httpClient.getCookieStore().addCookie(cok);
			}
		} else {
			System.out.println("Cookie is empty");
		}

		final HttpResponse res = httpClient.execute(method);

		final List<Cookie> cookies = httpClient.getCookieStore().getCookies();

		if (cookies.isEmpty()) {
			System.out.println("None");
		} else {

			Cookies.setCookies(cookies);

			System.out.println("size of cokies " + cookies.size());

			for (int i = 0; i < cookies.size(); i++) {
				Log.e("- " + cookies.get(i).toString(), "");
			}
		}

		final String code = res.getStatusLine().toString();
		Log.w("status line ", code);

		InputStream instream = res.getEntity().getContent();
		final Header contentEncoding = res.getFirstHeader("Content-Encoding");
		if (contentEncoding != null
				&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
			instream = new GZIPInputStream(instream);
		}

		return instream;
	}

	public static String GetText(final InputStream in) throws IOException {
		String text = "";

		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				in, "UTF-8"));
		final StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			text = sb.toString();

		} finally {

			in.close();

		}
		return text;
	}

	/*
	 * get inputstream for get request
	 */

	/*
	 * get input stream
	 */
//	public static InputStream getInputStreamPOST(final HttpPost httpost)
//			throws URISyntaxException, ClientProtocolException, IOException {
//
//		final HttpClient client = HTTPHandler.getClient();
//
//		/*
//		 *
//		 * send cookie
//		 */
//
//		if (Cookies.getCookies() != null) {
//
//			System.out.println("Cookie is added to client");
//
//			for (final Cookie cok : Cookies.getCookies()) {
//				((DefaultHttpClient) client).getCookieStore().addCookie(cok);
//			}
//		} else {
//			System.out.println("Cookie is empty");
//		}
//
//		final HttpResponse res = client.execute(httpost);
//
//		System.out.println("Login form get: " + res.getStatusLine());
//
//		System.out.println("get login cookies:");
//		final List<Cookie> cookies = ((DefaultHttpClient) client)
//				.getCookieStore().getCookies();
//
//		if (cookies.isEmpty()) {
//			System.out.println("None");
//		} else {
//
//			Cookies.setCookies(cookies);
//
//			System.out.println("size of cokies " + cookies.size());
//
//			for (int i = 0; i < cookies.size(); i++) {
//				Log.e("- " + cookies.get(i).toString(), "");
//			}
//		}
//
//		// Log.w("response",res.g)
//
//		return res.getEntity().getContent();
//	}

//	public static String GetPostDataFromURL(HTTPPostHelper helper) {
//
//		final HttpPost httpost = new HttpPost(helper.getURL().trim());
//
//		// httpost.addHeader("email",AppConstant.Email);
//		// httpost.addHeader("token",AppConstant.Token);
//
//		final String credentials = AppConstant.Email + ":" + AppConstant.Token;
//		final String base64EncodedCredentials = Base64.encodeToString(
//				credentials.getBytes(), Base64.NO_WRAP);
//		httpost.addHeader("Authorization", "Basic " + base64EncodedCredentials);
//
//		Log.d("Log in URL is ", helper.getURL().trim() + "");
//
//		try {
//			httpost.setEntity(new UrlEncodedFormEntity(helper.getNvps(),
//					HTTP.UTF_8));
//
//			return HTTPHandler.getData(httpost);
//
//		} catch (final UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (final IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (final URISyntaxException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return "error";
//
//	}

//	public static String doGetWithAuthentication(String URL,
//			boolean isAuthentic, String email, String token)
//			throws ClientProtocolException, IOException, JSONException {
//		final HttpClient httpClient = HTTPHandler.getClient();
//		try {
//			httpClient.getParams().setParameter(
//					CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
//			httpClient.getParams().setParameter("http.socket.timeout",
//					new Integer(90000)); // 90 second
//			final HttpGet httpGet = new HttpGet(URL);
//
//			if (isAuthentic) {
//				final String credentials = email + ":" + token;
//				final String base64EncodedCredentials = Base64.encodeToString(
//						credentials.getBytes(), Base64.NO_WRAP);
//				httpGet.addHeader("Authorization", "Basic "
//						+ base64EncodedCredentials);
//			}
//			/*
//			 * if not empty
//			 */
//
//			final HttpResponse lResponse = httpClient.execute(httpGet);
//
//			Log.w("HTTP RESPONSE CODE", lResponse.getStatusLine()
//					.getStatusCode() + "");
//
//			if (lResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
//				Log.e("PostWithJson-uploadFile", "Response Status line code:"
//						+ lResponse.getStatusLine());
//			} else {
//				Log.e("HTTP RESPONSE CODE ", lResponse
//						.getStatusLine().getStatusCode() + "");
//			}
//
//			InputStream instream = lResponse.getEntity().getContent();
//			final Header contentEncoding = lResponse
//					.getFirstHeader("Content-Encoding");
//			if (contentEncoding != null
//					&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
//				instream = new GZIPInputStream(instream);
//			}
//
//			return HTTPHandler.GetText(instream);
//
//		} finally {
//			httpClient.getConnectionManager().shutdown();
//		}
//	}

}