/**
 *
 */
package com.aapbd.worldrelaytune.utils;

import android.app.Activity;

import com.aapbd.worldrelaytune.MyApplication;
import com.aapbd.worldrelaytune.MyApplication.TrackerName;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * @author Ashraful
 *
 */
public class AnalyticsTracker {

	public static void sendTrackData(Activity activity, int strId) {
		// Get tracker.
		final Tracker t = ((MyApplication) activity.getApplication()).getTracker(TrackerName.APP_TRACKER);
		final String screenName = activity.getString(strId);
		// Set screen name.
		t.set(screenName, screenName);
		t.setScreenName(screenName);
		t.enableAdvertisingIdCollection(true);

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());
	}

	public static void sendTrackData(Activity activity, String  screenName, String title ) {
		// Get tracker.
		final Tracker t = ((MyApplication) activity.getApplication()).getTracker(TrackerName.APP_TRACKER);

		// Set screen name.

		t.setScreenName(screenName);
		t.set("View details", title);
		t.enableAdvertisingIdCollection(true);

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());
	}

	public static void sendEventData(Activity activity, String  category, String action ,String label) {
		// Get tracker.
		final Tracker t = ((MyApplication) activity.getApplication()).getTracker(TrackerName.APP_TRACKER);
		t.setSessionTimeout(10);
//		t.send(new HitBuilders.TimingBuilder().setCategory(category).setValue(30).setVariable(action).setLabel(label).build());
		t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
	}

}
