package com.aapbd.worldrelaytune.utils;

public class BaseURL {

//	public static String HTTP = "https://www.worldrelay.tv/ws/";
public static String HTTP = "http://www.worldrelay.tv/ws/";

	// public static String ImagePath = "http://aapbd.com/yawper/uploads/";

	/**
	 * @return the hTTP
	 */
	public static String makeHTTPURL(String param) {
		return BaseURL.HTTP + UrlUtils.encode(param);
	}

	/**
	 * @param hTTP
	 *            the hTTP to set
	 */
	public static void setHTTP(final String hTTP) {
		BaseURL.HTTP = hTTP;
	}

}
