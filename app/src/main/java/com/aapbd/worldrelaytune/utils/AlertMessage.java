package com.aapbd.worldrelaytune.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.VideoView;

import com.aapbd.worldrelaytune.R;
import com.aapbd.worldrelaytune.VideoPlayActivity;

public class AlertMessage {

	/*
	 * show alert dialog P: context, title and message
	 */
	public static void showMessage(final Context c, final String title,
			final String message) {

		final Dialog dd = new Dialog(c);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.dialogue);
		dd.show();

		TextView dialogeStatus = (TextView) dd.findViewById(R.id.dialogeStatus);
		TextView errorMsg = (TextView) dd.findViewById(R.id.errorMsg);
		TextView okBtn = (TextView) dd.findViewById(R.id.okBtn);
		dialogeStatus.setText(title);
		errorMsg.setText(message);
		okBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dd.dismiss();
			}

		});
	}

	public static void showPlayError(final Activity ac,final Context c, final String title,
								   final String message) {

		final Dialog dd = new Dialog(c);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.dialogue);
		dd.show();

		TextView dialogeStatus = (TextView) dd.findViewById(R.id.dialogeStatus);
		TextView errorMsg = (TextView) dd.findViewById(R.id.errorMsg);
		TextView okBtn = (TextView) dd.findViewById(R.id.okBtn);
		dialogeStatus.setText(title);
		errorMsg.setText(message);
		okBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ac.finish();
				dd.dismiss();
			}

		});
	}
	public static void showPlayErrorDemand(final Context c, final String title,
									 final String message, final VideoView view,final ListView list) {

		final Dialog dd = new Dialog(c);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.dialogue);
		dd.show();

		TextView dialogeStatus = (TextView) dd.findViewById(R.id.dialogeStatus);
		TextView errorMsg = (TextView) dd.findViewById(R.id.errorMsg);
		TextView okBtn = (TextView) dd.findViewById(R.id.okBtn);
		dialogeStatus.setText(title);
		errorMsg.setText(message);
		okBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				view.setVisibility(View.GONE);
				list.setVisibility(View.VISIBLE);
				dd.dismiss();
			}

		});
	}

	/*
	 * show alert dialog P: context, title and message
	 */

	public static void showMessage(final Context c, int icon,
			final String title, final String message) {
		final AlertDialog.Builder aBuilder = new AlertDialog.Builder(c);
		aBuilder.setTitle(title);
		aBuilder.setIcon(R.drawable.ic_launcher);
		aBuilder.setMessage(message);

		aBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
			}

		});

		aBuilder.show();
	}

	/*
	 * overloaded method with button text
	 */

	public static void showMessage(final Context c, final String title,
			final String message, String buttonText) {
		final AlertDialog.Builder aBuilder = new AlertDialog.Builder(c);
		aBuilder.setTitle(title);
		aBuilder.setIcon(R.drawable.ic_launcher);
		aBuilder.setMessage(message);

		aBuilder.setPositiveButton(buttonText,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(final DialogInterface dialog,
							final int which) {
						dialog.dismiss();
					}

				});

		aBuilder.show();
	}

	/*
	 * overloaded method with icon
	 */
	public static void showMessage(final Context c, final String title,
			final String message, String buttonText, int icon) {
		final AlertDialog.Builder aBuilder = new AlertDialog.Builder(c);
		aBuilder.setTitle(title);
		aBuilder.setIcon(icon);
		aBuilder.setMessage(message);

		aBuilder.setPositiveButton(buttonText,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(final DialogInterface dialog,
							final int which) {
						dialog.dismiss();
					}

				});

		aBuilder.show();
	}

}
