package com.aapbd.worldrelaytune.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.aapbd.worldrelaytune.model.AllChannelGeneInfo;
import com.aapbd.worldrelaytune.model.AllChannelTypesInfo;
import com.aapbd.worldrelaytune.model.AllCountryInfo;
import com.aapbd.worldrelaytune.model.AllLanguageInfo;
import com.aapbd.worldrelaytune.model.CategoryTypeInfo;
import com.aapbd.worldrelaytune.model.ChannelInfo;
import com.aapbd.worldrelaytune.model.PublicChannelInfo;

public class AppConstant {
	public static String urerId = "urerId";
	public static String user_name = "user_name";
	public static String fullName = "fullName";
	public static String singinHide = "singinHide";
	public static String TWITTER_KEY = "GdwYOj9PjJrpc9rfBs8T8w";
	public static String TWITTER_SECRET_KEY = "7Bz46a1bdcnJ2VA9qd9Od5QtzzfBuIrQd1U7p7sw";
	public static final String TWITTER_CALLBACK_URL = "http://shohel.co.nf/";

	public static final String IEXTRA_AUTH_URL = "auth_url";
	public static final String IEXTRA_OAUTH_VERIFIER = "oauth_verifier";
	public static final String IEXTRA_OAUTH_TOKEN = "oauth_token";

	public static final String SHARED_PREF_NAME = "demo_twitter_oauth";
	public static final String SHARED_PREF_KEY_SECRET = "demo_oauth_token_secret";
	public static final String SHARED_PREF_KEY_TOKEN = "demo_oauth_token";
	public static final String TWITTER_LOGIN_ERROR = "TwitterLoginError";
	public static String twitterUserId = "twitterUserId";
	public static String twitterUserImage = "twitterUserImage";
	public static String twitterUserName = "twitterUserName";
	public static String twitterUserFirstName = "twitterUserFirstName";
	public static String twitterUserLastName = "twitterUserLastName";
	public static boolean isTwitterLogin = false;

	public static List<AllCountryInfo> allCountryInfo;

	public static List<AllLanguageInfo> allLanguageInfo;

	public static List<AllChannelGeneInfo> allGenereinfo;
	public static List<AllChannelTypesInfo> allChannelTypesInfo;
	public static PublicChannelInfo channelInfo = null;
	public static String webUrl = "";
	public static String youtubeName = "";
	public static String googleAppKey = "";
	public static String channelName = "";
	public static String youtubeVideoUrl = "";
	public static boolean flag = true;
	public static Boolean buttonAction = false;
	public static int imageSize = 0;
	public static String visual_content_location = "";
	public static String Email = "";
	public static String Token = "";
	public static String videoTitle = "";
	public static String videoDescription = "";
	public static boolean fromVimeoVideo = false;
	public static String categoryName = "";
	public static String searchTxt = "";
	public static boolean searchChannelPress = false;
	public static boolean clickCategoryType = false;
	public static String categoryType = "";
	public static List<CategoryTypeInfo> categoryTypeList = null;
	public static String firstHead = "firstHead";
	public static String previousTime = "previousTime";
	public static List<ChannelInfo> categoryList = null;
	public static List<CategoryTypeInfo> categoryGenreList = null;
	public static List<CategoryTypeInfo> categoryCountryList = null;
	public static List<CategoryTypeInfo> categoryLanguageList = null;
	public static String typeCheck = "";
	public static List<PublicChannelInfo> tempPublicList=null;
	public static String discoverType = "";
	public static List<PublicChannelInfo> searchList = new ArrayList<PublicChannelInfo>();
	public static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	public static String shareTxt = "";
	public static String twitterId = "twitterId";
	public static boolean videoTwitterButtonClick = false;
	public static String videoSource="";
	public static String vimeoChannelId="";
	public static boolean fromAudio=false;
	public static boolean fromMore=false;
	public static String ondemendSOurce="";
}
