package com.aapbd.worldrelaytune.utils;

import java.util.Vector;

import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class AllURL {

	private static String getcommonURLWithParamAndAction(String action,
			Vector<KeyValue> params) {

		if (params == null || params.size() == 0) {
			return BaseURL.HTTP + action;
		} else {
			String pString = "";

			for (final KeyValue obj : params) {

				pString += obj.getKey().trim() + "=" + obj.getValue().trim()
						+ "&";
			}

			if (pString.endsWith("&")) {
				pString = pString.subSequence(0, pString.length() - 1)
						.toString();
			}

			return BaseURL.HTTP + action + "?" + UrlUtils.encode(pString);
		}
	}

	public static String getRegisterUrl(String email, String password,
			String firstName, String lastName) {

		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("req=register&email", email));
		temp.addElement(new KeyValue("pin", password));
		temp.addElement(new KeyValue("fname", firstName));
		temp.addElement(new KeyValue("lname", lastName));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String getLoginUrl(String email, String password) {

		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("req=subscriber&logd", email));
		temp.addElement(new KeyValue("pin", password));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String getFavoritesUrl(String user_id) {

		final Vector<KeyValue> temp = new Vector<KeyValue>();

		temp.addElement(new KeyValue("req=favorites&user_id", user_id));
		temp.addElement(new KeyValue("format", "json"));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String addFavoritesUrl(String user_id, String channel_id) {

		final Vector<KeyValue> temp = new Vector<KeyValue>();

		temp.addElement(new KeyValue("req=add_favorite&user_id", user_id));
		temp.addElement(new KeyValue("channel_id", channel_id));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String removeFavoritesUrl(String user_id, String channel_id) {

		final Vector<KeyValue> temp = new Vector<KeyValue>();

		temp.addElement(new KeyValue("req=remove_favorite&user_id", user_id));
		temp.addElement(new KeyValue("channel_id", channel_id));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String getChannelTypeUrl() {

		final Vector<KeyValue> temp = new Vector<KeyValue>();

		temp.addElement(new KeyValue("req=type&format", "json"));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String getGenereUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();

		temp.addElement(new KeyValue("req=genre&format", "json"));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String getCountryUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();

		temp.addElement(new KeyValue("req=country&format", "json"));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String getLanguageUrl() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();

		temp.addElement(new KeyValue("req=languages&format", "json"));
		return getcommonURLWithParamAndAction("", temp);

	}

	// https://www.worldrelay.tv/ws/?user=1&format=json

	public static String getAllChannelUrl(String userId) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("user", userId));
		temp.addElement(new KeyValue("format", "json"));
		return getcommonURLWithParamAndAction("", temp);

	}

	public static String getAllPublicChannelUrl() {
		return "https://worldrelay.appspot.com/json/ch?chanid=PUBLIC";

	}

	// https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=UUbW18JZRgko_mOGm5er8Yzg&key={YOUR_API_KEY}
	public static String getAllYoutubePlayList(String name) {
		return "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId="
				+ name
				+ "&key=AIzaSyAF-2okrQtZS7CmHWpn9jZDxiw51Fru154&maxResults=50";

	}

	// https://www.googleapis.com/youtube/v3/channels?part=contentDetails&forUsername=OneDirectionVEVO&key={YOUR_API_KEY}
	public static String getAllYoutubeChannelName(String userName) {
		return "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&forUsername="
				+ userName + "&key=AIzaSyAF-2okrQtZS7CmHWpn9jZDxiw51Fru154";

	}
	//https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=UCH1dpzjCEiGAt8CXkryhkZg&key=AIzaSyAF-2okrQtZS7CmHWpn9jZDxiw51Fru154
	public static String getAllYoutubeChannelNameFromUserId(String userId) {
		return "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id="
				+ userId + "&key=AIzaSyAF-2okrQtZS7CmHWpn9jZDxiw51Fru154";

	}

	// http://vimeo.com/api/v2/channel/01shortfilm/videos.json
	public static String getVimoeList(String channelName) {
		return "http://vimeo.com/api/v2/channel/" + channelName
				+ "/videos.json";

	}

	// http://player.vimeo.com/video/58600663/config
	public static String getVimoeSongUrl(String videoId) {
		return "http://player.vimeo.com/video/" + videoId + "/config";

	}

	public static String getGenre() {
		return "http://images.worldrelayinc.netdna-cdn.com/genres/genres.json";

	}

	public static String getCountries() {
		return "http://images.worldrelayinc.netdna-cdn.com/countries/countries.json";

	}

	public static String getLanguage() {
		return "http://images.worldrelayinc.netdna-cdn.com/languages/languages.json";

	}

	public static String getCategoryType() {
		return "http://images.worldrelayinc.netdna-cdn.com/types/types.json";

	}

	public static String getDiscover() {
		return "http://images.worldrelayinc.netdna-cdn.com/discover/discover.json";

	}

}