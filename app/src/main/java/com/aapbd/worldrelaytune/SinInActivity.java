package com.aapbd.worldrelaytune;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.PersistentUser;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;

import io.fabric.sdk.android.Fabric;

public class SinInActivity extends Activity implements OnClickListener {
	Context con;

	EditText email_sign_in, password_sign_in;

	TextView btn_sign_in, tv_twitter, tv_sign_in_to, signUpTv;
	String responses = "";

	Typeface tf3;
	private static final int TWITTER_REQ_CODE = 9002;
	String twitterEmail;
	public static final String TWITTER_KEY = "ONTxecdrmnKTzSkRV7u8yUxx6";

	public static final String TWITTER_SECRET = "pqoipwmJcCeZ6OrALtcs1cRVPp1htwcKt0YyIj4jzSAv9ZVmNk";
	TwitterLoginButton loginButton;
	LinearLayout twitterSubmit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY,
				TWITTER_SECRET);
		Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
		setContentView(R.layout.sign_in_wolrld_relay);

		con = this;
		AnalyticsTracker.sendTrackData(this, "Sign In", "");
		AnalyticsTracker.sendEventData(this, "Sign In", "action", "label");
		initUi();
	}

	private void initUi() {
		// TODO Auto-generated method stubn
		twitterSubmit = (LinearLayout) findViewById(R.id.twitterSubmit);
		signUpTv = (TextView) findViewById(R.id.signUpTv);
		email_sign_in = (EditText) findViewById(R.id.email_sign_in);
		password_sign_in = (EditText) findViewById(R.id.password_sign_in);
		tv_twitter = (TextView) findViewById(R.id.tv_twitter);
		btn_sign_in = (TextView) findViewById(R.id.btn_sign_in);
		tv_sign_in_to = (TextView) findViewById(R.id.tv_sign_in_to);
//		email_sign_in.setCursorVisible(true);
//		password_sign_in.setCursorVisible(true);
		// Typeface tf = Typeface.createFromAsset(getAssets(),
		// "fonts/OpenSans-Bold.ttf");
		Typeface tf2 = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		email_sign_in.setTypeface(tf2);
		password_sign_in.setTypeface(tf2);
		password_sign_in.setTypeface(tf2);

		tv_twitter.setTypeface(tf2);
		btn_sign_in.setTypeface(tf2);
		tv_sign_in_to.setTypeface(tf2);

		btn_sign_in.setOnClickListener(this);
//		email_sign_in.setText("shohel@gmail.com");
//		password_sign_in.setText("123456");
		setCursorDrawableColor(email_sign_in,R.color.black);
		setCursorDrawableColor(password_sign_in,R.color.black);
		loginButton = (TwitterLoginButton) findViewById(R.id.login_button);
		loginButton.setCallback(new Callback<TwitterSession>() {
			@Override
			public void success(Result<TwitterSession> result) {
				// Do something with result, which provides a
				// TwitterSession for making API calls

				TwitterSession session = Twitter.getSessionManager()
						.getActiveSession();
				TwitterAuthToken authToken = session.getAuthToken();
				String token = authToken.token;
				String secret = authToken.secret;
				PersistData.setLongData(con, AppConstant.twitterId,
						session.getUserId());

				Log.e("my ID", ">>>  " + session.getUserId());
				Log.e("my ID", ">>>  " + session.getUserName());
				getTwitterData(session);
			}

			@Override
			public void failure(TwitterException arg0) {
				// TODO Auto-generated method stub

			}
		});

		twitterSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				loginButton.performClick();

			}
		});

		email_sign_in.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

				// you can call or do what you want with your EditText here

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				Log.e("onTextChange", ">>" + s);
				boolean isSpace = s.toString().contains(" ");
				if (isSpace) {
					isSpace = false;
					s = s.toString().trim();
					Toast.makeText(con,
							"Space is not allowed at E-Mail Address",
							Toast.LENGTH_SHORT).show();
					// email_sign_in.setText(s.toString().trim());
				}
			}
		});
		password_sign_in.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

				// you can call or do what you want with your EditText here

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {

				Log.e("onTextChange", ">>" + s);
				boolean isSpace = s.toString().contains(" ");
				if (isSpace) {
					isSpace = false;
					s = s.toString().trim();
					Toast.makeText(con, "Space is not allowed at Passcode",
							Toast.LENGTH_SHORT).show();
					// email_sign_in.setText(s.toString().trim());
				}
			}
		});

		//actionBarSetup();
		signUpTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartActivity.toActivity(con, SinUpActivity.class);
				finish();
			}
		});
	TextView backImg =(TextView)findViewById(R.id.backImg);
		backImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			finish();
			}
		});

	}

	public void getTwitterData(final TwitterSession session) {
		MyTwitterApiClient tapiclient = new MyTwitterApiClient(session);
		tapiclient.getCustomService().show(session.getUserId(),
				new Callback<User>() {
					@Override
					public void success(Result<User> result) {

						TwitterAuthToken authToken = session.getAuthToken();
						String token = authToken.token;
						String secret = authToken.secret;

						Log.e("Name", result.data.name);
						// Log.e("Email", result.data.email);
						// Log.e("profileImg", result.data.profileImageUrl);
						twitterEmail = session.getUserId() + "@twitter.com";
						//
						String twitterName = result.data.name;
						String[] names = twitterName.split(" ");
						System.out.println("" + names[0] + " "
								+ names[names.length - 1]);
						PersistData.setStringData(con,
								AppConstant.twitterUserFirstName, names[0]);
						PersistData.setStringData(con,
								AppConstant.twitterUserLastName,
								names[names.length - 1]);
						getTwitterSignIn(AllURL.getLoginUrl(twitterEmail,
								"555555"));
					}

					@Override
					public void failure(TwitterException exception) {
						// TODO Auto-generated method stub
						exception.printStackTrace();
					}

				});
	}

	public static void setCursorDrawableColor(EditText editText, int color) {
		try {
			Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
			fCursorDrawableRes.setAccessible(true);
			int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);
			Field fEditor = TextView.class.getDeclaredField("mEditor");
			fEditor.setAccessible(true);
			Object editor = fEditor.get(editText);
			Class<?> clazz = editor.getClass();
			Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
			fCursorDrawable.setAccessible(true);
			Drawable[] drawables = new Drawable[2];
			drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
			drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
			drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_IN);
			drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_IN);
			fCursorDrawable.set(editor, drawables);
		} catch (final Throwable ignored) {
		}
	}

	public void twitterSubmit(View v) {

		// if (!NetInfo.isOnline(con)) {
		// AlertMessage.showMessage(con, getString(R.string.Status),
		// getString(R.string.checkInternet));
		// return;
		// }
		// if (PersistentUser.isLogged(con) && LoginActivity.isActive(con)) {
		// Toast.makeText(con, "Twitter already logged in", Toast.LENGTH_SHORT)
		// .show();
		// } else {
		// Intent intent = new Intent(con, LoginActivity.class);
		// startActivityForResult(intent, TWITTER_REQ_CODE);
		// AppConstant.isTwitterLogin = true;
		//
		// }

	}

	private void actionBarSetup() {
		// TODO Auto-generated method stub

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.login_top);

		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
				R.id.signtitle);
		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
				R.id.backImg);
		// signtitle.setText(R.string.txt_activity_edit_profile);

		signtitle.setVisibility(View.GONE);
		// Typeface tf = Typeface.createFromAsset(getAssets(),
		// "fonts/OpenSans-Bold.ttf");

		// signtitle.setTypeface(tf);

		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	public void checkInformation() {
		if (TextUtils.isEmpty(email_sign_in.getText().toString())) {
			AlertMessage.showMessage(con, "Sign in Error",
					getString(R.string.PleaseEnterEmail));
		} else if (TextUtils.isEmpty(password_sign_in.getText().toString())) {
			AlertMessage.showMessage(con, "Sign in Error",
					getString(R.string.PleaseEnterPassword));
		} else {
			String email = email_sign_in.getText().toString();
			String pass = password_sign_in.getText().toString();
			Log.e("email", "" + email);
			String url = AllURL.getLoginUrl(email, pass);
			getSignInInfo(AllURL.getLoginUrl(email, pass));

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btn_sign_in) {

			checkInformation();
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if (requestCode == TWITTER_REQ_CODE) {
		// if (resultCode == RESULT_OK) {
		// if (AppConstant.isTwitterLogin) {
		// // profileName.setText("HELLO, " +
		// // PersistentUser.getUserName(con));
		// // logoutLayout.setVisibility(View.VISIBLE);
		// Log.e("Twitter UserName",
		// ":"
		// + PersistData.getStringData(con,
		// AppConstant.twitterUserName));
		// twitterEmail = PersistData.getStringData(con,
		// AppConstant.twitterUserId) + "@twitter.com";
		// getTwitterSignIn(AllURL.getLoginUrl(twitterEmail, "555555"));
		// }
		// }
		// }

		loginButton.onActivityResult(requestCode, resultCode, data);

	}



	private void getSignInInfo(final String url) {
		// TODO Auto-generated method stub
		Log.e("url",">> "+url);

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					JSONObject job;

					try {
						job = new JSONObject(new String(result));
						responses = job.optString("response");
						if (responses.equalsIgnoreCase("1")) {
							PersistData.setStringData(con, AppConstant.urerId,
									job.optString("userId"));
							PersistData.setStringData(con,
									AppConstant.user_name,
									job.optString("user_name"));
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						String resp = responses;

						if (responses.equalsIgnoreCase("1")) {
							PersistentUser.setLogin(con);
							Log.e("UserId",
									""
											+ PersistData.getStringData(con,
											AppConstant.urerId));
							Toast toast=Toast.makeText(con, "Welcome to WorldRelay, " + PersistData.getStringData(con,
											AppConstant.user_name) + "!", Toast.LENGTH_LONG);

							LinearLayout toastLayout = (LinearLayout) toast.getView();
							TextView toastTV = (TextView) toastLayout.getChildAt(0);
							toastTV.setTextSize(18);
							toast.show();
							if (BeforloginActivity.getInstance() != null) {
								BeforloginActivity.getInstance().finish();
							}
							StartActivity.toActivity(con, MainActivity.class);
							finish();
						} else if (responses.equalsIgnoreCase("2")) {
							AlertMessage
									.showMessage(con, "Sign In Error",
											"Incorrect E-Mail and Passcode combination.");
							return;
						} else if (responses.equalsIgnoreCase("3")) {
							AlertMessage.showMessage(con, "Sign In Error",
									"Incorrect Passcode.");

						} else {
							AlertMessage
									.showMessage(con, "Sign In Error",
											"Incorrect E-Mail and Passcode combination.");

						}

					}

				});
			}
		});

		thread.start();

	}

	private void getTwitterSignIn(final String url) {
		// TODO Auto-generated method stub
		Log.e("url",">>"+url);

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					JSONObject job;

					try {
						job = new JSONObject(new String(result));
						responses = job.optString("response");
						if (responses.equalsIgnoreCase("1")) {
							PersistData.setStringData(con, AppConstant.urerId,
									job.optString("userId"));
							PersistData.setStringData(con,
									AppConstant.user_name,
									job.optString("user_name"));
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (responses.equalsIgnoreCase("1")) {
							PersistentUser.setLogin(con);
							Toast toast=Toast.makeText(con, "Welcome to WorldRelay, " + PersistData.getStringData(con,
									AppConstant.user_name) + "!", Toast.LENGTH_LONG);

							LinearLayout toastLayout = (LinearLayout) toast.getView();
							TextView toastTV = (TextView) toastLayout.getChildAt(0);
							toastTV.setTextSize(18);
							toast.show();
							if (BeforloginActivity.getInstance() != null) {
								BeforloginActivity.getInstance().finish();
							}
							StartActivity.toActivity(con, MainActivity.class);
							finish();
						} else if (responses.equalsIgnoreCase("2")) {
							getTwitterRegister(AllURL.getRegisterUrl(
									twitterEmail, "555555",
									PersistData.getStringData(con,
											AppConstant.twitterUserFirstName),
									AppConstant.twitterUserLastName));
							return;
						} else if (responses.equalsIgnoreCase("3")) {
							AlertMessage.showMessage(con, "Sign up Error",
									"Incorrect Passcode.");

						} else {
							AlertMessage
									.showMessage(con, "Sign up Error",
											"Incorrect E-Mail and Passcode combination.");

						}

					}

				});
			}
		});

		thread.start();

	}

	private void getTwitterRegister(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					JSONObject job;

					try {
						job = new JSONObject(new String(result));
						responses = job.optString("response");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (responses.equalsIgnoreCase("OK")) {

							getTwitterSignIn(AllURL.getLoginUrl(twitterEmail,
									"555555"));
						} else {

							AlertMessage.showMessage(con, "Sign up Error",
									responses);
							return;
						}

					}

				});
			}
		});

		thread.start();

	}

	// private void getLoginInfo(final String url) {
	//
	//
	// if (!NetInfo.isOnline(con)) {
	// AlertMessage.showMessage(con, getString(R.string.Status),
	// getString(R.string.checkInternet));
	// return;
	// }
	//
	// final BusyDialog busyNow = new BusyDialog(con, true, false);
	// busyNow.show();
	//
	// // Log.e("login URL", url + "");
	//
	// final AsyncHttpClient client = new AsyncHttpClient();
	// client.get(url, new AsyncHttpResponseHandler() {
	//
	// @Override
	// public void onStart() {
	// // called before request is started
	// }
	//
	// @Override
	// public void onSuccess(int statusCode, Header[] headers,
	// byte[] response) {
	// // called when response HTTP status is "200 OK"
	//
	// if (busyNow != null) {
	// busyNow.dismis();
	// }
	// Log.e("response",">>"+ new String(response));
	//
	// JSONObject job;
	//
	// try {
	// job = new JSONObject(new String(response));
	// responses=job.optString("response");
	// if(responses.equalsIgnoreCase("1")){
	// PersistData.setStringData(con, AppConstant.urerId,
	// job.optString("userId"));
	// PersistData.setStringData(con, AppConstant.user_name,
	// job.optString("user_name"));
	// }
	//
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// if (responses.equalsIgnoreCase("1")) {
	// Toast.makeText(con, "SignIn Succsessful", Toast.LENGTH_LONG)
	// .show();
	// if(BeforloginActivity.getInstance()!=null){
	// BeforloginActivity.getInstance().finish();
	// }
	// StartActivity.toActivity(con, AllPublicChannelActivity.class);
	// finish();
	// } else if(responses.equalsIgnoreCase("2")){
	// AlertMessage.showMessage(con, "Status",
	// "Sorry, but this user e-mail is already registered. Please sign in with your e-mail and passcode or register again using a different e-mail address.");
	// return;
	// }else if(responses.equalsIgnoreCase("3")){
	// AlertMessage.showMessage(con, "Status",
	// "Sorry, but the selected passcode must consist of 6 numbers.");
	//
	// }else{
	// AlertMessage.showMessage(con, "Status", "Server Problem Please Retry");
	//
	// }
	//
	// }
	//
	// @Override
	// public void onFailure(int statusCode, Header[] headers,
	// byte[] errorResponse, Throwable e) {
	// // called when response HTTP status is "4XX" (eg. 401, 403, 404)
	//
	// if (busyNow != null) {
	// busyNow.dismis();
	// }
	//
	// try {
	// Log.e("response",">>"+new String(errorResponse));
	// } catch (Exception e2) {
	// // TODO: handle exception
	// Log.e("e2",">>"+e2.toString());
	// }
	//
	// }
	//
	// @Override
	// public void onRetry(int retryNo) {
	// // called when request is retried
	//
	// }
	// });
	//
	// }

}
