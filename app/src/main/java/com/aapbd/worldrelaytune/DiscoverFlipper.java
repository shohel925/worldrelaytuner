package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.DiscoverInfo;
import com.aapbd.worldrelaytune.model.PublicChannelInfo;
import com.aapbd.worldrelaytune.radio.RadioMainActivity;
import com.aapbd.worldrelaytune.radio.StaticRadioArray;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class DiscoverFlipper {

	ViewFlipper viewFlipper;
	Context con;
	Activity newActivty;
	// HListView newList, featureList, sponsorList;

	TextView newChannel, featureTv, sponsorTv, taptosee, taptosee_one, france;

	Typeface t3;
	// private NewAdapter newAdapter;
	TextView discoverTitile;
	ImageView discoverSearch, discoverSettings, newChannelImg, featuredImg,
			sponsorImg;
	Dialog dd;
	EditText et_search;
	Typeface tf3;
	BusyDialog busy;
	String result = "";
	List<DiscoverInfo> discoverList;
	DatabaseHandler db;
	List<PublicChannelInfo> newTempList, featureTempList, sponsorTempList;
	// FeatureAdapter featureAdapter;
	// SponserAdapter sponserAdapter;
	JSONArray jArray;
	RelativeLayout newChannelLayout, featureLayout, sponsorLayout;

	public DiscoverFlipper(ViewFlipper viewFlipper, Context con,
			Activity newActivty) {

		this.viewFlipper = viewFlipper;
		this.con = con;
		this.newActivty = newActivty;
		viewFlipper.setDisplayedChild(2);
		db = new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(newActivty, "Discover", "");
		AnalyticsTracker.sendEventData(newActivty, "Discover", "action", "label");
//		MyApplication.getInstance().trackEvent("Discover", "", "");
		initUi();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		newChannelLayout = (RelativeLayout) newActivty.findViewById(R.id.newChannelLayout);
		sponsorLayout = (RelativeLayout) newActivty.findViewById(R.id.sponsorLayout);
		featureLayout = (RelativeLayout) newActivty.findViewById(R.id.featureLayout);
		newChannelImg = (ImageView) newActivty.findViewById(R.id.newChannelImg);
		featuredImg = (ImageView) newActivty.findViewById(R.id.featuredImg);
		sponsorImg = (ImageView) newActivty.findViewById(R.id.sponsorImg);
		discoverSearch = (ImageView) newActivty.findViewById(R.id.discoverSearch);
		discoverSettings = (ImageView) newActivty.findViewById(R.id.discoverSettings);
		discoverTitile = (TextView) newActivty.findViewById(R.id.discoverTitile);
		// featureList = (HListView) newActivty.findViewById(R.id.featureList);
		// newList = (HListView) newActivty.findViewById(R.id.newList);
		// sponsorList = (HListView) newActivty.findViewById(R.id.sponsorList);
		newChannel = (TextView) newActivty.findViewById(R.id.newChannel);
		featureTv = (TextView) newActivty.findViewById(R.id.featureTv);
		sponsorTv = (TextView) newActivty.findViewById(R.id.sponsorTv);
		discoverTitile.setText("Discover");

		Typeface tf = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Bold.ttf");
		tf3 = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Light.ttf");
		newChannel.setTypeface(tf);
		featureTv.setTypeface(tf);
		sponsorTv.setTypeface(tf);
		discoverTitile.setTypeface(tf);

		getDiscover(AllURL.getDiscover());

		newChannelLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.discoverType = "New Channels";
				AppConstant.tempPublicList = newTempList;
				new DiscoverItemFlipper(viewFlipper, con, newActivty);

			}
		});
		featureLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.discoverType = "Featured";
				AppConstant.tempPublicList = featureTempList;
				new DiscoverItemFlipper(viewFlipper, con, newActivty);
			}
		});
		sponsorLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.discoverType = "Sponsored";
				AppConstant.tempPublicList = sponsorTempList;
				new DiscoverItemFlipper(viewFlipper, con, newActivty);
			}
		});

		discoverSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchDialoge();
			}
		});
		discoverSettings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartActivity.toActivity(con, SettingsActivity.class);
			}
		});

	}

	public void searchDialoge() {
		dd = new Dialog(con);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.search_dialoge_cancel_search);
		dd.show();

		et_search = (EditText) dd.findViewById(R.id.et_search);
		ViewGroup btn_cancel = (ViewGroup) dd.findViewById(R.id.btn_cancel);
		ViewGroup btn_search = (ViewGroup) dd.findViewById(R.id.btn_search);
		TextView btn_sign = (TextView) dd.findViewById(R.id.btn_sign);
		TextView searchTv = (TextView) dd.findViewById(R.id.searchTv);

		btn_sign.setTypeface(tf3);
		et_search.setTypeface(tf3);
		searchTv.setTypeface(tf3);
		btn_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (AppConstant.searchList != null) {
					AppConstant.searchList.clear();
				}
				String searchTXT = "";
				searchTXT = et_search.getText().toString();
				Log.e("searchTXT", ">>" + searchTXT);
				if (TextUtils.isEmpty(searchTXT)) {
					StartActivity.toActivity(con, SearchResultActivity.class);
					// AlertMessage.showMessage(con,
					// getString(R.string.app_name),
					// "Please enter channel name or number");
					dd.dismiss();
				} else {
					AppConstant.searchList = db.getAllSearchData(searchTXT);
					Log.e("searchVector Size", ">>" + AppConstant.searchList.size());

					if (AppConstant.searchList.size() > 0) {
						StartActivity.toActivity(con, SearchResultActivity.class);
					} else {

						AlertMessage
								.showMessage(con, "Nothing Found",
										"The search did not find any matching channels.");
					}

					dd.dismiss();
				}
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dd.dismiss();
			}
		});

	}

	private void getDiscover(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + result);

					Gson g = new Gson();
					Type fooType = new TypeToken<ArrayList<DiscoverInfo>>() {
					}.getType();
					discoverList = g.fromJson(new String(result), fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				newActivty.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (discoverList.size() > 0) {
							newChannel.setText(discoverList.get(0).getTitle());
							featureTv.setText(discoverList.get(1).getTitle());
							sponsorTv.setText(discoverList.get(2).getTitle());
							Picasso.with(con)
									.load(discoverList.get(0).getImage())
									.error(R.drawable.ic_launcher)
									.into(newChannelImg);
							Picasso.with(con)
									.load(discoverList.get(1).getImage())
									.error(R.drawable.ic_launcher)
									.into(featuredImg);
							Picasso.with(con)
									.load(discoverList.get(2).getImage())
									.error(R.drawable.ic_launcher)
									.into(sponsorImg);
							String newString = createString(discoverList.get(0)
									.getChannels());
							String featureString = createString(discoverList
									.get(1).getChannels());
							String sponserString = createString(discoverList
									.get(2).getChannels());
							newTempList = db.getAllDiscover(newString);
							featureTempList = db.getAllDiscover(featureString);
							sponsorTempList = db.getAllDiscover(sponserString);

							// // ====== for Call new list ======
							// newAdapter = new NewAdapter(con);
							// newList.setAdapter(newAdapter);
							// newAdapter.notifyDataSetChanged();
							//
							// // ====== for Call Feature list ======
							// featureAdapter = new FeatureAdapter(con);
							// featureList.setAdapter(featureAdapter);
							// featureAdapter.notifyDataSetChanged();
							//
							// // ====== for Call Sponsorlist list ======
							//
							// sponserAdapter = new SponserAdapter(con);
							// sponsorList.setAdapter(sponserAdapter);
							// sponserAdapter.notifyDataSetChanged();
						} else {
							AlertMessage.showMessage(con, "Status",
									"No Discover Info Found");
						}

					}

				});
			}
		});

		thread.start();

	}

	public String createString(List<String> dicoverInfo) {

		String names = "";
		for (String number : dicoverInfo) {
			names = names + "'" + number + "',";
		}

		if (names.endsWith(",")) {
			names = names.subSequence(0, names.length() - 1).toString();
		}
		Log.e("names", ">>" + names);
		return names;

	}

	private class NewAdapter extends ArrayAdapter<PublicChannelInfo> {
		Context context;

		NewAdapter(Context context) {
			super(context, R.layout.row_discover, newTempList);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row_discover, null);

			}

			if (position < newTempList.size()) {

				final PublicChannelInfo query = newTempList.get(position);

				final ImageView discoverImg = (ImageView) v
						.findViewById(R.id.discoverImg);
				final TextView discoverName = (TextView) v
						.findViewById(R.id.discoverName);
				final TextView discoverNumber = (TextView) v
						.findViewById(R.id.discoverNumber);

				Typeface tf3 = Typeface.createFromAsset(con.getAssets(),
						"fonts/OpenSans-Light.ttf");
				discoverName.setTypeface(tf3);
				discoverNumber.setTypeface(tf3);
				discoverName.setText(query.getName());
				discoverNumber.setText(query.getWrid());
				Picasso.with(con).load(query.getChannel_image_file())
						.error(R.drawable.ic_launcher).into(discoverImg);
			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final PublicChannelInfo publicChannel = newTempList
							.get(position);
					if (TextUtils.isEmpty(publicChannel.getAudio_location())) {

						if (publicChannel.getStream_type()
								.equalsIgnoreCase("1")) {
							if (publicChannel.getVisual_content_location()
									.startsWith("youtubelive")) {
								AppConstant.youtubeVideoUrl = publicChannel
										.getVisual_content_location()
										.substring(
												publicChannel
														.getVisual_content_location()
														.lastIndexOf("+") + 1);
								AppConstant.channelName = publicChannel
										.getName();
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										YoutubePlayActivity.class);
							} else {
								AppConstant.youtubeName = publicChannel
										.getVisual_content_location();
								AppConstant.channelName = publicChannel
										.getName();
								Log.e("Normal Video Name", ">>"
										+ AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										VideoPlayActivity.class);

							}
						} else if (publicChannel.getStream_type()
								.equalsIgnoreCase("2")) {
							if (TextUtils.isEmpty(publicChannel
									.getStream_vod_url())) {
								AppConstant.youtubeVideoUrl = publicChannel
										.getVisual_content_location()
										.substring(
												publicChannel
														.getVisual_content_location()
														.lastIndexOf("+") + 1);
								Log.e("Youtube video Live Name", ">>"
										+ AppConstant.youtubeVideoUrl);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										YoutubePlayActivity.class);
							} else {
								if ((publicChannel.getStream_vod_url()
										.startsWith("http://vimeo"))
										|| (publicChannel.getStream_vod_url()
												.startsWith("https://vimeo"))) {
									AppConstant.youtubeName = publicChannel
											.getStream_vod_url()
											.substring(
													publicChannel
															.getStream_vod_url()
															.lastIndexOf("/") + 1);
									Log.e("Vimeo Name", ">>"
											+ AppConstant.youtubeName);
									AppConstant.channelInfo = publicChannel;
									AppConstant.channelName = publicChannel
											.getName();
									StartActivity.toActivity(con,
											VimeoPlayListActivity.class);
								} else if (publicChannel.getStream_vod_url()
										.startsWith("youtube")) {

									if (publicChannel.getStream_vod_url()
											.startsWith("youtubeuser")) {
										AppConstant.youtubeName = publicChannel
												.getStream_vod_url()
												.substring(
														publicChannel
																.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube User", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = publicChannel
												.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL
												.getAllYoutubeChannelName(AppConstant.youtubeName));

									} else if (publicChannel
											.getStream_vod_url().startsWith(
													"youtubeplaylist")) {
										AppConstant.youtubeName = publicChannel
												.getStream_vod_url()
												.substring(
														publicChannel
																.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube PlayList", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = publicChannel
												.getName();
										AppConstant.channelInfo = publicChannel;
										StartActivity.toActivity(con,
												YoutubePlayListActivity.class);
									}

								} else if (publicChannel.getStream_vod_url()
										.startsWith("vevo")) {
									// ===== This condition for Vevo ======
									AppConstant.youtubeName = publicChannel
											.getStream_vod_url();
									Log.e("Youtube PlayList", ">>"
											+ AppConstant.youtubeName);
									Toast.makeText(
											con,
											"Vevo channels are currently not supported on Android",
											Toast.LENGTH_SHORT).show();
								}
							}

						} else if (publicChannel.getStream_type()
								.equalsIgnoreCase("3")) {
							// Normal M3u3 video in video view
							AppConstant.youtubeName = publicChannel
									.getVisual_content_location();
							AppConstant.channelName = publicChannel.getName();
							Log.e("Normal Video Name", ">>"
									+ AppConstant.youtubeName);
							AppConstant.channelInfo = publicChannel;
							StartActivity.toActivity(con,
									VideoPlayActivity.class);
						}

					} else {
						StaticRadioArray.urls[0] = publicChannel
								.getAudio_location();
						AppConstant.channelName = publicChannel.getName();
						AppConstant.channelInfo = publicChannel;
						AppConstant.visual_content_location = publicChannel
								.getVisual_content_location();
						getMaxImage(publicChannel.getVisual_content_location()
								+ "maxNumOfImages");

					}

				}
			});

			return v;

		}

	}

	private void getChannelName(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				try {
					JSONObject job = new JSONObject(new String(response));
					jArray = job.getJSONArray("items");
					JSONObject jholder = jArray.getJSONObject(0);
					JSONObject jholder2 = jholder
							.getJSONObject("contentDetails");
					JSONObject jholder3 = jholder2
							.getJSONObject("relatedPlaylists");
					AppConstant.youtubeName = jholder3.optString("uploads");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jArray.length() > 0) {

					StartActivity
							.toActivity(con, YoutubePlayListActivity.class);
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getMaxImage(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));
				String imageCount = new String(response);
				AppConstant.imageSize = Integer.parseInt(imageCount);
				Log.e("Image Size", ">>" + AppConstant.imageSize);
				StartActivity.toActivity(con, RadioMainActivity.class);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private class FeatureAdapter extends ArrayAdapter<PublicChannelInfo> {
		Context context;

		FeatureAdapter(Context context) {
			super(context, R.layout.row_discover, featureTempList);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row_discover, null);

			}

			if (position < featureTempList.size()) {

				final PublicChannelInfo query = featureTempList.get(position);

				final ImageView discoverImg = (ImageView) v
						.findViewById(R.id.discoverImg);
				final TextView discoverName = (TextView) v
						.findViewById(R.id.discoverName);
				final TextView discoverNumber = (TextView) v
						.findViewById(R.id.discoverNumber);

				Typeface tf3 = Typeface.createFromAsset(con.getAssets(),
						"fonts/OpenSans-Light.ttf");
				discoverName.setTypeface(tf3);

				discoverName.setText(query.getName());
				discoverNumber.setText(query.getWrid());
				discoverNumber.setTypeface(tf3);
				Picasso.with(con).load(query.getChannel_image_file())
						.error(R.drawable.ic_launcher).into(discoverImg);
			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final PublicChannelInfo publicChannel = featureTempList
							.get(position);
					if (TextUtils.isEmpty(publicChannel.getAudio_location())) {

						if (publicChannel.getStream_type()
								.equalsIgnoreCase("1")) {
							if (publicChannel.getVisual_content_location()
									.startsWith("youtubelive")) {
								AppConstant.youtubeVideoUrl = publicChannel
										.getVisual_content_location()
										.substring(
												publicChannel
														.getVisual_content_location()
														.lastIndexOf("+") + 1);
								AppConstant.channelName = publicChannel
										.getName();
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										YoutubePlayActivity.class);
							} else {
								AppConstant.youtubeName = publicChannel
										.getVisual_content_location();
								AppConstant.channelName = publicChannel
										.getName();
								Log.e("Normal Video Name", ">>"
										+ AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										VideoPlayActivity.class);

							}
						} else if (publicChannel.getStream_type()
								.equalsIgnoreCase("2")) {
							if (TextUtils.isEmpty(publicChannel
									.getStream_vod_url())) {
								AppConstant.youtubeVideoUrl = publicChannel
										.getVisual_content_location()
										.substring(
												publicChannel
														.getVisual_content_location()
														.lastIndexOf("+") + 1);
								Log.e("Youtube video Live Name", ">>"
										+ AppConstant.youtubeVideoUrl);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										YoutubePlayActivity.class);
							} else {
								if ((publicChannel.getStream_vod_url()
										.startsWith("http://vimeo"))
										|| (publicChannel.getStream_vod_url()
												.startsWith("https://vimeo"))) {
									AppConstant.youtubeName = publicChannel
											.getStream_vod_url()
											.substring(
													publicChannel
															.getStream_vod_url()
															.lastIndexOf("/") + 1);
									Log.e("Vimeo Name", ">>"
											+ AppConstant.youtubeName);
									AppConstant.channelInfo = publicChannel;
									AppConstant.channelName = publicChannel
											.getName();
									StartActivity.toActivity(con,
											VimeoPlayListActivity.class);
								} else if (publicChannel.getStream_vod_url()
										.startsWith("youtube")) {

									if (publicChannel.getStream_vod_url()
											.startsWith("youtubeuser")) {
										AppConstant.youtubeName = publicChannel
												.getStream_vod_url()
												.substring(
														publicChannel
																.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube User", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = publicChannel
												.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL
												.getAllYoutubeChannelName(AppConstant.youtubeName));

									} else if (publicChannel
											.getStream_vod_url().startsWith(
													"youtubeplaylist")) {
										AppConstant.youtubeName = publicChannel
												.getStream_vod_url()
												.substring(
														publicChannel
																.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube PlayList", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = publicChannel
												.getName();
										AppConstant.channelInfo = publicChannel;
										StartActivity.toActivity(con,
												YoutubePlayListActivity.class);
									}

								} else if (publicChannel.getStream_vod_url()
										.startsWith("vevo")) {
									// ===== This condition for Vevo ======
									AppConstant.youtubeName = publicChannel
											.getStream_vod_url();
									Log.e("Youtube PlayList", ">>"
											+ AppConstant.youtubeName);
									Toast.makeText(
											con,
											"Vevo channels are currently not supported on Android",
											Toast.LENGTH_SHORT).show();
								}
							}

						} else if (publicChannel.getStream_type()
								.equalsIgnoreCase("3")) {
							// Normal M3u3 video in video view
							AppConstant.youtubeName = publicChannel
									.getVisual_content_location();
							AppConstant.channelName = publicChannel.getName();
							Log.e("Normal Video Name", ">>"
									+ AppConstant.youtubeName);
							AppConstant.channelInfo = publicChannel;
							StartActivity.toActivity(con,
									VideoPlayActivity.class);
						}

					} else {
						StaticRadioArray.urls[0] = publicChannel
								.getAudio_location();
						AppConstant.channelName = publicChannel.getName();
						AppConstant.channelInfo = publicChannel;
						AppConstant.visual_content_location = publicChannel
								.getVisual_content_location();
						getMaxImage(publicChannel.getVisual_content_location()
								+ "maxNumOfImages");

					}
				}
			});

			return v;

		}

	}

	private class SponserAdapter extends ArrayAdapter<PublicChannelInfo> {
		Context context;

		SponserAdapter(Context context) {
			super(context, R.layout.row_discover, sponsorTempList);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row_discover, null);

			}

			if (position < sponsorTempList.size()) {

				final PublicChannelInfo query = sponsorTempList.get(position);

				final ImageView discoverImg = (ImageView) v
						.findViewById(R.id.discoverImg);
				final TextView discoverName = (TextView) v
						.findViewById(R.id.discoverName);
				final TextView discoverNumber = (TextView) v
						.findViewById(R.id.discoverNumber);
				Typeface tf3 = Typeface.createFromAsset(con.getAssets(),
						"fonts/OpenSans-Light.ttf");
				discoverName.setTypeface(tf3);
				discoverName.setText(query.getName());
				discoverNumber.setTypeface(tf3);
				discoverNumber.setText(query.getWrid());
				Picasso.with(con).load(query.getChannel_image_file())
						.error(R.drawable.ic_launcher).into(discoverImg);
			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final PublicChannelInfo publicChannel = sponsorTempList
							.get(position);
					if (TextUtils.isEmpty(publicChannel.getAudio_location())) {

						if (publicChannel.getStream_type()
								.equalsIgnoreCase("1")) {
							if (publicChannel.getVisual_content_location()
									.startsWith("youtubelive")) {
								AppConstant.youtubeVideoUrl = publicChannel
										.getVisual_content_location()
										.substring(
												publicChannel
														.getVisual_content_location()
														.lastIndexOf("+") + 1);
								AppConstant.channelName = publicChannel
										.getName();
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										YoutubePlayActivity.class);
							} else {
								AppConstant.youtubeName = publicChannel
										.getVisual_content_location();
								AppConstant.channelName = publicChannel
										.getName();
								Log.e("Normal Video Name", ">>"
										+ AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										VideoPlayActivity.class);

							}
						} else if (publicChannel.getStream_type()
								.equalsIgnoreCase("2")) {
							if (TextUtils.isEmpty(publicChannel
									.getStream_vod_url())) {
								AppConstant.youtubeVideoUrl = publicChannel
										.getVisual_content_location()
										.substring(
												publicChannel
														.getVisual_content_location()
														.lastIndexOf("+") + 1);
								Log.e("Youtube video Live Name", ">>"
										+ AppConstant.youtubeVideoUrl);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con,
										YoutubePlayActivity.class);
							} else {
								if ((publicChannel.getStream_vod_url()
										.startsWith("http://vimeo"))
										|| (publicChannel.getStream_vod_url()
												.startsWith("https://vimeo"))) {
									AppConstant.youtubeName = publicChannel
											.getStream_vod_url()
											.substring(
													publicChannel
															.getStream_vod_url()
															.lastIndexOf("/") + 1);
									Log.e("Vimeo Name", ">>"
											+ AppConstant.youtubeName);
									AppConstant.channelInfo = publicChannel;
									AppConstant.channelName = publicChannel
											.getName();
									StartActivity.toActivity(con,
											VimeoPlayListActivity.class);
								} else if (publicChannel.getStream_vod_url()
										.startsWith("youtube")) {

									if (publicChannel.getStream_vod_url()
											.startsWith("youtubeuser")) {
										AppConstant.youtubeName = publicChannel
												.getStream_vod_url()
												.substring(
														publicChannel
																.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube User", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = publicChannel
												.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL
												.getAllYoutubeChannelName(AppConstant.youtubeName));

									} else if (publicChannel
											.getStream_vod_url().startsWith(
													"youtubeplaylist")) {
										AppConstant.youtubeName = publicChannel
												.getStream_vod_url()
												.substring(
														publicChannel
																.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube PlayList", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = publicChannel
												.getName();
										AppConstant.channelInfo = publicChannel;
										StartActivity.toActivity(con,
												YoutubePlayListActivity.class);
									}

								} else if (publicChannel.getStream_vod_url()
										.startsWith("vevo")) {
									// ===== This condition for Vevo ======
									AppConstant.youtubeName = publicChannel
											.getStream_vod_url();
									Log.e("Youtube PlayList", ">>"
											+ AppConstant.youtubeName);
									Toast.makeText(
											con,
											"Vevo channels are currently not supported on Android",
											Toast.LENGTH_SHORT).show();
								}
							}

						} else if (publicChannel.getStream_type()
								.equalsIgnoreCase("3")) {
							// Normal M3u3 video in video view
							AppConstant.youtubeName = publicChannel
									.getVisual_content_location();
							AppConstant.channelName = publicChannel.getName();
							Log.e("Normal Video Name", ">>"
									+ AppConstant.youtubeName);
							AppConstant.channelInfo = publicChannel;
							StartActivity.toActivity(con,
									VideoPlayActivity.class);
						}

					} else {
						StaticRadioArray.urls[0] = publicChannel
								.getAudio_location();
						AppConstant.channelName = publicChannel.getName();
						AppConstant.channelInfo = publicChannel;
						AppConstant.visual_content_location = publicChannel
								.getVisual_content_location();
						getMaxImage(publicChannel.getVisual_content_location()
								+ "maxNumOfImages");

					}
				}
			});

			return v;

		}

	}

}
