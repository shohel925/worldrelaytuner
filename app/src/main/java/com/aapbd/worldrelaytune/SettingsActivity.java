package com.aapbd.worldrelaytune;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.PersistentUser;
import com.aapbd.worldrelaytune.utils.StartActivity;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends Activity {

	Dialog dialog;
	ViewFlipper viewFlipper;
	Context con;
	Activity newActivty;
	TextView myAccountTv, aboutTv, contctUsTv, shareTv, logoutTv;
	LinearLayout lay_logout, lay_share, lay_contact, lay_about, layMayaccount,lay_createChnnel;
	String shareTxt = "";
	TextView  settingTitle;
	LinearLayout settingBack;
	DatabaseHandler db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_lay);

		con = this;
		db=new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(this, "Settings","");
		AnalyticsTracker.sendEventData(this,"Settings", "","");
		initUI();

	}

	private void initUI() {
		// TODO Auto-generated method stub
		///actionBarSetup();
		lay_createChnnel=(LinearLayout)findViewById(R.id.lay_createChnnel);
		settingBack=(LinearLayout)findViewById(R.id.settingBack);
		settingBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		settingTitle=(TextView)findViewById(R.id.settingTitle);
		settingTitle.setText("Settings");
		shareTxt = "The Internet is changing the way we watch TV. So checkout https://www.worldrelay.tv/";
		layMayaccount = (LinearLayout) findViewById(R.id.layMayaccount);
		lay_about = (LinearLayout) findViewById(R.id.lay_about);
		lay_contact = (LinearLayout) findViewById(R.id.lay_contact);
		lay_share = (LinearLayout) findViewById(R.id.lay_share);
		lay_logout = (LinearLayout) findViewById(R.id.lay_logout);

		myAccountTv = (TextView) findViewById(R.id.myAccountTv);
		aboutTv = (TextView) findViewById(R.id.aboutTv);
		contctUsTv = (TextView) findViewById(R.id.contctUsTv);
		shareTv = (TextView) findViewById(R.id.shareTv);
		logoutTv = (TextView) findViewById(R.id.aboutTv);
		Typeface tf3 = Typeface.createFromAsset(con.getAssets(),
				"fonts/OpenSans-Light.ttf");
//		myAccountTv.setTypeface(tf3);
//		aboutTv.setTypeface(tf3);
//		contctUsTv.setTypeface(tf3);
//		shareTv.setTypeface(tf3);
//		logoutTv.setTypeface(tf3);

		lay_createChnnel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog = new Dialog(con);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.create_channel_dialoge);
				TextView continue_create_chanel = (TextView) dialog.findViewById(R.id.continue_create_chanel);
				TextView cancel_create_chanel=(TextView)dialog.findViewById(R.id.cancel_create_chanel);
				continue_create_chanel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						AppConstant.webUrl="https://www.worldrelay.tv/add-personal-channel/";
						Intent i= new Intent(con,WebActivity.class);
						startActivity(i);
						dialog.dismiss();
					}
				});
				cancel_create_chanel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				dialog.show();

			}
		});

		layMayaccount.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				dialog = new Dialog(con);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.about_dialogue);
				TextView ok_about = (TextView) dialog.findViewById(R.id.ok_about);
				TextView version=(TextView)dialog.findViewById(R.id.version);
				try{

					version.setText("Account name: "+ PersistData.getStringData(con, AppConstant.user_name));
				}catch (Exception e){

				}


				ok_about.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				dialog.show();

			}
		});

		lay_about.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog = new Dialog(con);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.about_dialogue);
				TextView ok_about = (TextView) dialog.findViewById(R.id.ok_about);
				TextView version=(TextView)dialog.findViewById(R.id.version);
				try{
					String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
					version.setText("WorldRelay Tuner Version for Android \nVersion "+versionName);
				}catch (Exception e){

				}


				ok_about.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});

		lay_contact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String versionStr="";
				try{
					String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
					versionStr="App version: "+versionName;
				}catch (Exception e){

				}

				final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@worldrelay.net"});
				intent.putExtra(Intent.EXTRA_SUBJECT, "WorldRelay Tuner Android Feedback");
				intent.putExtra(Intent.EXTRA_TEXT   , ""+versionStr);
				final PackageManager pm = getPackageManager();
				final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
				ResolveInfo best = null;
				for (final ResolveInfo info : matches)
					if (info.activityInfo.packageName.endsWith(".gm") ||
							info.activityInfo.name.toLowerCase().contains("gmail")) best = info;
				if (best != null)
					intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
				startActivity(intent);
			}
		});

		lay_share.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent emailIntent = new Intent();
				emailIntent.setAction(Intent.ACTION_SEND);
				// Native email client doesn't currently support HTML, but it
				// doesn't hurt to try in case they fix it
				emailIntent.putExtra(Intent.EXTRA_TEXT, shareTxt);
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "WorldRelay");
				emailIntent.setType("message/rfc822");

				PackageManager pm = getPackageManager();
				Intent sendIntent = new Intent(Intent.ACTION_SEND);
				sendIntent.setType("text/plain");

				Intent openInChooser = Intent.createChooser(emailIntent,
						"Share");

				List<ResolveInfo> resInfo = pm.queryIntentActivities(
						sendIntent, 0);
				List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
				for (int i = 0; i < resInfo.size(); i++) {
					// Extract the label, append it, and repackage it in a
					// LabeledIntent
					ResolveInfo ri = resInfo.get(i);
					String packageName = ri.activityInfo.packageName;
					if (packageName.contains("android.email")) {
						emailIntent.setPackage(packageName);
					} else if (packageName.contains("twitter")
							|| packageName.contains("facebook")
							|| packageName.contains("mms")
							|| packageName.contains("android.gm")) {
						Intent intent = new Intent();
						intent.setComponent(new ComponentName(packageName,
								ri.activityInfo.name));
						intent.setAction(Intent.ACTION_SEND);
						intent.setType("text/plain");
						if (packageName.contains("twitter")) {
							intent.putExtra(Intent.EXTRA_TEXT, shareTxt);
						} else if (packageName.contains("facebook")) {
							// Warning: Facebook IGNORES our text. They say
							// "These fields are intended for users to express themselves. Pre-filling these fields erodes the authenticity of the user voice."
							// One workaround is to use the Facebook SDK to
							// post, but that doesn't allow the user to choose
							// how they want to share. We can also make a custom
							// landing page, and the link
							// will show the <meta content ="..."> text from
							// that page with our link in Facebook.
							intent.putExtra(Intent.EXTRA_TEXT, shareTxt);
						} else if (packageName.contains("mms")) {
							intent.putExtra(Intent.EXTRA_TEXT, shareTxt);
						} else if (packageName.contains("android.gm")) { // If
																			// Gmail
																			// shows
																			// up
																			// twice,
																			// try
																			// removing
																			// this
																			// else-if
																			// clause
																			// and
																			// the
																			// reference
																			// to
																			// "android.gm"
																			// above
							intent.putExtra(Intent.EXTRA_TEXT, shareTxt);
							intent.putExtra(Intent.EXTRA_SUBJECT, "WorldRelay");
							intent.setType("message/rfc822");
						}

						intentList.add(new LabeledIntent(intent, packageName,
								ri.loadLabel(pm), ri.icon));
					}
				}

				// convert intentList to array
				LabeledIntent[] extraIntents = intentList
						.toArray(new LabeledIntent[intentList.size()]);

				openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS,
						extraIntents);
				startActivity(openInChooser);

			}
		});

		lay_logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog = new Dialog(con);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.signout_dialogue);
				TextView ok = (TextView) dialog.findViewById(R.id.ok);
				TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

				ok.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (MainActivity.getInstance() != null) {
							MainActivity.getInstance().finish();
						}
						db.RemoveAllFavorites();
						PersistentUser.logOut(con);
						StartActivity.toActivity(con, SinInActivity.class);
						finish();
					}
				});

				dialog.show();
			}
		});
	}

	private void actionBarSetup() {
		// TODO Auto-generated method stub

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.login_top);

		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
				R.id.signtitle);
		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
				R.id.backImg);
		signtitle.setText("Settings");

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");

		signtitle.setTypeface(tf);

		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

}
