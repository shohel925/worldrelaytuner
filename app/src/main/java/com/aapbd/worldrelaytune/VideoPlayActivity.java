package com.aapbd.worldrelaytune;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.ChannelInfo;
import com.aapbd.worldrelaytune.model.VimeoInfo;
import com.aapbd.worldrelaytune.model.YoutubeSnippet;
import com.aapbd.worldrelaytune.model.YoutubesResponse;
import com.aapbd.worldrelaytune.radio.LastFMCover;
import com.aapbd.worldrelaytune.radio.MyService;
import com.aapbd.worldrelaytune.radio.RadioService;
import com.aapbd.worldrelaytune.radio.StaticRadioArray;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.ShareUtils;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;

public class VideoPlayActivity extends YouTubeBaseActivity implements
		YouTubePlayer.OnInitializedListener {
	Context con;

	LinearLayout btn_favorite, btn_guide, videoLayout,videolayouLIner;
	TextView tv_addfav, tv_guide, descript, description;
	String responses = "",normalUrl="";
	Dialog dd, dd2;
	VideoView videoview;
	ProgressBar progress,progress2;
	LinearLayout bottom,videoMainLyout;
	LinearLayout topLayout;
	TextView backImg, channelName,channelId ,landBack;
	ImageView share_img,audioImage;
	boolean flag = true;
	RelativeLayout addFavoriteLayout,videoReltive,audioLayout,youtubLayout,demandLayout,newLayout;
	FrameLayout normalvideoLayout;
	DatabaseHandler db;
	EditText et_search;
	ViewGroup llview;
	LinearLayout.LayoutParams params, params2,param3,param4;
	BusyDialog busy;
	Dialog dialog;
	// ===== for FB Share =======
	private UiLifecycleHelper uiHelper;
	private boolean isShare = false;
	String shareText;
	TwitterLoginButton twitter_button;
	public static VideoPlayActivity instance;

	public static VideoPlayActivity getInstance() {
		return instance;
	}

	private static final String TWITTER_KEY = "ONTxecdrmnKTzSkRV7u8yUxx6";
	private static final String TWITTER_SECRET = "pqoipwmJcCeZ6OrALtcs1cRVPp1htwcKt0YyIj4jzSAv9ZVmNk";
	ImageView videoFavorites,demandImage,listDemandImg,controlBtn;
	ListView videoList;
	ListAdapter gGridAdapter;
	Typeface tf2;
	//YouTubePlayerView youTubeView;
	YoutubesResponse youtubesResponse;
	YoutubeAdapter mYoutubeAdapter;
	YouTubePlayer player2;
	YouTubePlayerView youTubeView;
	YouTubePlayer.Provider provider;
	//======== for audio play =========================
	private TelephonyManager telephonyManager;
	private boolean wasPlayingBeforePhoneCall = false;// /false
	List<String> imageArray;
	private RadioUpdateReceiver radioUpdateReceiver;
	private AudioManager leftAm;
	public static int stationID = 0;
	public static boolean isStationChanged = false;
	private static final String TYPE_AAC = "aac";
	private static final String TYPE_MP3 = "mp3";
	private final Handler mHandler = new Handler();
	long oldTime;
	private static final int RECOVERY_DIALOG_REQUEST = 1;
	int i = 0;
	boolean isOneImage = true;
	Activity mActivity;
	boolean isLandscape=false,videoFirstHeat=false;
	ImageView youtubePlay,normalVideoPlay;
	// MediaController mc;
	String firstBack="1";
	public  boolean showControl=false;
	Handler handler = new Handler();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
		Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
		setContentView(R.layout.video_play);
		instance = this;
		con = this;
		mActivity=this;
		AnalyticsTracker.sendEventData(mActivity, "Channel_" + AppConstant.channelInfo.getWrid(), "action", "label");
		AnalyticsTracker.sendTrackData(mActivity, "Channel_" + AppConstant.channelInfo.getWrid(), "");
//		MyApplication.getInstance().trackEvent("Channel_"+AppConstant.channelInfo.getWrid(), "", "");
		db = new DatabaseHandler(con);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);

		initUi();
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private void initUi() {
		// TODO Auto-generated method stub
		newLayout=(RelativeLayout)findViewById(R.id.newLayout);
		normalVideoPlay=(ImageView)findViewById(R.id.normalVideoPlay);
		normalvideoLayout=(FrameLayout)findViewById(R.id.normalvideoLayout);
		progress2=(ProgressBar)findViewById(R.id.progress2);
		demandLayout=(RelativeLayout)findViewById(R.id.demandLayout);
		landBack=(TextView)findViewById(R.id.landBack);
		youtubePlay=(ImageView)findViewById(R.id.youtubePlay);
		youtubLayout=(RelativeLayout)findViewById(R.id.youtubLayout);
		controlBtn=(ImageView)findViewById(R.id.controlBtn);
		audioLayout=(RelativeLayout)findViewById(R.id.audioLayout);
		listDemandImg=(ImageView)findViewById(R.id.listDemandImg);
		oldTime=System.currentTimeMillis();
		videoMainLyout=(LinearLayout)findViewById(R.id.videoMainLyout);
		videolayouLIner=(LinearLayout)findViewById(R.id.videolayouLIner);
		channelId=(TextView)findViewById(R.id.channelId);
		videoReltive=(RelativeLayout)findViewById(R.id.videoReltive);
		youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
		youTubeView.initialize(DeveloperKey.DEVELOPER_KEY, this);
		demandImage=(ImageView)findViewById(R.id.demandImage);
		audioImage=(ImageView)findViewById(R.id.audioImage);
		videoList=(ListView)findViewById(R.id.videoList);
		videoFavorites=(ImageView)findViewById(R.id.videoFavorites);
		videoLayout = (LinearLayout) findViewById(R.id.videoLayout);
	//	favoritesImg = (ImageView) findViewById(R.id.favoritesImg);
		channelName = (TextView) findViewById(R.id.channelName);
		addFavoriteLayout = (RelativeLayout) findViewById(R.id.addFavoriteLayout);
		backImg = (TextView) findViewById(R.id.backImg);
		//search_img = (ImageView) findViewById(R.id.search_img);
		share_img = (ImageView) findViewById(R.id.videoShare_img);
		topLayout = (LinearLayout) findViewById(R.id.topLayout);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		progress = (ProgressBar) findViewById(R.id.progress);
		videoview = (VideoView) findViewById(R.id.videoview);
		btn_favorite = (LinearLayout) findViewById(R.id.btn_favorite);
		btn_guide = (LinearLayout) findViewById(R.id.btn_guide);

		tv_addfav = (TextView) findViewById(R.id.tv_addfav);
		tv_guide = (TextView) findViewById(R.id.tv_guide);
		descript = (TextView) findViewById(R.id.descript);
		description = (TextView) findViewById(R.id.description);

		//search_img.setVisibility(View.VISIBLE);
		share_img.setVisibility(View.VISIBLE);
		params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (int) con.getResources().getDimension(R.dimen.audioImg2));
		params.setMargins(15, 15, 15, 15);
		params2 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		params2.setMargins(0, 0, 0, 0);
		param3 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		param3.setMargins(15, 15, 15, 15);
		param4 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		param4.setMargins(0,0,0,0);
		videolayouLIner.setLayoutParams(param3);
		videoLayout.setLayoutParams(params);
		videoLayout.setBackgroundResource(R.drawable.bg_trans);
		normalVideoPlay.setVisibility(View.GONE);
		// Changes the height and width to the specified *pixels*

		Log.e("channelNumber >>", ">>" + AppConstant.channelInfo.getWrid());
		 tf2 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");

		channelName.setTypeface(tf2);
		tv_addfav.setTypeface(tf2);
		tv_guide.setTypeface(tf2);
		descript.setTypeface(tf2);
		description.setTypeface(tf2);
		description.setVisibility(View.VISIBLE);

		Log.e("favorites", "" + db.ifFavorites(AppConstant.channelInfo.getWrid()));
		if (AppConstant.channelInfo != null) {
			channelId.setText(AppConstant.channelInfo.getWrid());
			channelName.setText(AppConstant.channelInfo.getName());
			List<ChannelInfo> tempInfo = db.getCountryFromChannelNumber(AppConstant.channelInfo.getWrid());
			Log.e("country Size", "" + tempInfo.size());

		}
		// shareText="Check out "+AppConstant.channelInfo.getName()+" on WorldRelay Tuner,Channel "+AppConstant.channelInfo.getWrid();
		if (TextUtils.isEmpty(AppConstant.channelInfo.getChannel_info())) {
			description.setText(getString(R.string.dercription));
			description.setVisibility(View.INVISIBLE);
		} else {
			description.setVisibility(View.VISIBLE);
			description.setText(Html.fromHtml(AppConstant.channelInfo.getChannel_info()));
		}
//		shareText = "Check out " + AppConstant.channelInfo.getName()
//				+ " on WorldRelay Tuner, Channel "
//				+ AppConstant.channelInfo.getWrid()
//				+ " https://www.worldrelay.tv";
		shareText ="I am watching channel number "+AppConstant.channelInfo.getWrid()+" "+AppConstant.channelInfo.getName()+" on the WorldRelay Tuner. Check it out! https://www.worldrelay.tv/tuner "+" http://player.vimeo.com/external/129006668.sd.mp4?s=ea813e9f34f567ada996179167073f7c&profile_id=112";

		if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {
			videoFavorites.setImageResource(R.drawable.star_fill);
		} else {
			videoFavorites.setImageResource(R.drawable.star_fillas);
		}

//		 mc = new MediaController(this) {
//			@Override
//			public void hide() {
//				super.hide();
//
//			}
//
//			@Override
//			public boolean dispatchKeyEvent(KeyEvent event) {
//				if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
//					((Activity) getContext()).finish();
//				}
//
//				return super.dispatchKeyEvent(event);
//			}
//
//			/*
//             * (non-Javadoc)
//             *
//             * @see android.widget.MediaController#show()
//             */
//			@Override
//			public void show() {
//				// TODO Auto-generated method stub
//				super.show();
//
//				// showHideView(true);
//
//			}
//
//		};
//		mc.setAnchorView(videoview);
//		videoview.setMediaController(mc);

		// =========== final change ================
		if(showControl){
			normalVideoPlay.setVisibility(View.VISIBLE);

		}else{
			normalVideoPlay.setVisibility(View.GONE);
		}

		if(flag){
			if(AppConstant.videoSource.equalsIgnoreCase("youtubelive")){
				youTubeView.setVisibility(View.VISIBLE);
				youtubLayout.setVisibility(View.VISIBLE);
				demandImage.setVisibility(View.GONE);
				demandLayout.setVisibility(View.GONE);
				progress.setVisibility(View.GONE);
				audioImage.setVisibility(View.GONE);
				controlBtn.setVisibility(View.GONE);
				audioLayout.setVisibility(View.GONE);
				videoview.setVisibility(View.GONE);
				videoList.setVisibility(View.GONE);
				normalvideoLayout.setVisibility(View.GONE);
				newLayout.setVisibility(View.GONE);
				showControl=true;
			}else if(AppConstant.videoSource.equalsIgnoreCase("normal")){
				youTubeView.setVisibility(View.GONE);
				youtubLayout.setVisibility(View.GONE);
				demandImage.setVisibility(View.GONE);
				demandLayout.setVisibility(View.GONE);
				progress.setVisibility(View.VISIBLE);
				audioImage.setVisibility(View.GONE);
				audioLayout.setVisibility(View.GONE);
				controlBtn.setVisibility(View.GONE);
				videoview.setVisibility(View.VISIBLE);
				videoList.setVisibility(View.GONE);
				normalvideoLayout.setVisibility(View.VISIBLE);
				newLayout.setVisibility(View.VISIBLE);
				normalUrl=AppConstant.channelInfo.getVisual_content_location();
				Log.e("flag", ">>" + flag);
				showControl=false;
				if (flag) {
					Log.e("Call", "Call");
					startVideo();
				}

			}else if(AppConstant.videoSource.equalsIgnoreCase("vimeo")){
				AppConstant.ondemendSOurce="";
				youTubeView.setVisibility(View.GONE);
				youtubLayout.setVisibility(View.GONE);
				demandImage.setVisibility(View.GONE);
				demandLayout.setVisibility(View.GONE);
				progress.setVisibility(View.GONE);
				controlBtn.setVisibility(View.GONE);
				audioImage.setVisibility(View.GONE);
				audioLayout.setVisibility(View.GONE);
				videoview.setVisibility(View.GONE);
				videoList.setVisibility(View.VISIBLE);
				normalvideoLayout.setVisibility(View.GONE);
				newLayout.setVisibility(View.GONE);
				getVimeoList(AllURL.getVimoeList(AppConstant.vimeoChannelId));

				showControl=true;
			} else if(AppConstant.videoSource.equalsIgnoreCase("youtubeUser")){
				AppConstant.ondemendSOurce="";
				youTubeView.setVisibility(View.GONE);
				demandImage.setVisibility(View.GONE);
				demandLayout.setVisibility(View.GONE);
				progress.setVisibility(View.GONE);
				audioImage.setVisibility(View.GONE);
				controlBtn.setVisibility(View.GONE);
				audioLayout.setVisibility(View.GONE);
				videoview.setVisibility(View.GONE);
				videoList.setVisibility(View.VISIBLE);
				youtubLayout.setVisibility(View.GONE);
				normalvideoLayout.setVisibility(View.GONE);
				newLayout.setVisibility(View.GONE);
				getChannelInfo(AllURL.getAllYoutubePlayList(AppConstant.youtubeName));
				showControl=true;

			}else if(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand")){
				progress2.setVisibility(View.GONE);
				showControl=true;
				if(AppConstant.ondemendSOurce.equalsIgnoreCase("youtube")){

					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.GONE);
					demandLayout.setVisibility(View.GONE);
					progress.setVisibility(View.VISIBLE);
					audioImage.setVisibility(View.GONE);
					audioLayout.setVisibility(View.GONE);
					controlBtn.setVisibility(View.GONE);
					videoview.setVisibility(View.VISIBLE);
					videoList.setVisibility(View.GONE);
					youtubLayout.setVisibility(View.GONE);
					normalvideoLayout.setVisibility(View.VISIBLE);
					newLayout.setVisibility(View.VISIBLE);
					normalUrl=AppConstant.channelInfo.getVisual_content_location();
					startVideo();
					getChannelInfo(AllURL.getAllYoutubePlayList(AppConstant.youtubeName));
				}else if (AppConstant.ondemendSOurce.equalsIgnoreCase("vimeo")){
					showControl=true;
					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.VISIBLE);
					demandLayout.setVisibility(View.VISIBLE);
					progress.setVisibility(View.VISIBLE);
					audioImage.setVisibility(View.GONE);
					controlBtn.setVisibility(View.GONE);
					audioLayout.setVisibility(View.GONE);
					videoview.setVisibility(View.VISIBLE);
					videoList.setVisibility(View.GONE);
					youtubLayout.setVisibility(View.GONE);
					normalvideoLayout.setVisibility(View.VISIBLE);
					newLayout.setVisibility(View.VISIBLE);
					normalUrl=AppConstant.channelInfo.getVisual_content_location();
					startVideo();
					getVimeoList(AllURL.getVimoeList(AppConstant.vimeoChannelId));
				}


			}else if(AppConstant.videoSource.equalsIgnoreCase("audio")){
				if(AppConstant.channelInfo.getStream_type().equalsIgnoreCase("3") ){
					showControl=true;
					if(AppConstant.ondemendSOurce.equalsIgnoreCase("youtube")){
						getChannelInfo(AllURL.getAllYoutubePlayList(AppConstant.youtubeName));
					}else if (AppConstant.ondemendSOurce.equalsIgnoreCase("vimeo")){
						getVimeoList(AllURL.getVimoeList(AppConstant.vimeoChannelId));
					}

					if(imageArray!=null){
						imageArray.clear();
					}
					imageArray = new ArrayList<String>();
					for (int i = 1; i <= AppConstant.imageSize; i++) {
						imageArray.add(AppConstant.visual_content_location + "Music" + i
								+ ".jpg");
					}
					for(int i=0; i<AppConstant.imageSize; i++){
						Log.e("image Url",">>"+imageArray.get(i));
					}


					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.GONE);
					listDemandImg.setVisibility(View.GONE);
					progress.setVisibility(View.VISIBLE);
					audioImage.setVisibility(View.VISIBLE);
					audioLayout.setVisibility(View.VISIBLE);
					controlBtn.setVisibility(View.GONE);
					videoview.setVisibility(View.GONE);
					videoList.setVisibility(View.GONE);
					normalvideoLayout.setVisibility(View.GONE);
					newLayout.setVisibility(View.GONE);
					//====== For Audio Play =======================


					StaticRadioArray.isFromString = false;
					try {

						if (MyService.getMyService() == null) {
							MyService.startService(con, VideoPlayActivity.this);
						}

					} catch (final Exception e) {

						Toast.makeText(getApplicationContext(),
								"Exception" + e.getMessage(), Toast.LENGTH_SHORT).show();

					}
					telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
					if (telephonyManager != null) {
						telephonyManager.listen(phoneStateListener,
								PhoneStateListener.LISTEN_CALL_STATE);
					}


					initialize();



				}else {
					showControl=true;
					if(imageArray!=null){
						imageArray.clear();
					}
					imageArray = new ArrayList<String>();
					for (int i = 1; i <= AppConstant.imageSize; i++) {
						imageArray.add(AppConstant.visual_content_location + "Music" + i
								+ ".jpg");
					}
					for(int i=0; i<AppConstant.imageSize; i++){
						Log.e("image Url",">>"+imageArray.get(i));
					}


					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.GONE);
					progress.setVisibility(View.VISIBLE);
					audioImage.setVisibility(View.VISIBLE);
					audioLayout.setVisibility(View.VISIBLE);
					controlBtn.setVisibility(View.GONE);
					videoview.setVisibility(View.GONE);
					videoList.setVisibility(View.GONE);
					normalvideoLayout.setVisibility(View.GONE);
					newLayout.setVisibility(View.GONE);
					//====== For Audio Play =======================


					StaticRadioArray.isFromString = false;
					try {

						if (MyService.getMyService() == null) {
							MyService.startService(con, VideoPlayActivity.this);
						}

					} catch (final Exception e) {

						Toast.makeText(getApplicationContext(),
								"Exception" + e.getMessage(), Toast.LENGTH_SHORT).show();

					}
					telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
					if (telephonyManager != null) {
						telephonyManager.listen(phoneStateListener,
								PhoneStateListener.LISTEN_CALL_STATE);
					}


					initialize();
				}

			}
		}

		if(isLandscape){
			landBack.setVisibility(View.VISIBLE);
		}else{
			landBack.setVisibility(View.GONE);
		}

		audioImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!isLandscape){
					if ((AppConstant.videoSource.equalsIgnoreCase("audio"))) {
						if(AppConstant.channelInfo.getStream_type().equalsIgnoreCase("3")){
							demandImage.setVisibility(View.VISIBLE);
							demandLayout.setVisibility(View.VISIBLE);
							progress2.setVisibility(View.GONE);
						}
					}
				}
			}
		});
		landBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


				if(isLandscape){
					if((AppConstant.videoSource.equalsIgnoreCase("youtubeUser"))||(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand"))||(AppConstant.videoSource.equalsIgnoreCase("vimeo"))){
						if(firstBack.equalsIgnoreCase("1")){
							videoview.setVisibility(View.GONE);
							videoList.setVisibility(View.VISIBLE);
							youTubeView.setVisibility(View.GONE);
							firstBack="";
							if (videoview.isPlaying()) {
								videoview.stopPlayback();
							}
							if(player2.isPlaying()){
								player2.pause();
							}
						}else{
							finish();
						}


					}else if(AppConstant.videoSource.equalsIgnoreCase("audio")){
						if(AppConstant.channelInfo.getStream_type().equalsIgnoreCase("3")){
							if(firstBack.equalsIgnoreCase("1")){
								videoview.setVisibility(View.GONE);
								videoList.setVisibility(View.VISIBLE);
								youTubeView.setVisibility(View.GONE);
								firstBack="";
								if (videoview.isPlaying()) {
									videoview.stopPlayback();
								}

								if(player2.isPlaying()){
									player2.pause();
								}
							}else{
								if (videoview.isPlaying()) {
									videoview.stopPlayback();
								}
								if (MyService.getMyService()!=null) {
									MyService.getMyService().stop();
								}

								if(player2.isPlaying()){
									player2.pause();
								}


								long currentTime=System.currentTimeMillis();
								long time=(currentTime-oldTime)/1000;
								String duration=convertToTime(time);
								Log.e("duration", "" + duration);
								//AnalyticsTracker.sendEventData(VideoPlayActivity.this, "Video Play", "Play", ""+AppConstant.channelInfo.getWrid() + " " + duration);

								finish();
							}
						}else{
							if (videoview.isPlaying()) {
								videoview.stopPlayback();
							}

							if(player2.isPlaying()){
								player2.pause();
							}
							finish();
						}
					}else{
						if (videoview.isPlaying()) {
							videoview.stopPlayback();
						}

						if(player2.isPlaying()){
							player2.pause();
						}

						long currentTime=System.currentTimeMillis();
						long time=(currentTime-oldTime)/1000;
						String duration=convertToTime(time);
						Log.e("duration", "" + duration);
						//AnalyticsTracker.sendEventData(VideoPlayActivity.this, "Video Play", "Play", ""+AppConstant.channelInfo.getWrid() + " " + duration);
							finish();
						}

				}else{
					if (videoview.isPlaying()) {
						videoview.stopPlayback();
					}

					if(player2.isPlaying()){
						player2.pause();
					}
					long currentTime=System.currentTimeMillis();
					long time=(currentTime-oldTime)/1000;
					String duration=convertToTime(time);
					Log.e("duration", "" + duration);
					//AnalyticsTracker.sendEventData(VideoPlayActivity.this, "Video Play", "Play", ""+AppConstant.channelInfo.getWrid() + " " + duration);
					finish();
				}
			}
		});
		youtubePlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (player2.isPlaying()){
					youtubePlay.setImageResource(R.drawable.play_btn_play);
					player2.pause();
				}else{
					youtubePlay.setImageResource(R.drawable.play_btn);
					player2.play();
				}
			}
		});

		normalVideoPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (videoview.isPlaying()) {
					videoview.pause();
					normalVideoPlay.setImageResource(R.drawable.play_btn_play);
				} else {
					videoview.start();
					normalVideoPlay.setImageResource(R.drawable.play_btn);
				}
			}
		});

		videoview.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				//gesture detector to detect swipe.
				//gestureDetector.onTouchEvent(arg1);
				//mc.show();
				if(!isLandscape){
					if ((AppConstant.videoSource.equalsIgnoreCase("vimeo")) ||
							(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand"))) {
						demandImage.setVisibility(View.VISIBLE);
						demandLayout.setVisibility(View.VISIBLE);
						progress2.setVisibility(View.GONE);
					}
				}


				return true;//always return true to consume event
			}
		});

		youTubeView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.e("click", "click");
				if (!isLandscape) {
					demandImage.setVisibility(View.VISIBLE);
					demandLayout.setVisibility(View.VISIBLE);
					progress2.setVisibility(View.GONE);
				}

			}
		});


		mHandler.removeCallbacks(mUpdateTimeTask);
		mHandler.postDelayed(mUpdateTimeTask, 100);

		videoFavorites.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {

					removeFavoritesInfo(AllURL.removeFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				} else {
					addFavoritesInfo(AllURL.addFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				}
			}
		});
		demandImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				progress2.setVisibility(View.GONE);
				if (AppConstant.videoSource.equalsIgnoreCase("normalAndDemand")){
					showControl=true;
					listDemandImg.setVisibility(View.VISIBLE);
					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.GONE);
					progress.setVisibility(View.GONE);
					audioImage.setVisibility(View.GONE);
					//audioLayout.setVisibility(View.GONE);
					videoview.setVisibility(View.GONE);
					videoList.setVisibility(View.VISIBLE);
				}else if (AppConstant.videoSource.equalsIgnoreCase("vimeo")){
					showControl=true;
					listDemandImg.setVisibility(View.GONE);
					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.GONE);
					progress.setVisibility(View.GONE);
					audioImage.setVisibility(View.GONE);
					//audioLayout.setVisibility(View.GONE);
					videoview.setVisibility(View.GONE);
					videoList.setVisibility(View.VISIBLE);
				}
				else if (AppConstant.videoSource.equalsIgnoreCase("youtubeUser")){
					showControl=true;
					listDemandImg.setVisibility(View.GONE);
					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.GONE);
					progress.setVisibility(View.GONE);
					audioImage.setVisibility(View.GONE);
					//audioLayout.setVisibility(View.GONE);
					videoview.setVisibility(View.GONE);
					videoList.setVisibility(View.VISIBLE);
				}else if(AppConstant.videoSource.equalsIgnoreCase("audio")){
					if(AppConstant.channelInfo.getStream_type().equalsIgnoreCase("3")){
						showControl=true;
						listDemandImg.setVisibility(View.VISIBLE);
						youTubeView.setVisibility(View.GONE);
						demandImage.setVisibility(View.GONE);
						progress.setVisibility(View.GONE);
						audioImage.setVisibility(View.GONE);
						//audioLayout.setVisibility(View.GONE);
						videoview.setVisibility(View.GONE);
						videoList.setVisibility(View.VISIBLE);
						if(MyService.getMyService()!=null){
							MyService.getMyService().stop();
						}
					}
				}
			}
		});

		listDemandImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(AppConstant.videoSource.equalsIgnoreCase("audio")){
					if(AppConstant.channelInfo.getStream_type().equalsIgnoreCase("3")){
						listDemandImg.setVisibility(View.GONE);
						youTubeView.setVisibility(View.GONE);
						demandImage.setVisibility(View.GONE);
						progress.setVisibility(View.VISIBLE);
						audioImage.setVisibility(View.VISIBLE);
						audioLayout.setVisibility(View.VISIBLE);
						controlBtn.setVisibility(View.GONE);
						videoview.setVisibility(View.GONE);
						videoList.setVisibility(View.GONE);
						normalvideoLayout.setVisibility(View.GONE);
						newLayout.setVisibility(View.GONE);
						if(player2.isPlaying()){
							player2.pause();
						}
						//====== For Audio Play =======================


						StaticRadioArray.isFromString = false;
						try {

							if (MyService.getMyService() == null) {
								MyService.startService(con, VideoPlayActivity.this);
							}

						} catch (final Exception e) {

							Toast.makeText(getApplicationContext(),
									"Exception" + e.getMessage(), Toast.LENGTH_SHORT).show();

						}
						telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
						if (telephonyManager != null) {
							telephonyManager.listen(phoneStateListener,
									PhoneStateListener.LISTEN_CALL_STATE);
						}


						initialize();
					}

				}else{
					listDemandImg.setVisibility(View.GONE);
					youTubeView.setVisibility(View.GONE);
					demandImage.setVisibility(View.GONE);
					progress.setVisibility(View.VISIBLE);
					audioImage.setVisibility(View.GONE);
					//audioLayout.setVisibility(View.GONE);
					videoview.setVisibility(View.VISIBLE);
					videoList.setVisibility(View.GONE);
					normalUrl=AppConstant.channelInfo.getVisual_content_location();
					player2.pause();
					startVideo();
				}



			}
		});

		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isLandscape){
					if((AppConstant.videoSource.equalsIgnoreCase("youtubeUser"))||(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand"))||(AppConstant.videoSource.equalsIgnoreCase("vimeo"))){
						videoList.setVisibility(View.VISIBLE);
						youTubeView.setVisibility(View.GONE);
						videoview.setVisibility(View.GONE);
					}

				}else{

					long currentTime=System.currentTimeMillis();
					long time=(currentTime-oldTime)/1000;
					String duration=convertToTime(time);
					Log.e("duration", "" + duration);
					//AnalyticsTracker.sendEventData(VideoPlayActivity.this, "Video Play", "Play", ""+AppConstant.channelInfo.getWrid() + " " + duration);
					if (videoview.isPlaying()) {
						videoview.stopPlayback();
					}
					if(AppConstant.fromAudio){
						AppConstant.fromAudio=false;
						StopRadio();
					}

					AppConstant.fromVimeoVideo = false;

					handler.postDelayed(new Runnable() {

						@Override
						public void run() {
							VideoPlayActivity.this.finish();

						}
					}, 1000);


				}


			}
		});
		twitter_button = (TwitterLoginButton) findViewById(R.id.twitter_button);
		twitter_button.setCallback(new Callback<TwitterSession>() {
			@Override
			public void success(Result<TwitterSession> result) {
				// Do something with result, which provides a
				// TwitterSession for making API calls
				AppConstant.videoTwitterButtonClick = true;
				TwitterSession session = Twitter.getSessionManager().getActiveSession();
				TwitterAuthToken authToken = session.getAuthToken();
				String token = authToken.token;
				String secret = authToken.secret;
				PersistData.setLongData(con, AppConstant.twitterId, session.getUserId());

				Log.e("my ID", ">>>  " + session.getUserId());
				Log.e("my ID", ">>>  " + session.getUserName());

			}

			@Override
			public void failure(TwitterException arg0) {
				// TODO Auto-generated method stub

			}
		});
//		search_img.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				searchDialoge();
//			}
//		});


		share_img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog = new Dialog(con);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialogue_share);
				LinearLayout faceBook = (LinearLayout) dialog.findViewById(R.id.faceBook);
				LinearLayout mail = (LinearLayout) dialog.findViewById(R.id.mail);
				LinearLayout twitter = (LinearLayout) dialog.findViewById(R.id.twitter);
				LinearLayout message = (LinearLayout) dialog.findViewById(R.id.message);
				TextView cancelShare = (TextView) dialog.findViewById(R.id.cancelShare);
				cancelShare.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();

					}
				});

				faceBook.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fbDialoge("Facebook", shareText);
						dialog.dismiss();
					}
				});

				mail.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						ShareUtils.byMail(con, "", "", shareText);
					}
				});

				twitter.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						fbDialoge("Twitter", shareText);
						dialog.dismiss();
					}
				});

				message.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						ShareUtils.shareBySMS(con, shareText);
					}
				});

				dialog.show();

			}

		});

		btn_favorite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {
					removeFavoritesInfo(AllURL.removeFavoritesUrl(PersistData.getStringData(con, AppConstant.urerId), AppConstant.channelInfo.getWrid()));
				} else {
					addFavoritesInfo(AllURL.addFavoritesUrl(PersistData.getStringData(con, AppConstant.urerId), AppConstant.channelInfo.getWrid()));
				}

			}
		});

		btn_guide.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// removeFavoritesInfo(AllURL.removeFavoritesUrl(PersistData.getStringData
				// (con, AppConstant.urerId), "44877"));
			}
		});

		Log.e("Url", "" + AppConstant.youtubeName);
		videoview.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				Log.e("Onprepare ", "Call");

				progress.setVisibility(View.INVISIBLE);
				// if (busy != null) {
				// busy.dismis();
				// }
				//mc.show();

				// if (seekPos != 0) {
				//
				// videoview.seekTo(seekPos);
				// seekPos = 0;
				// }

			}
		});

//		videoview.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//			@Override
//			public boolean onInfo(MediaPlayer mp, int what, int extra) {
//				Log.e("Mp",":"+mp.getTrackInfo());
//				Log.e("what",":"+what);
//				Log.e("extra",":"+extra);
//				return true;
//			}
//		});

		videoview.setOnErrorListener(new MediaPlayer.OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				Log.e("Error","Something error");
				if(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand")){
					progress.setVisibility(View.INVISIBLE);
					AlertMessage.showPlayErrorDemand(con, "Channel Unavailable", "Sorry, but this channel is not\navailable at this time", videoview,videoList);
				}else{
					progress.setVisibility(View.INVISIBLE);
					AlertMessage.showPlayError(VideoPlayActivity.this, con, "Channel Unavailable", "Sorry, but this channel is not \navailable at this time");
				}


				return true;
			}
		});
		controlBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(MyService.getMyService().isPlaying()){
					MyService.getMyService().pause();
					controlBtn.setImageResource(R.drawable.play_btn_play);
				}else{
					MyService.getMyService().play();
					controlBtn.setImageResource(R.drawable.play_btn);
				}
			}
		});



	}

	public String convertToTime(long time) {

		String startTime = "00:00";
		int intTime = (int)time;
		int h = intTime / 60 + Integer.valueOf(startTime.substring(0, 1));
		int m = intTime % 60 + Integer.valueOf(startTime.substring(3, 4));
		String newtime = h + ":" + m;
		return newtime;
	}

	public void initialize() {
		try {

			leftAm = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			leftAm.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			leftAm.getStreamVolume(AudioManager.STREAM_MUSIC);

			final Handler playNow = new Handler();
			playNow.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Log.e("StaticRadioArray.urls", "are" + StaticRadioArray.urls[0]);

					MyService.getMyService().play();
					Log.e("Service", "" + MyService.getMyService());
					// updateStatus();
					updateMetadata();
					updateAlbum();
					updatePlayTimer();
					// MyService.getMyService().showNotification();

				}
			}, 2000);

			// displayAd();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void updatePlayTimer() {
		// timeTextView.setText(MyService.getMyService().getPlayingTime());

		final Handler handler = new Handler();
		final Timer timer = new Timer();
		final TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						// timeTextView.setText(MyService.getMyService()
						// .getPlayingTime());
					}
				});
			}
		};
		timer.schedule(doAsynchronousTask, 0, 1000);
	}

	public void updateAlbum() {
		MyService.getMyService().getAlbum();
		final String artist = MyService.getMyService().getArtist();
		final String track = MyService.getMyService().getTrack();
		final Bitmap albumCover = MyService.getMyService().getAlbumCover();

		Log.e("Cover Image", "" + albumCover);
		// artistName.setText(artist);
		// songName.setText(track);
		// albumTextView.setText(album);

		if (albumCover == null || artist.equals("") && track.equals("")) {
			updateDefaultCoverImage();

		} else {
			// coverImg.setImageBitmap(albumCover);
			// stationImageView.setImageBitmap(albumCover);
			MyService.getMyService().setAlbum(LastFMCover.album);

			if (MyService.getMyService().getAlbum().length()
					+ MyService.getMyService().getArtist().length() > 50) {
				// albumTextView.setText("");
			}
		}
	}
	String status;
	public void updateMetadata() {
		String artistfull = "";
		String artist = MyService.getMyService().getArtist();
		status = MyService.getMyService().getStatus();
	//	buffer.setText("" + status);
		artistfull = artist;
		String track = MyService.getMyService().getTrack();
		if (artist.length() > 30) {
			artist = artist.substring(0, 30) + "...";
		}

		artistfull = artistfull.replaceAll(" ", "%20");
		track = track.replaceAll(" ", "%20");

		if (track != "" || artistfull != "") {

		}

	}


	// /==========hello=
	PhoneStateListener phoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			if (state == TelephonyManager.CALL_STATE_RINGING) {
				wasPlayingBeforePhoneCall = MyService.getMyService().isPlaying();
				MyService.getMyService().stop();
			} else if (state == TelephonyManager.CALL_STATE_IDLE) {
				if (wasPlayingBeforePhoneCall) {
					MyService.getMyService().play();
				}
			} else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
				// A call is dialing,
				// active or on hold
				wasPlayingBeforePhoneCall = MyService.getMyService().isPlaying();
				MyService.getMyService().stop();
			}
			super.onCallStateChanged(state, incomingNumber);
		}
	};

	private void startVideo() {
		// TODO Auto-generated method stub

		progress.setVisibility(View.VISIBLE);
		// busy = new BusyDialog(con, true, false);
		// busy.show();
		try {
			Log.e("normalUrl", ">>" + normalUrl);
			normalvideoLayout.removeAllViews();
			//videoview = new VideoView(this);
			videoview.setFocusable(false);
			videoview.setFocusableInTouchMode(false);
			if (videoview.isPlaying()) {
				videoview.stopPlayback();
			}
			//videoview.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			normalvideoLayout.addView(videoview);
			videoview.setVideoURI(Uri.parse(normalUrl));
			videoview.start();

		}catch (Exception e){
			e.printStackTrace();

			Log.e("Error message Video",">>"+e.getMessage());
		}


	}
	public void StopRadio() {
		MyService.getMyService().stop();
		MyService.stopServiceConnection();

		resetMetadata();
		updateDefaultCoverImage();
	}

	public void resetMetadata() {
		MyService.getMyService().resetMetadata();
		// artistTextView.setText("");
		// albumTextView.setText("");
		// trackTextView.setText("");
	}
	public void updateDefaultCoverImage() {
		try {
			final String mDrawableName = "station_" + (MyService.getMyService().getCurrentStationID() + 1);
			getResources().getIdentifier(mDrawableName, "drawable", getPackageName());

			getResources().getIdentifier("station_default", "drawable", getPackageName());

			// if (resID > 0)
			// stationImageView.setImageResource(resID);
			// else
			// stationImageView.setImageResource(resID_default);
			//
			// albumTextView.setText("");
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();

		if (telephonyManager != null) {
			telephonyManager.listen(phoneStateListener,
					PhoneStateListener.LISTEN_NONE);
		}

		Runtime.getRuntime().gc();
	}


	@Override
	protected void onResume() {
		super.onResume();
		/* Register for receiving broadcast messages */
		if (radioUpdateReceiver == null) {
			radioUpdateReceiver = new RadioUpdateReceiver();
		}
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_CREATED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_DESTROYED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_STARTED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_CONNECTING));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_START_PREPARING));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_PREPARED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_PLAYING));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_PAUSED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_STOPPED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_COMPLETED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_ERROR));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_BUFFERING_START));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_BUFFERING_END));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_METADATA_UPDATED));
		registerReceiver(radioUpdateReceiver, new IntentFilter(RadioService.MODE_ALBUM_UPDATED));

		if (wasPlayingBeforePhoneCall) {
			MyService.getMyService().play();
			wasPlayingBeforePhoneCall = false;
		}

		if (MyService.getMyService() != null) {
			if (isStationChanged) {
				if (stationID != MyService.getMyService().getCurrentStationID()) {
					MyService.getMyService().stop();
					MyService.getMyService().setCurrentStationID(stationID);
					resetMetadata();
					updateDefaultCoverImage();

				}
				if (!MyService.getMyService().isPlaying()) {
					MyService.getMyService().play();

				}
				isStationChanged = false;
			}
		}

	}

	/* Receive Broadcast Messages from RadioService */
	private class RadioUpdateReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(RadioService.MODE_CREATED)) {

			} else if (intent.getAction().equals(RadioService.MODE_DESTROYED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				updateDefaultCoverImage();
				updateMetadata();
				progress.setVisibility(View.GONE);
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_STARTED)) {

				// pauseButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				Log.e("MODE_STARTED", "MODE_STARTED..............");

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				//
				// playButton.setEnabled(true);

				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_CONNECTING)) {
				// pauseButton.setEnabled(false);
				// playButton.setVisibility(VRiew.VISIBLE);
				// pauseButton.setVisibility(View.GONE);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				progress.setVisibility(View.GONE);
				Log.e("MODE_CONNECTING", "MODE_CONNECTING..............");

				// playButton.setEnabled(false);

				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_START_PREPARING)) {
				// pauseButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);

				// playButton.setEnabled(false);
				// stopButton.setEnabled(true);
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_PREPARED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				// // pauseButton.setImageResource(R.drawable.load);
				// pauseButton.setImageResource(R.drawable.play_btn);

				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_BUFFERING_START)) {

				Log.e("MODE_BUFFERING_START",
						"MODE_BUFFERING_START..............");

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);

				// pauseButton.startAnimation(AnimationUtils.loadAnimation(
				// newActivty, R.anim.rotate));

				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_BUFFERING_END)) {
				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				// pauseButton.setImageResource(R.drawable.play_btn);
				// pauseButton.clearAnimation();
				progress.setVisibility(View.GONE);
				Log.e("MODE_BUFFERING_END", "MODE_BUFFERING_END..............");

				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_PLAYING)) {
				if (MyService.getMyService().getCurrentStationType()
						.equals(TYPE_AAC)) {
					// playButton.setEnabled(false);
					// stopButton.setEnabled(true);
				} else {
					// playButton.setEnabled(false);
					// pauseButton.setEnabled(true);
					// stopButton.setEnabled(true);
					// playButton.setVisibility(View.GONE);
					// pauseButton.setVisibility(View.VISIBLE);
					// pauseButton.setImageResource(R.drawable.play_btn);
				}
				// pauseButton.clearAnimation();
				// pauseButton.setImageResource(R.drawable.play_btn);
				Log.e("MODE_PLAYING", "MODE_PLAYING..............");
				// updateStatus();
				progress.setVisibility(View.GONE);
			} else if (intent.getAction().equals(RadioService.MODE_PAUSED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(true);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_STOPPED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_COMPLETED)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);
				// playButton.setVisibility(View.VISIBLE);
				// pauseButton.setVisibility(View.GONE);
				// MyService.getMyService().setCurrentStationURL2nextSlot();
				// MyService.getMyService().play();
				// updateStatus();
			} else if (intent.getAction().equals(RadioService.MODE_ERROR)) {
				// playButton.setEnabled(true);
				// pauseButton.setEnabled(false);
				// stopButton.setEnabled(false);

				// playButton.setVisibility(View.GONE);
				// pauseButton.setVisibility(View.VISIBLE);
				// pauseButton.clearAnimation();
				// updateStatus();
			} else if (intent.getAction().equals(
					RadioService.MODE_METADATA_UPDATED)) {
				updateMetadata();
				// updateStatus();
				updateDefaultCoverImage();
			} else if (intent.getAction().equals(
					RadioService.MODE_ALBUM_UPDATED)) {
				updateAlbum();
			}
		}
	}

	@Override
	public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
		if (errorReason.isUserRecoverableError()) {
			errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
		} else {
			String errorMessage = String.format(
					getString(R.string.error_player), errorReason.toString());
			Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
			player2=player;
			// loadVideo() will auto play video
			// Use cueVideo() method, if you don't want to play it automatically
			Log.e("On success","call");
			if(flag){
				if(AppConstant.videoSource.equalsIgnoreCase("youtubelive")){
					player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
					player.loadVideo("" + AppConstant.youtubeVideoUrl);
				}
			}


			// Hiding player controls
			//player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
		}
	}

	private YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerView) findViewById(R.id.youtube_view);
	}

	private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

		@Override
		public void onBuffering(boolean arg0) {
			Log.e("Play2","Buffering");
		}

		@Override
		public void onPaused() {
			Log.e("Play2", "pasue");
			//player2.play();
		}

		@Override
		public void onPlaying() {
			Log.e("Play2","playing");
			progress2.setVisibility(View.GONE);
		}

		@Override
		public void onSeekTo(int arg0) {
		}

		@Override
		public void onStopped() {
			Log.e("Play2", "Stop");
			if(player2.getCurrentTimeMillis()==player2.getDurationMillis()){

			}else {
				player2.play();
			}

		}

	};

	private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

		@Override
		public void onAdStarted() {
			Log.e("started",">>");
			progress2.setVisibility(View.GONE);
			progress.setVisibility(View.GONE);
		}

		@Override
		public void onError(YouTubePlayer.ErrorReason arg0) {
			Log.e("error",">>"+arg0);
		}

		@Override
		public void onLoaded(String arg0) {
			Log.e("onLoaded",">>"+arg0);
		}

		@Override
		public void onLoading() {
			Log.e("onLoading",">>");
		}

		@Override
		public void onVideoEnded() {
			Log.e("videoEnded",">>");
		}

		@Override
		public void onVideoStarted() {
			Log.e("VideoStarted",">>");
		}
	};
	 BusyDialog busyNow;
	private void getChannelInfo(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}
			if(!(AppConstant.ondemendSOurce.equalsIgnoreCase("youtube")||AppConstant.ondemendSOurce.equalsIgnoreCase("vimeo"))){
//				 busyNow = new BusyDialog(con, true, false);
//				busyNow.show();
				progress.setVisibility(View.VISIBLE);
			}


		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
								  byte[] response) {
				// called when response HTTP status is "200 OK"

//				if (busyNow != null) {
//					busyNow.dismis();
//				}
				progress.setVisibility(View.GONE);
				Log.e("response", new String(response));
				Gson g = new Gson();
				youtubesResponse = g.fromJson(new String(response), YoutubesResponse.class);

				if (youtubesResponse.getItems().size() > 0) {

					for (YoutubeSnippet yInfo : youtubesResponse.getItems()) {
						Log.e("image", ">>" + yInfo.getSnippet().getThumbnails().getHigh().getUrl());
						Log.e("Video id", ">>" + yInfo.getSnippet().getResourceId().getVideoId());
					}

					mYoutubeAdapter = new YoutubeAdapter(con);
					videoList.setAdapter(mYoutubeAdapter);
					mYoutubeAdapter.notifyDataSetChanged();
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
								  byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)
				progress.setVisibility(View.GONE);
				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private class YoutubeAdapter extends ArrayAdapter<YoutubeSnippet> {
		Context context;

		YoutubeAdapter(Context context) {
			super(context, R.layout.new_rows, youtubesResponse.getItems());

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.new_rows, null);

			}

			if (position < youtubesResponse.getItems().size()) {

				final YoutubeSnippet query = youtubesResponse.getItems().get(position);

				final ImageView cat_item_Img = (ImageView) v.findViewById(R.id.cat_item_Img);
				final TextView chanelTitle = (TextView) v.findViewById(R.id.chanelTitle);
				final TextView channelName = (TextView) v.findViewById(R.id.channelName);
				chanelTitle.setText(query.getSnippet().getTitle());
				channelName.setText(AppConstant.channelInfo.getName());

				if (!TextUtils.isEmpty(query.getSnippet().getThumbnails().getHigh().getUrl())) {
					Picasso.with(con).load(query.getSnippet().getThumbnails().getHigh()
							.getUrl()).error(R.drawable.ic_launcher).into(cat_item_Img);
				} else {
					cat_item_Img.setImageResource(R.drawable.discover_14);
				}

				chanelTitle.setTypeface(tf2);
				channelName.setTypeface(tf2);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final YoutubeSnippet query = youtubesResponse.getItems().get(position);

					Log.e("Video id", "" + query.getSnippet().getResourceId().getVideoId());
					AppConstant.youtubeVideoUrl = query.getSnippet().getResourceId().getVideoId();
					AppConstant.channelName = query.getSnippet().getTitle();
					videoview.setVisibility(View.GONE);
					videoList.setVisibility(View.GONE);
					progress2.setVisibility(View.GONE);
					youtubLayout.setVisibility(View.VISIBLE);
					youTubeView.setVisibility(View.VISIBLE);
					demandImage.setVisibility(View.VISIBLE);
					demandLayout.setVisibility(View.VISIBLE);
					audioImage.setVisibility(View.GONE);
					progress.setVisibility(View.VISIBLE);
					listDemandImg.setVisibility(View.GONE);
					if(MyService.getMyService()!=null){
						MyService.getMyService().stop();
					}
					Log.e("Raw click", "call");
					try {
						firstBack="1";
						//player2.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
						player2.setPlaybackEventListener(playbackEventListener);
						player2.setPlayerStateChangeListener(playerStateChangeListener);
						player2.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
						videoview.pause();
						player2.loadVideo("" + AppConstant.youtubeVideoUrl);
					}catch (Exception e){
						e.printStackTrace();
					}
				}
			});

			return v;

		}

	}
	List<VimeoInfo> vimeoChannelList;

	private void getVimeoList(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		if(!(AppConstant.ondemendSOurce.equalsIgnoreCase("youtube")||AppConstant.ondemendSOurce.equalsIgnoreCase("vimeo"))){
//			busyNow = new BusyDialog(con, true, false);
//			busyNow.show();
			progress.setVisibility(View.VISIBLE);
		}


		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					Gson g = new Gson();

					Type fooType = new TypeToken<ArrayList<VimeoInfo>>() {
					}.getType();
					vimeoChannelList = g.fromJson(new String(result), fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

//						if (busyNow != null) {
//							busyNow.dismis();
//						}
						progress.setVisibility(View.GONE);

						if (vimeoChannelList.size() > 0) {

							gGridAdapter = new ListAdapter(con);
							videoList.setAdapter(gGridAdapter);
							gGridAdapter.notifyDataSetChanged();
						}

					}

				});
			}
		});

		thread.start();

	}


	private class ListAdapter extends ArrayAdapter<VimeoInfo> {
		Context context;

		ListAdapter(Context context) {
			super(context, R.layout.new_rows, vimeoChannelList);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
							ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.new_rows, null);

			}

			if (position < vimeoChannelList.size()) {

				final VimeoInfo query = vimeoChannelList.get(position);

				final ImageView cat_item_Img = (ImageView) v.findViewById(R.id.cat_item_Img);
				final TextView chanelTitle = (TextView) v.findViewById(R.id.chanelTitle);
				final TextView channelName = (TextView) v.findViewById(R.id.channelName);

				chanelTitle.setText(query.getTitle());
				channelName.setText(convertToTime(query.getDuration()));
				Log.e("Duration",":: "+query.getDuration());
				Picasso.with(con).load(query.getThumbnail_medium()).error(R.drawable.ic_launcher).into(cat_item_Img);
				chanelTitle.setTypeface(tf2);
				channelName.setTypeface(tf2);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					firstBack="1";
					VimeoInfo query = vimeoChannelList.get(position);
					Log.e("Song id", ">>" + query.getId());
					normalUrl="";
					AppConstant.videoTitle = query.getTitle();
					AppConstant.videoDescription = query.getDescription();
					getVimeoUrl(AllURL.getVimoeSongUrl(query.getId()));

				}
			});

			return v;

		}

	}

	public String convertToTime(String time) {

		String startTime = "00:00";
		int intTime = Integer.parseInt(time);
		int h = intTime / 60 + Integer.valueOf(startTime.substring(0, 1));
		int m = intTime % 60 + Integer.valueOf(startTime.substring(3, 4));
		String newtime = h + ":" + m;
		return newtime;
	}

	private void getVimeoUrl(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

//		final BusyDialog busy = new BusyDialog(con, true, false);
//		busy.show();
		progress.setVisibility(View.VISIBLE);

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);
					JSONObject job = new JSONObject(result);
					JSONObject job1 = job.getJSONObject("request");
					JSONObject job2 = job1.getJSONObject("files");
					JSONArray jsonArray=job2.getJSONArray("progressive");
					for (int i=0; i<jsonArray.length(); i++){
						JSONObject jholder=jsonArray.getJSONObject(i);
						if(jholder.optString("quality").equalsIgnoreCase("270p")){
							normalUrl = jholder.optString("url");
						}

					}
				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

//						if (busy != null) {
//							busy.dismis();
//						}

						if(TextUtils.isEmpty(normalUrl)){
							if(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand")){
								progress.setVisibility(View.INVISIBLE);
								AlertMessage.showPlayErrorDemand(con, "Channel Unavailable", "Sorry, but this channel is not\navailable at this time", videoview,videoList);
							}else{
								progress.setVisibility(View.INVISIBLE);
								AlertMessage.showPlayError(VideoPlayActivity.this, con, "Channel Unavailable", "Sorry, but this channel is not \navailable at this time");
							}
							return;
						}
						progress.setVisibility(View.VISIBLE);
						videoview.setVisibility(View.VISIBLE);
						videoList.setVisibility(View.GONE);
						listDemandImg.setVisibility(View.GONE);
						demandImage.setVisibility(View.VISIBLE);
						normalvideoLayout.setVisibility(View.VISIBLE);
						newLayout.setVisibility(View.VISIBLE);
						startVideo();

					}

				});
			}
		});

		thread.start();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// volControl = (SeekBar) findViewById(R.id.VolumeSeekBar);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			AppConstant.fromVimeoVideo = false;
			if (videoview.isPlaying()) {
				videoview.stopPlayback();
			}

			if(AppConstant.fromAudio){
				AppConstant.fromAudio = false;
				StopRadio();

			}
			if(player2.isPlaying()){
				player2.pause();
			}
			if(isLandscape){
				if((AppConstant.videoSource.equalsIgnoreCase("youtubeUser"))||(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand"))||(AppConstant.videoSource.equalsIgnoreCase("vimeo"))){
					if(firstBack.equalsIgnoreCase("1")){
						videoview.setVisibility(View.GONE);
						videoList.setVisibility(View.VISIBLE);
						youTubeView.setVisibility(View.GONE);
						firstBack="";
						if (videoview.isPlaying()) {
							videoview.stopPlayback();
						}
						if(player2.isPlaying()){
							player2.pause();
						}
					}else{
						finish();
					}


				}else if(AppConstant.videoSource.equalsIgnoreCase("audio")){
					if(AppConstant.channelInfo.getStream_type().equalsIgnoreCase("3")){
						if(firstBack.equalsIgnoreCase("1")){
							videoview.setVisibility(View.GONE);
							videoList.setVisibility(View.VISIBLE);
							youTubeView.setVisibility(View.GONE);
							firstBack="";
							if (videoview.isPlaying()) {
								videoview.stopPlayback();
							}
							if (MyService.getMyService()!=null) {
								MyService.getMyService().stop();
							}

							if(player2.isPlaying()){
								player2.pause();
							}
						}else{
							if (videoview.isPlaying()) {
								videoview.stopPlayback();
							}
							if (MyService.getMyService()!=null) {
								MyService.getMyService().stop();
							}

							if(player2.isPlaying()){
								player2.pause();
							}
							finish();
						}
					}else{
						if (videoview.isPlaying()) {
							videoview.stopPlayback();
						}
						if (MyService.getMyService()!=null) {
							MyService.getMyService().stop();
						}

						if(player2.isPlaying()){
							player2.pause();
						}
						finish();
					}
				}else{
					if (videoview.isPlaying()) {
						videoview.stopPlayback();
					}
					if (MyService.getMyService()!=null) {
						MyService.getMyService().stop();
					}

					if(player2.isPlaying()){
						player2.pause();
					}
					finish();
				}

			}else{
				if (videoview.isPlaying()) {
					videoview.stopPlayback();
				}
				if (MyService.getMyService()!=null) {
					MyService.getMyService().stop();
				}

				if(player2.isPlaying()){
					player2.pause();
				}
				finish();
			}


			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (videoview.isPlaying()) {
			videoview.stopPlayback();
		}
		if (radioUpdateReceiver != null) {
			unregisterReceiver(radioUpdateReceiver);
			// finish();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RECOVERY_DIALOG_REQUEST) {
			// Retry initialization if user performed a recovery action
			getYouTubePlayerProvider().initialize(DeveloperKey.DEVELOPER_KEY, this);
		}
		uiHelper.onActivityResult(requestCode, resultCode, data);
		twitter_button.onActivityResult(requestCode, resultCode, data);
		if (AppConstant.videoTwitterButtonClick) {
			//startVideo();
			final BusyDialog busyDialog =new BusyDialog(con,false,false);
			AppConstant.videoTwitterButtonClick = false;
			TwitterApiClient twitterApiClient = TwitterCore.getInstance()
					.getApiClient(
							Twitter.getSessionManager().getActiveSession());
			StatusesService statusesService = twitterApiClient
					.getStatusesService();
			statusesService.update(shareText,
					PersistData.getLongData(con, AppConstant.twitterId), null,
					null, null, null, null, null, new Callback<Tweet>() {
						@Override
						public void success(Result<Tweet> result) {
							// Do something with result, which provides a Tweet
							// inside of result.data
							if (busyDialog != null) {
								busyDialog.dismis();
							}
							if (dd2 != null) {
								dd2.dismiss();
							}

							Log.e("result", "Success");
							Toast.makeText(con, "Twitter Post Successful!", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void failure(TwitterException exception) {
							// Do something on failure
							if (busyDialog != null) {
								busyDialog.dismis();
							}
							Log.e("result", "Failed");
						}
					});

		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub

		super.onConfigurationChanged(newConfig);
		Log.e("ONCONFIGCHANGE", "CALLED");
		flag = false;
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			controlBtn.setVisibility(View.GONE);
			youtubePlay.setVisibility(View.GONE);
			youtubLayout.setVisibility(View.VISIBLE);
			normalVideoPlay.setVisibility(View.GONE);
			landBack.setVisibility(View.GONE);
			isLandscape=false;
			bottom.setVisibility(View.VISIBLE);
			topLayout.setVisibility(View.VISIBLE);
			videoLayout.setLayoutParams(params);
			videoLayout.setBackgroundResource(R.drawable.bg_trans);
			videolayouLIner.setLayoutParams(param3);
			videoMainLyout.setBackgroundResource(R.color.white);
			if((AppConstant.videoSource.equalsIgnoreCase("vimeo"))||
					(AppConstant.videoSource.equalsIgnoreCase("youtubeUser"))||
					(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand"))){
				demandImage.setVisibility(View.VISIBLE);
			}
			landBack.setVisibility(View.GONE);
		} else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			if((AppConstant.videoSource.equalsIgnoreCase("vimeo"))||
					(AppConstant.videoSource.equalsIgnoreCase("youtubeUser"))||
					(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand"))){
				landBack.setVisibility(View.VISIBLE);
			}
			if((AppConstant.videoSource.equalsIgnoreCase("audio"))){
				if(AppConstant.channelInfo.getStream_type().equalsIgnoreCase("3")){
					landBack.setVisibility(View.VISIBLE);

				}
			}
			if(AppConstant.videoSource.equalsIgnoreCase("audio")){
			}else if ((AppConstant.videoSource.equalsIgnoreCase("youtubeUser"))||(AppConstant.ondemendSOurce.equalsIgnoreCase("youtube"))){
				youtubePlay.setVisibility(View.VISIBLE);
				normalVideoPlay.setVisibility(View.GONE);
				controlBtn.setVisibility(View.GONE);
			}
			if(showControl){
				youtubePlay.setVisibility(View.GONE);
				normalVideoPlay.setVisibility(View.VISIBLE);
				controlBtn.setVisibility(View.GONE);
				Log.e("Normal Play","Call");
			}

			isLandscape=true;
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
			audioLayout.setVisibility(View.VISIBLE);
			bottom.setVisibility(View.GONE);
			topLayout.setVisibility(View.GONE);
			videoLayout.setLayoutParams(params2);
			videolayouLIner.setLayoutParams(param4);
			videoMainLyout.setBackgroundResource(R.color.black);
			videoLayout.setBackgroundResource(0);
			if((AppConstant.videoSource.equalsIgnoreCase("vimeo"))||
					(AppConstant.videoSource.equalsIgnoreCase("youtubeUser"))||
					(AppConstant.videoSource.equalsIgnoreCase("normalAndDemand"))){
					demandImage.setVisibility(View.GONE);
			}
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Log.e("handler ","Call");
					///player2.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
					controlBtn.setVisibility(View.GONE);
					youtubePlay.setVisibility(View.GONE);
					normalVideoPlay.setVisibility(View.GONE);
					//mc.hide();

				}
			}, 4000);

		}
	}

	public void searchDialoge() {
		dd = new Dialog(con);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.search_dialoge_cancel_search);
		dd.show();

		et_search = (EditText) dd.findViewById(R.id.et_search);
		ViewGroup btn_cancel = (ViewGroup) dd.findViewById(R.id.btn_cancel);
		ViewGroup btn_search = (ViewGroup) dd.findViewById(R.id.btn_search);

		btn_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String searchTXT = "";
				searchTXT = et_search.getText().toString();
				if (TextUtils.isEmpty(searchTXT)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),
							"Please enter channel name or number");
				} else {
					AppConstant.searchList = db.getAllSearchData(searchTXT);

					if (AppConstant.searchList.size() > 0) {
						StartActivity.toActivity(con,
								SearchResultActivity.class);
						if (SearchResultActivity.getInstance() != null) {
							SearchResultActivity.getInstance().finish();
						}
						finish();
					} else {

						AlertMessage
								.showMessage(con, "Nothing Found",
										"The search did not find any matching channels.");
					}

					dd.dismiss();
				}
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dd.dismiss();
			}
		});

	}

	public void fbDialoge(final String shareTitle, final String searchTxt) {
		dd2 = new Dialog(con);
		dd2.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd2.setContentView(R.layout.dialogue_fb);
		dd2.show();

		TextView dialogeFbCancel = (TextView) dd2.findViewById(R.id.dialogeFbCancel);
		TextView fbPost = (TextView) dd2.findViewById(R.id.fbPost);
		TextView title = (TextView) dd2.findViewById(R.id.title);
		final EditText fbPostTxt = (EditText) dd2.findViewById(R.id.fbPostTxt);
		title.setText(shareTitle);
		fbPostTxt.setText(searchTxt);
		fbPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isShare = true;
				shareText = fbPostTxt.getText().toString();
				if (shareTitle.equalsIgnoreCase("Facebook")) {
					Session session = Session.getActiveSession();
					if (!session.isOpened() && !session.isClosed()) {
						session.openForRead(new Session.OpenRequest(
								VideoPlayActivity.this).setPermissions(
								Arrays.asList("email")).setCallback(callback));
					} else {
						Session.openActiveSession(VideoPlayActivity.this, true,
								callback);
					}
				} else {
					if (Twitter.getSessionManager().getActiveSession() != null) {
						final BusyDialog busyDialog= new BusyDialog(con,false,false);
						TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient(Twitter.getSessionManager().getActiveSession());
						StatusesService statusesService = twitterApiClient.getStatusesService();
						statusesService.update(shareText, PersistData.getLongData(con, AppConstant.twitterId),
								null, null, null, null, null, null, new Callback<Tweet>() {
									@Override
									public void success(Result<Tweet> result) {
										// Do something with result, which
										// provides a Tweet inside of
										// result.l
										if (busyDialog != null) {
											busyDialog.dismis();
										}
										if (dd2 != null) {
											dd2.dismiss();
										}

										Log.e("result", "Success");
										Toast.makeText(con, "Twitter Post Successful!", Toast.LENGTH_SHORT).show();

									}

									@Override
									public void failure(
											TwitterException exception) {
										// Do something on failure
										if (busyDialog != null) {
											busyDialog.dismis();
										}
										Log.e("result", "Failed");
										Toast.makeText(con, "Twitter Post Failed", Toast.LENGTH_SHORT).show();
									}
								});
					} else {
						twitter_button.performClick();
					}
				}


			}
		});

		dialogeFbCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dd2.dismiss();
			}
		});

	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private void onSessionStateChange(final Session session,
			SessionState state, Exception exception) {
		if (state.isOpened()) {

			publishStoryByFb();

		} else if (state.isClosed()) {
			Log.e("Status", "Logged out...");
		}
	}

	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	private void publishStoryByFb() {
		Session session = Session.getActiveSession();

		if (session != null) {

			// Check for publish permissions
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(AppConstant.PERMISSIONS, permissions)) {
				Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
						this, AppConstant.PERMISSIONS);
				session.requestNewPublishPermissions(newPermissionsRequest);
				isShare = true;
				return;
			}

			isShare = false;

			Bundle postParams = new Bundle();
			// postParams.putString("message", "Checkout this business: " +
			// businessUrl);
			postParams.putString("message", shareText);

			busy = new BusyDialog(con, false, "Sharing listing...");
			busy.show();

			Request.Callback callback = new Request.Callback() {
				@Override
				public void onCompleted(Response response) {
					if (busy != null) {
						busy.dismis();
					}
					GraphObject graphObj = response.getGraphObject();
					if (graphObj == null) {
						// Toast.makeText(con, "Already posted",
						// Toast.LENGTH_SHORT).show();
						FacebookRequestError error = response.getError();
						if (error != null) {
							Toast.makeText(con.getApplicationContext(), error.getErrorMessage(), Toast.LENGTH_SHORT).show();
						}
					} else {
						JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
						String postId = null;
						try {
							postId = graphResponse.getString("id");
							if (postId != null && postId.length() > 0) {
								Toast.makeText(con, "Successfully posted on facebook", Toast.LENGTH_SHORT).show();
								try {
									dd2.dismiss();
								} catch (Exception ex) {
								}
								;
							}
						} catch (JSONException e) {
							Log.i("Status", "JSON error " + e.getMessage());
						}
						FacebookRequestError error = response.getError();
						if (error != null) {
							Toast.makeText(con.getApplicationContext(),
									error.getErrorMessage(), Toast.LENGTH_SHORT)
									.show();
						} else {
							// Toast.makeText(con.getApplicationContext(),
							// postId, Toast.LENGTH_LONG).show();
						}
					}
				}
			};

			Request request = new Request(session, "me/feed", postParams,
					HttpMethod.POST, callback);

			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
		}

	}

	String addFavoritesResponse="";
	private void addFavoritesInfo(final String url) {
		// TODO Auto-generated method stub
		Log.e("url",">>"+url);
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					addFavoritesResponse = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + addFavoritesResponse);


				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (addFavoritesResponse.trim().equalsIgnoreCase("1")) {
							db.updateFavorites(AppConstant.channelInfo.getWrid());
							videoFavorites.setImageResource(R.drawable.star_fill);
							Toast.makeText(con, "Channel added successfully", Toast.LENGTH_LONG).show();

						} else if (addFavoritesResponse.trim().equalsIgnoreCase("2")) {

							Toast.makeText(con, "Channel  already a Favorite. Did not add it again", Toast.LENGTH_LONG).show();
						} else if (addFavoritesResponse.trim().equalsIgnoreCase("3")) {

							Toast.makeText(con, "10-Channel limit reached. Could not add more. ", Toast.LENGTH_LONG).show();
						}

					}

				});
			}
		});

		thread.start();

	}

	String removeFavoritesResponse="";
	private void removeFavoritesInfo(final String url) {
		// TODO Auto-generated method stub
		Log.e("url",">>"+url);
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					removeFavoritesResponse = HTTPHandler.GetText(HTTPHandler.getInputStreamGET(url));

					Log.e("response", ">>" + removeFavoritesResponse);


				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (removeFavoritesResponse.trim().equalsIgnoreCase("1")) {
							db.removeFavorites(AppConstant.channelInfo.getWrid());
							videoFavorites.setImageResource(R.drawable.star_fillas);
							Toast.makeText(con, "Channel removed  successfully",
									Toast.LENGTH_LONG).show();

						} else if (removeFavoritesResponse.trim().equalsIgnoreCase("0")) {

							Toast.makeText(con, "Channel not removed (not on the list of Favorites).", Toast.LENGTH_LONG).show();
						}

					}

				});
			}
		});

		thread.start();

	}

	private final Runnable mUpdateTimeTask = new Runnable() {
		@Override
		public void run() {
		if(AppConstant.fromAudio){
			setImageInPicaso();
		}

			demandImage.setVisibility(View.GONE);
			mHandler.postDelayed(this, 10000);
		}
	};

	public void setImageInPicaso() {
		// TODO Auto-generated method stub

		try{

			if (AppConstant.imageSize == 1) {
				if (isOneImage) {
					Picasso.with(con).load(imageArray.get(0)).into(audioImage);
					isOneImage = false;
				}

			} else {

				Picasso.with(con).load(imageArray.get(i)).into(audioImage);
				i++;
				if (i == (imageArray.size())) {
					i = 0;
				}

			}

		}catch(Exception e){
			e.printStackTrace();
		}



	}
}
