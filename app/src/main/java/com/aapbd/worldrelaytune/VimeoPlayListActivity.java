package com.aapbd.worldrelaytune;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aapbd.worldrelaytune.model.VimeoInfo;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class VimeoPlayListActivity extends Activity {
	Context con;
	Activity act;
	ListView vimeoListView;
	ListAdapter gGridAdapter;

	Dialog dd;
	TextView mainTitle;
	Typeface tf3;
	String responses;
	List<VimeoInfo> vimeoChannelList;
	LinearLayout vimeoBack;
	TextView vimeoTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vimeo_playlist);
		con = this;
		act = this;
	//	AnalyticsTracker.sendTrackData(this, R.string.app_name);
		initUI();

	}

	private void initUI() {
		vimeoTitle=(TextView)findViewById(R.id.vimeoTitle);
		vimeoBack=(LinearLayout)findViewById(R.id.vimeoBack);
		vimeoListView = (ListView) findViewById(R.id.vimeoListView);
		vimeoTitle.setText(AppConstant.channelName);
		//actionBarSetup();

		getVimeoList(AllURL.getVimoeList(AppConstant.vimeoChannelId));

		tf3 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
		vimeoBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void actionBarSetup() {
		// TODO Auto-generated method stub

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.login_top);

		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
				R.id.signtitle);
		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
				R.id.backImg);
		signtitle.setText(AppConstant.channelName);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");

		signtitle.setTypeface(tf);

		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	private void getVimeoList(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					Gson g = new Gson();

					Type fooType = new TypeToken<ArrayList<VimeoInfo>>() {
					}.getType();
					vimeoChannelList = g.fromJson(new String(result), fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (vimeoChannelList.size() > 0) {

							gGridAdapter = new ListAdapter(con);
							vimeoListView.setAdapter(gGridAdapter);
							gGridAdapter.notifyDataSetChanged();
						}

					}

				});
			}
		});

		thread.start();

	}

	private class ListAdapter extends ArrayAdapter<VimeoInfo> {
		Context context;

		ListAdapter(Context context) {
			super(context, R.layout.vimeo_row, vimeoChannelList);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.vimeo_row, null);

			}

			if (position < vimeoChannelList.size()) {

				final VimeoInfo query = vimeoChannelList.get(position);

				final ImageView vimeoChannelImg = (ImageView) v
						.findViewById(R.id.vimeoChannelImg);
				final TextView vimeoTime = (TextView) v
						.findViewById(R.id.vimeoTime);
				final TextView vimeoTitle = (TextView) v
						.findViewById(R.id.vimeoTitle);
				final TextView vimeoUserName = (TextView) v
						.findViewById(R.id.vimeoUserName);
				final TextView vimeoUploadDate = (TextView) v
						.findViewById(R.id.vimeoUploadDate);

				vimeoTitle.setText(query.getTitle());
				vimeoTime.setText(convertToTime(query.getDuration()));
				vimeoUserName.setText(query.getUser_name());
				vimeoUploadDate.setText(query.getUpload_date());
				Picasso.with(con).load(query.getThumbnail_medium())
						.error(R.drawable.ic_launcher).into(vimeoChannelImg);
				vimeoTitle.setTypeface(tf3);
				vimeoTime.setTypeface(tf3);
				vimeoUserName.setTypeface(tf3);
				vimeoUploadDate.setTypeface(tf3);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					VimeoInfo query = vimeoChannelList.get(position);
					Log.e("Song id", ">>" + query.getId());
					AppConstant.videoTitle = query.getTitle();
					AppConstant.videoDescription = query.getDescription();
					getVimeoUrl(AllURL.getVimoeSongUrl(query.getId()));

				}
			});

			return v;

		}

	}

	public String convertToTime(String time) {

		String startTime = "00:00";
		int intTime = Integer.parseInt(time);
		int h = intTime / 60 + Integer.valueOf(startTime.substring(0, 1));
		int m = intTime % 60 + Integer.valueOf(startTime.substring(3, 4));
		String newtime = h + ":" + m;
		return newtime;
	}

	private void getVimeoUrl(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);
					JSONObject job = new JSONObject(result);
					JSONObject job1 = job.getJSONObject("request");
					JSONObject job2 = job1.getJSONObject("files");
					JSONObject job3 = job2.getJSONObject("h264");
					JSONObject job4 = job3.getJSONObject("sd");
					AppConstant.youtubeName = job4.optString("url");
					Log.e("Vimeo URl", ">>" + AppConstant.youtubeName);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}
						AppConstant.fromVimeoVideo = true;
						StartActivity.toActivity(con, VideoPlayActivity.class);

					}

				});
			}
		});

		thread.start();

	}

}
