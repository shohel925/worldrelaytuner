package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.PublicChannelInfo;
import com.aapbd.worldrelaytune.radio.StaticRadioArray;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DiscoverItemFlipper {

	ViewFlipper viewFlipper;
	Context con;
	Activity newActivty;

	Typeface t3;
	ListView discoverItemList;
	Typeface tf;
	Dialog dd;
	EditText et_search;
	Typeface tf3;
	LinearLayout discoverItemBack;
	TextView discoverItemTitile;
	DatabaseHandler db;
	JSONArray jArray;
	PublicAdapter publicAdapter;
	boolean flag=false;

	public DiscoverItemFlipper(ViewFlipper viewFlipper, Context con,
			Activity newActivty) {

		this.viewFlipper = viewFlipper;
		this.con = con;
		this.newActivty = newActivty;
		viewFlipper.setDisplayedChild(5);
		db = new DatabaseHandler(con);
		AnalyticsTracker.sendTrackData(newActivty, "Discover", "");
		AnalyticsTracker.sendEventData(newActivty, "Discover", "action", "label");
//		MyApplication.getInstance().trackEvent("Discover", "", "");
		initUi();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		discoverItemTitile = (TextView) newActivty.findViewById(R.id.discoverItemTitile);
		discoverItemBack = (LinearLayout) newActivty.findViewById(R.id.discoverItemBack);
		discoverItemList = (ListView) newActivty.findViewById(R.id.discoverItemList);
		tf = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Bold.ttf");
		tf3 = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Light.ttf");
		discoverItemTitile.setText(AppConstant.discoverType);
		publicAdapter = new PublicAdapter(con);
		discoverItemList.setAdapter(publicAdapter);
		publicAdapter.notifyDataSetChanged();

		discoverItemBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				viewFlipper.setDisplayedChild(2);

			}
		});

	}

	private class PublicAdapter extends ArrayAdapter<PublicChannelInfo> {
		Context context;

		PublicAdapter(Context context) {
			super(context, R.layout.category_item_rows,
					AppConstant.tempPublicList);

			this.context = context;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.category_item_rows, null);

			}

			if (position < AppConstant.tempPublicList.size()) {

				final PublicChannelInfo query = AppConstant.tempPublicList.get(position);

				final ImageView cat_item_Img = (ImageView) v.findViewById(R.id.cat_item_Img);
				final TextView cat_channelNumber = (TextView) v.findViewById(R.id.cat_channelNumber);
				final TextView catChannelName = (TextView) v.findViewById(R.id.catChannelName);
				Typeface tf3 = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Light.ttf");
				cat_channelNumber.setTypeface(tf);
				cat_channelNumber.setText(query.getWrid());
				catChannelName.setTypeface(tf);
				catChannelName.setText(query.getName());
				Log.e("SearchData", "" + query.getSearchData());
				Picasso.with(con).load(query.getChannel_image_file())
						.error(R.drawable.default_img).into(cat_item_Img);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					PublicChannelInfo publicChannel = AppConstant.tempPublicList.get(position);
//					if (publicChannel.getWrid().equalsIgnoreCase("11776")||publicChannel.getWrid().equalsIgnoreCase("11809")) {
//						AppConstant.channelInfo=publicChannel;
//						StartActivity.toActivity(con,TestActivity.class);
//						return;
//					}
					if (TextUtils.isEmpty(publicChannel.getAudio_location())) {

						if (publicChannel.getStream_type().equalsIgnoreCase("1")) {
							if (publicChannel.getVisual_content_location().startsWith("youtubelive")) {
								AppConstant.videoSource = "youtubelive";
								AppConstant.youtubeVideoUrl = publicChannel.getVisual_content_location().substring(publicChannel.getVisual_content_location().lastIndexOf("+") + 1);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							} else {
								AppConstant.videoSource = "normal";
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);

							}
						} else if (publicChannel.getStream_type().equalsIgnoreCase("2")) {
							if (TextUtils.isEmpty(publicChannel.getStream_vod_url())) {
								AppConstant.videoSource = "youtubelive";
								AppConstant.youtubeVideoUrl = publicChannel.getVisual_content_location().substring(publicChannel.getVisual_content_location().lastIndexOf("+") + 1);
								Log.e("Youtube video Live Name", ">>" + AppConstant.youtubeVideoUrl);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							} else {
								if ((publicChannel.getStream_vod_url().startsWith("http://vimeo")) || (publicChannel.getStream_vod_url().startsWith("https://vimeo"))) {
									AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
									Log.e("Vimeo Name", ">>" + AppConstant.youtubeName);
									AppConstant.videoSource = "vimeo";
									AppConstant.channelInfo = publicChannel;
									AppConstant.channelName = publicChannel.getName();
									StartActivity.toActivity(con, VideoPlayActivity.class);
								} else if (publicChannel.getStream_vod_url().startsWith("youtube")) {
									AppConstant.videoSource = "youtubeUser";
									if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube User", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

									} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										StartActivity.toActivity(con, VideoPlayActivity.class);
									}else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube User", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

									}

								} else if (publicChannel.getStream_vod_url().startsWith("vevo")) {
									// ===== This condition for Vevo ======
									AppConstant.youtubeName = publicChannel.getStream_vod_url();
									Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
									Toast.makeText(con, "Vevo channels are currently not supported on Android", Toast.LENGTH_SHORT).show();
								}
							}

						} else if (publicChannel.getStream_type().equalsIgnoreCase("3")) {
							AppConstant.videoSource = "normalAndDemand";
							if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube User", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

							} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								StartActivity.toActivity(con, VideoPlayActivity.class);
							}
							else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube Channel", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

							}else {
								AppConstant.ondemendSOurce = "vimeo";
								AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							}

						}

					} else {
						if (publicChannel.getStream_type().equalsIgnoreCase("3")) {
							AppConstant.videoSource = "audio";
							flag=true;
							AppConstant.fromAudio = true;
							StaticRadioArray.urls[0] = publicChannel.getAudio_location();
							AppConstant.channelName = publicChannel.getName();
							AppConstant.channelInfo = publicChannel;
							AppConstant.visual_content_location = publicChannel.getVisual_content_location();
							getMaxImage(publicChannel.getVisual_content_location() + "maxNumOfImages");

							if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube User", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName2(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

							} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								//StartActivity.toActivity(con, VideoPlayActivity.class);
							}else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube Channel", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName2(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

							} else {
								AppConstant.ondemendSOurce = "vimeo";

								AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								//StartActivity.toActivity(con, VideoPlayActivity.class);
							}

						} else {
							AppConstant.videoSource = "audio";
							AppConstant.fromAudio = true;
							StaticRadioArray.urls[0] = publicChannel.getAudio_location();
							AppConstant.channelName = publicChannel.getName();
							AppConstant.channelInfo = publicChannel;
							AppConstant.visual_content_location = publicChannel.getVisual_content_location();
							getMaxImage(publicChannel.getVisual_content_location() + "maxNumOfImages");
						}


					}

				}
			});

			return v;

		}

	}

	private void getChannelName2(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
								  byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				try {
					JSONObject job = new JSONObject(new String(response));
					jArray = job.getJSONArray("items");
					JSONObject jholder = jArray.getJSONObject(0);
					JSONObject jholder2 = jholder
							.getJSONObject("contentDetails");
					JSONObject jholder3 = jholder2
							.getJSONObject("relatedPlaylists");
					AppConstant.youtubeName = jholder3.optString("uploads");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jArray.length() > 0) {

//					if(flag){
//						flag=false;
//					}else{
//						StartActivity.toActivity(con, VideoPlayActivity.class);
//					}


				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
								  byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getChannelName(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				try {
					JSONObject job = new JSONObject(new String(response));
					jArray = job.getJSONArray("items");
					JSONObject jholder = jArray.getJSONObject(0);
					JSONObject jholder2 = jholder
							.getJSONObject("contentDetails");
					JSONObject jholder3 = jholder2
							.getJSONObject("relatedPlaylists");
					AppConstant.youtubeName = jholder3.optString("uploads");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jArray.length() > 0) {

					if(flag){
						flag=false;
					}else{
						StartActivity.toActivity(con, VideoPlayActivity.class);
					}
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getMaxImage(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));
				String imageCount = new String(response);
				AppConstant.imageSize = Integer.parseInt(imageCount);
				Log.e("Image Size", ">>" + AppConstant.imageSize);
				StartActivity.toActivity(con, VideoPlayActivity.class);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

}
