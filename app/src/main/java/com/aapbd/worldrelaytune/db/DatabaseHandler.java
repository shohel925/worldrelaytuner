package com.aapbd.worldrelaytune.db;

import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.aapbd.worldrelaytune.model.ChannelInfo;
import com.aapbd.worldrelaytune.model.PublicChannelInfo;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "channelInfo";

	// allChannel table name
	private static final String TABLE_Public_channel = "Public_channel";

	private static final String TABLE_channel = "channel";

	// ========= for Public Channel ==========================

	private static final String KEY_public_id = "public_id";
	private static final String KEY_provider_id = "provider_id";
	private static final String KEY_countrycode = "countrycode";
	private static final String KEY_tag1 = "tag1";
	private static final String KEY_tag2 = "tag2";
	private static final String KEY_content_focus = "content_focus";
	private static final String KEY_visual_content_format = "visual_content_format";
	private static final String KEY_visual_content_type = "visual_content_type";
	private static final String KEY_stream_vod_url = "stream_vod_url";
	private static final String KEY_channel_format = "channel_format";
	private static final String KEY_visual_content_location = "visual_content_location";
	private static final String KEY_type = "type";
	private static final String KEY_audio_location = "audio_location";
	private static final String KEY_language = "language";
	private static final String KEY_stream_type = "stream_type";
	private static final String KEY_active = "active";
	private static final String KEY_stream_vod_user = "stream_vod_user";
	private static final String KEY_genre0 = "genre0";
	private static final String KEY_subscription_plan = "subscription_plan";
	private static final String KEY_wrid = "wrid";
	private static final String KEY_name = "name";
	private static final String KEY_url = "url";
	private static final String KEY_stream_vod_password = "stream_vod_password";
	private static final String KEY_audio_location_2 = "audio_location_2";
	private static final String KEY_visual_content_location_2 = "visual_content_location_2";
	private static final String KEY_channel_info = "channel_info";
	private static final String KEY_channel_image_file = "channel_image_file";
	private static final String KEY_favorites = "favorites";
	private static final String KEY_searchData = "searchData";

	// ========= for Channel ==========================

	private static final String KEY_channel_id = "channel_id";
	private static final String KEY_long_desc = "long_desc";
	private static final String KEY_description = "description";
	private static final String KEY_language_channel = "language_channel";
	private static final String KEY_channel_name = "channel_name";
	private static final String KEY_country = "country";
	private static final String KEY_icon_image = "icon_image";
	private static final String KEY_channel_number = "channel_number";
	private static final String KEY_genre = "genre";
	private static final String KEY_type_channel = "type_channel";
	private static final String KEY_website_link = "website_link";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// ========= for Public Channel ==========================

	final String CREATE_CHANNEL_TABLE = "CREATE TABLE " + TABLE_Public_channel
			+ "(" + KEY_public_id + " INTEGER PRIMARY KEY , " + KEY_provider_id
			+ " TEXT , " + KEY_countrycode + " TEXT , " + KEY_tag1 + " TEXT , "
			+ KEY_tag2 + " TEXT , " + KEY_content_focus + " TEXT , "
			+ KEY_visual_content_format + " TEXT , " + KEY_visual_content_type
			+ " TEXT , " + KEY_stream_vod_url + " TEXT , " + KEY_channel_format
			+ " TEXT , " + KEY_visual_content_location + " TEXT , " + KEY_type
			+ " TEXT , " + KEY_audio_location + " TEXT , " + KEY_language
			+ " TEXT , " + KEY_stream_type + " TEXT , " + KEY_active
			+ " TEXT , " + KEY_stream_vod_user + " TEXT , " + KEY_genre0
			+ " TEXT , " + KEY_subscription_plan + " TEXT , " + KEY_wrid
			+ " TEXT , " + KEY_name + " TEXT , " + KEY_url + " TEXT , "
			+ KEY_stream_vod_password + " TEXT , " + KEY_audio_location_2
			+ " TEXT , " + KEY_visual_content_location_2 + " TEXT , "
			+ KEY_channel_info + " TEXT , " + KEY_channel_image_file
			+ " TEXT , " + KEY_favorites + " TEXT , " + KEY_searchData
			+ " TEXT " + ")";

	// ========= for Channel ==========================

	final String CREATE_CHANNEL_TABLE2 = "CREATE TABLE " + TABLE_channel + "("
			+ KEY_channel_id + " INTEGER PRIMARY KEY , " + KEY_long_desc
			+ " TEXT , " + KEY_description + " TEXT , " + KEY_language_channel
			+ " TEXT , " + KEY_channel_name + " TEXT , " + KEY_country
			+ " TEXT , " + KEY_icon_image + " TEXT , " + KEY_channel_number
			+ " TEXT , " + KEY_genre + " TEXT , " + KEY_type_channel
			+ " TEXT , " + KEY_website_link + " TEXT " + ")";

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_CHANNEL_TABLE);
		db.execSQL(CREATE_CHANNEL_TABLE2);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Public_channel);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_channel);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// ========= for Public Channel ==========================

	public void addChannelInfo(PublicChannelInfo pInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_provider_id, pInfo.getProvider_id());
		values.put(KEY_countrycode, pInfo.getCountrycode());
		values.put(KEY_tag1, pInfo.getTag1());
		values.put(KEY_tag2, pInfo.getTag2());
		values.put(KEY_content_focus, pInfo.getContent_focus());
		values.put(KEY_visual_content_format, pInfo.getVisual_content_format());
		values.put(KEY_visual_content_type, pInfo.getVisual_content_type());
		values.put(KEY_stream_vod_url, pInfo.getStream_vod_url());
		values.put(KEY_channel_format, pInfo.getChannel_format());
		values.put(KEY_visual_content_location,
				pInfo.getVisual_content_location());
		values.put(KEY_type, pInfo.getType());
		values.put(KEY_audio_location, pInfo.getAudio_location());
		values.put(KEY_language, pInfo.getLanguage());
		values.put(KEY_stream_type, pInfo.getStream_type());
		values.put(KEY_active, pInfo.getActive());
		values.put(KEY_stream_vod_user, pInfo.getStream_vod_user());
		values.put(KEY_genre0, pInfo.getGenre0());
		values.put(KEY_subscription_plan, pInfo.getSubscription_plan());
		values.put(KEY_wrid, pInfo.getWrid());
		values.put(KEY_name, pInfo.getName());
		values.put(KEY_url, pInfo.getUrl());
		values.put(KEY_stream_vod_password, pInfo.getStream_vod_password());
		values.put(KEY_audio_location_2, pInfo.getAudio_location_2());
		values.put(KEY_visual_content_location_2,
				pInfo.getVisual_content_location_2());
		values.put(KEY_channel_info, pInfo.getChannel_info());
		values.put(KEY_channel_image_file, pInfo.getChannel_image_file());
		values.put(KEY_favorites, pInfo.getFavorites());
		values.put(KEY_searchData, pInfo.getName() + " " + pInfo.getWrid());

		// Inserting Row
		db.insert(TABLE_Public_channel, null, values);
		db.close(); // Closing database connection
	}

	// ========= for Channel ==========================

	public void addChannelInfo2(ChannelInfo cInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_long_desc, cInfo.getLong_desc());
		values.put(KEY_description, cInfo.getDescription());
		values.put(KEY_language_channel, cInfo.getLanguage());
		values.put(KEY_channel_name, cInfo.getChannel_name());
		values.put(KEY_country, cInfo.getCountry());
		values.put(KEY_icon_image, cInfo.getIcon_image());
		values.put(KEY_channel_number, cInfo.getChannel_number());
		values.put(KEY_genre, cInfo.getGenre());
		values.put(KEY_type_channel, cInfo.getType());
		values.put(KEY_website_link, cInfo.getWebsite_link());

		// Inserting Row
		db.insert(TABLE_channel, null, values);
		db.close(); // Closing database connection
	}

	public Vector<ChannelInfo> getAllChannel() {
		final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_channel;

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ChannelInfo pInfo = new ChannelInfo();
				pInfo.setLong_desc(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setLanguage(cursor.getString(3));
				pInfo.setChannel_name(cursor.getString(4));
				pInfo.setCountry(cursor.getString(5));
				pInfo.setIcon_image(cursor.getString(6));
				pInfo.setChannel_number(cursor.getString(7));
				pInfo.setGenre(cursor.getString(8));
				pInfo.setType(cursor.getString(9));
				pInfo.setWebsite_link(cursor.getString(10));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<ChannelInfo> getCountryFromChannelNumber(String channelNumber) {
		final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_channel + " WHERE "
				+ KEY_channel_number + " = '" + channelNumber + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ChannelInfo pInfo = new ChannelInfo();
				pInfo.setLong_desc(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setLanguage(cursor.getString(3));
				pInfo.setChannel_name(cursor.getString(4));
				pInfo.setCountry(cursor.getString(5));
				pInfo.setIcon_image(cursor.getString(6));
				pInfo.setChannel_number(cursor.getString(7));
				pInfo.setGenre(cursor.getString(8));
				pInfo.setType(cursor.getString(9));
				pInfo.setWebsite_link(cursor.getString(10));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<ChannelInfo> getAllChannelByType(String typeId) {
		final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_channel + " WHERE "
				+ KEY_type_channel + " = '" + typeId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ChannelInfo pInfo = new ChannelInfo();
				pInfo.setLong_desc(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setLanguage(cursor.getString(3));
				pInfo.setChannel_name(cursor.getString(4));
				pInfo.setCountry(cursor.getString(5));
				pInfo.setIcon_image(cursor.getString(6));
				pInfo.setChannel_number(cursor.getString(7));
				pInfo.setGenre(cursor.getString(8));
				pInfo.setType(cursor.getString(9));
				pInfo.setWebsite_link(cursor.getString(10));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<ChannelInfo> getAllChannelByGenre(String genreId) {
		final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_channel + " WHERE "
				+ KEY_genre + " = '" + genreId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ChannelInfo pInfo = new ChannelInfo();
				pInfo.setLong_desc(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setLanguage(cursor.getString(3));
				pInfo.setChannel_name(cursor.getString(4));
				pInfo.setCountry(cursor.getString(5));
				pInfo.setIcon_image(cursor.getString(6));
				pInfo.setChannel_number(cursor.getString(7));
				pInfo.setGenre(cursor.getString(8));
				pInfo.setType(cursor.getString(9));
				pInfo.setWebsite_link(cursor.getString(10));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<ChannelInfo> getAllChannelByCountry(String countryName) {
		final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_channel + " WHERE "
				+ KEY_country + " = '" + countryName + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ChannelInfo pInfo = new ChannelInfo();
				pInfo.setLong_desc(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setLanguage(cursor.getString(3));
				pInfo.setChannel_name(cursor.getString(4));
				pInfo.setCountry(cursor.getString(5));
				pInfo.setIcon_image(cursor.getString(6));
				pInfo.setChannel_number(cursor.getString(7));
				pInfo.setGenre(cursor.getString(8));
				pInfo.setType(cursor.getString(9));
				pInfo.setWebsite_link(cursor.getString(10));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<ChannelInfo> getAllChannelByLanguage(String languageName) {
		final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_channel + " WHERE "
				+ KEY_language_channel + " = '" + languageName + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ChannelInfo pInfo = new ChannelInfo();
				pInfo.setLong_desc(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setLanguage(cursor.getString(3));
				pInfo.setChannel_name(cursor.getString(4));
				pInfo.setCountry(cursor.getString(5));
				pInfo.setIcon_image(cursor.getString(6));
				pInfo.setChannel_number(cursor.getString(7));
				pInfo.setGenre(cursor.getString(8));
				pInfo.setType(cursor.getString(9));
				pInfo.setWebsite_link(cursor.getString(10));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<PublicChannelInfo> getAllPublicChannelInfo() {
		final Vector<PublicChannelInfo> ExternalLinkList = new Vector<PublicChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel;

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PublicChannelInfo pInfo = new PublicChannelInfo();
				pInfo.setProvider_id(cursor.getString(1));
				pInfo.setCountrycode(cursor.getString(2));
				pInfo.setTag1(cursor.getString(3));
				pInfo.setTag2(cursor.getString(4));
				pInfo.setContent_focus(cursor.getString(5));
				pInfo.setVisual_content_format(cursor.getString(6));
				pInfo.setVisual_content_type(cursor.getString(7));
				pInfo.setStream_vod_url(cursor.getString(8));
				pInfo.setChannel_format(cursor.getString(9));
				pInfo.setVisual_content_location(cursor.getString(10));
				pInfo.setType(cursor.getString(11));
				pInfo.setAudio_location(cursor.getString(12));
				pInfo.setLanguage(cursor.getString(13));
				pInfo.setStream_type(cursor.getString(14));
				pInfo.setActive(cursor.getString(15));
				pInfo.setStream_vod_user(cursor.getString(16));
				pInfo.setGenre0(cursor.getString(17));
				pInfo.setSubscription_plan(cursor.getString(18));
				pInfo.setWrid(cursor.getString(19));
				pInfo.setName(cursor.getString(20));
				pInfo.setUrl(cursor.getString(21));
				pInfo.setStream_vod_password(cursor.getString(22));
				pInfo.setAudio_location_2(cursor.getString(23));
				pInfo.setVisual_content_location_2(cursor.getString(24));
				pInfo.setChannel_info(cursor.getString(25));
				pInfo.setChannel_image_file(cursor.getString(26));
				pInfo.setFavorites(cursor.getString(27));
				pInfo.setSearchData(cursor.getString(28));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<PublicChannelInfo> getPublicChannelInfoByChannelNumber(
			String channelNumber) {
		final Vector<PublicChannelInfo> ExternalLinkList = new Vector<PublicChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel
				+ " WHERE " + KEY_wrid + " = '" + channelNumber + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PublicChannelInfo pInfo = new PublicChannelInfo();
				pInfo.setProvider_id(cursor.getString(1));
				pInfo.setCountrycode(cursor.getString(2));
				pInfo.setTag1(cursor.getString(3));
				pInfo.setTag2(cursor.getString(4));
				pInfo.setContent_focus(cursor.getString(5));
				pInfo.setVisual_content_format(cursor.getString(6));
				pInfo.setVisual_content_type(cursor.getString(7));
				pInfo.setStream_vod_url(cursor.getString(8));
				pInfo.setChannel_format(cursor.getString(9));
				pInfo.setVisual_content_location(cursor.getString(10));
				pInfo.setType(cursor.getString(11));
				pInfo.setAudio_location(cursor.getString(12));
				pInfo.setLanguage(cursor.getString(13));
				pInfo.setStream_type(cursor.getString(14));
				pInfo.setActive(cursor.getString(15));
				pInfo.setStream_vod_user(cursor.getString(16));
				pInfo.setGenre0(cursor.getString(17));
				pInfo.setSubscription_plan(cursor.getString(18));
				pInfo.setWrid(cursor.getString(19));
				pInfo.setName(cursor.getString(20));
				pInfo.setUrl(cursor.getString(21));
				pInfo.setStream_vod_password(cursor.getString(22));
				pInfo.setAudio_location_2(cursor.getString(23));
				pInfo.setVisual_content_location_2(cursor.getString(24));
				pInfo.setChannel_info(cursor.getString(25));
				pInfo.setChannel_image_file(cursor.getString(26));
				pInfo.setFavorites(cursor.getString(27));
				pInfo.setSearchData(cursor.getString(28));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<PublicChannelInfo> getAllDiscover(String name) {
		final Vector<PublicChannelInfo> ExternalLinkList = new Vector<PublicChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb

		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel
				+ " WHERE " + KEY_wrid + " IN (" + name + ")";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PublicChannelInfo pInfo = new PublicChannelInfo();
				pInfo.setProvider_id(cursor.getString(1));
				pInfo.setCountrycode(cursor.getString(2));
				pInfo.setTag1(cursor.getString(3));
				pInfo.setTag2(cursor.getString(4));
				pInfo.setContent_focus(cursor.getString(5));
				pInfo.setVisual_content_format(cursor.getString(6));
				pInfo.setVisual_content_type(cursor.getString(7));
				pInfo.setStream_vod_url(cursor.getString(8));
				pInfo.setChannel_format(cursor.getString(9));
				pInfo.setVisual_content_location(cursor.getString(10));
				pInfo.setType(cursor.getString(11));
				pInfo.setAudio_location(cursor.getString(12));
				pInfo.setLanguage(cursor.getString(13));
				pInfo.setStream_type(cursor.getString(14));
				pInfo.setActive(cursor.getString(15));
				pInfo.setStream_vod_user(cursor.getString(16));
				pInfo.setGenre0(cursor.getString(17));
				pInfo.setSubscription_plan(cursor.getString(18));
				pInfo.setWrid(cursor.getString(19));
				pInfo.setName(cursor.getString(20));
				pInfo.setUrl(cursor.getString(21));
				pInfo.setStream_vod_password(cursor.getString(22));
				pInfo.setAudio_location_2(cursor.getString(23));
				pInfo.setVisual_content_location_2(cursor.getString(24));
				pInfo.setChannel_info(cursor.getString(25));
				pInfo.setChannel_image_file(cursor.getString(26));
				pInfo.setFavorites(cursor.getString(27));
				pInfo.setSearchData(cursor.getString(28));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<PublicChannelInfo> getAllSearchData(String channelNameOrNumber) {
		final Vector<PublicChannelInfo> ExternalLinkList = new Vector<PublicChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		// \"" + sCat + "\"
		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel
				+ " WHERE " + KEY_searchData + " like '%" + channelNameOrNumber
				+ "%' OR " + KEY_channel_info + " like '%"
				+ channelNameOrNumber + "%'";
		Log.e("searchQuery", "" + selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PublicChannelInfo pInfo = new PublicChannelInfo();
				pInfo.setProvider_id(cursor.getString(1));
				pInfo.setCountrycode(cursor.getString(2));
				pInfo.setTag1(cursor.getString(3));
				pInfo.setTag2(cursor.getString(4));
				pInfo.setContent_focus(cursor.getString(5));
				pInfo.setVisual_content_format(cursor.getString(6));
				pInfo.setVisual_content_type(cursor.getString(7));
				pInfo.setStream_vod_url(cursor.getString(8));
				pInfo.setChannel_format(cursor.getString(9));
				pInfo.setVisual_content_location(cursor.getString(10));
				pInfo.setType(cursor.getString(11));
				pInfo.setAudio_location(cursor.getString(12));
				pInfo.setLanguage(cursor.getString(13));
				pInfo.setStream_type(cursor.getString(14));
				pInfo.setActive(cursor.getString(15));
				pInfo.setStream_vod_user(cursor.getString(16));
				pInfo.setGenre0(cursor.getString(17));
				pInfo.setSubscription_plan(cursor.getString(18));
				pInfo.setWrid(cursor.getString(19));
				pInfo.setName(cursor.getString(20));
				pInfo.setUrl(cursor.getString(21));
				pInfo.setStream_vod_password(cursor.getString(22));
				pInfo.setAudio_location_2(cursor.getString(23));
				pInfo.setVisual_content_location_2(cursor.getString(24));
				pInfo.setChannel_info(cursor.getString(25));
				pInfo.setChannel_image_file(cursor.getString(26));
				pInfo.setFavorites(cursor.getString(27));
				pInfo.setSearchData(cursor.getString(28));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<PublicChannelInfo> getFavortesInfo(String channelId) {
		final Vector<PublicChannelInfo> ExternalLinkList = new Vector<PublicChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel
				+ " WHERE " + KEY_wrid + " = '" + channelId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PublicChannelInfo pInfo = new PublicChannelInfo();
				pInfo.setProvider_id(cursor.getString(1));
				pInfo.setCountrycode(cursor.getString(2));
				pInfo.setTag1(cursor.getString(3));
				pInfo.setTag2(cursor.getString(4));
				pInfo.setContent_focus(cursor.getString(5));
				pInfo.setVisual_content_format(cursor.getString(6));
				pInfo.setVisual_content_type(cursor.getString(7));
				pInfo.setStream_vod_url(cursor.getString(8));
				pInfo.setChannel_format(cursor.getString(9));
				pInfo.setVisual_content_location(cursor.getString(10));
				pInfo.setType(cursor.getString(11));
				pInfo.setAudio_location(cursor.getString(12));
				pInfo.setLanguage(cursor.getString(13));
				pInfo.setStream_type(cursor.getString(14));
				pInfo.setActive(cursor.getString(15));
				pInfo.setStream_vod_user(cursor.getString(16));
				pInfo.setGenre0(cursor.getString(17));
				pInfo.setSubscription_plan(cursor.getString(18));
				pInfo.setWrid(cursor.getString(19));
				pInfo.setName(cursor.getString(20));
				pInfo.setUrl(cursor.getString(21));
				pInfo.setStream_vod_password(cursor.getString(22));
				pInfo.setAudio_location_2(cursor.getString(23));
				pInfo.setVisual_content_location_2(cursor.getString(24));
				pInfo.setChannel_info(cursor.getString(25));
				pInfo.setChannel_image_file(cursor.getString(26));
				pInfo.setFavorites(cursor.getString(27));
				pInfo.setSearchData(cursor.getString(28));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<PublicChannelInfo> getCategoryType(String typeId) {
		final Vector<PublicChannelInfo> ExternalLinkList = new Vector<PublicChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel
				+ " WHERE " + KEY_type + " = '" + typeId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PublicChannelInfo pInfo = new PublicChannelInfo();
				pInfo.setProvider_id(cursor.getString(1));
				pInfo.setCountrycode(cursor.getString(2));
				pInfo.setTag1(cursor.getString(3));
				pInfo.setTag2(cursor.getString(4));
				pInfo.setContent_focus(cursor.getString(5));
				pInfo.setVisual_content_format(cursor.getString(6));
				pInfo.setVisual_content_type(cursor.getString(7));
				pInfo.setStream_vod_url(cursor.getString(8));
				pInfo.setChannel_format(cursor.getString(9));
				pInfo.setVisual_content_location(cursor.getString(10));
				pInfo.setType(cursor.getString(11));
				pInfo.setAudio_location(cursor.getString(12));
				pInfo.setLanguage(cursor.getString(13));
				pInfo.setStream_type(cursor.getString(14));
				pInfo.setActive(cursor.getString(15));
				pInfo.setStream_vod_user(cursor.getString(16));
				pInfo.setGenre0(cursor.getString(17));
				pInfo.setSubscription_plan(cursor.getString(18));
				pInfo.setWrid(cursor.getString(19));
				pInfo.setName(cursor.getString(20));
				pInfo.setUrl(cursor.getString(21));
				pInfo.setStream_vod_password(cursor.getString(22));
				pInfo.setAudio_location_2(cursor.getString(23));
				pInfo.setVisual_content_location_2(cursor.getString(24));
				pInfo.setChannel_info(cursor.getString(25));
				pInfo.setChannel_image_file(cursor.getString(26));
				pInfo.setFavorites(cursor.getString(27));
				pInfo.setSearchData(cursor.getString(28));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public Vector<PublicChannelInfo> getAllFavorites() {
		final Vector<PublicChannelInfo> ExternalLinkList = new Vector<PublicChannelInfo>();
		ExternalLinkList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel
				+ " WHERE " + KEY_favorites + " = '1'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PublicChannelInfo pInfo = new PublicChannelInfo();
				pInfo.setProvider_id(cursor.getString(1));
				pInfo.setCountrycode(cursor.getString(2));
				pInfo.setTag1(cursor.getString(3));
				pInfo.setTag2(cursor.getString(4));
				pInfo.setContent_focus(cursor.getString(5));
				pInfo.setVisual_content_format(cursor.getString(6));
				pInfo.setVisual_content_type(cursor.getString(7));
				pInfo.setStream_vod_url(cursor.getString(8));
				pInfo.setChannel_format(cursor.getString(9));
				pInfo.setVisual_content_location(cursor.getString(10));
				pInfo.setType(cursor.getString(11));
				pInfo.setAudio_location(cursor.getString(12));
				pInfo.setLanguage(cursor.getString(13));
				pInfo.setStream_type(cursor.getString(14));
				pInfo.setActive(cursor.getString(15));
				pInfo.setStream_vod_user(cursor.getString(16));
				pInfo.setGenre0(cursor.getString(17));
				pInfo.setSubscription_plan(cursor.getString(18));
				pInfo.setWrid(cursor.getString(19));
				pInfo.setName(cursor.getString(20));
				pInfo.setUrl(cursor.getString(21));
				pInfo.setStream_vod_password(cursor.getString(22));
				pInfo.setAudio_location_2(cursor.getString(23));
				pInfo.setVisual_content_location_2(cursor.getString(24));
				pInfo.setChannel_info(cursor.getString(25));
				pInfo.setChannel_image_file(cursor.getString(26));
				pInfo.setFavorites(cursor.getString(27));
				pInfo.setSearchData(cursor.getString(28));
				ExternalLinkList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return ExternalLinkList;
	}

	public boolean ifFavorites(String channelId) {
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Public_channel
				+ " WHERE " + KEY_favorites + " = '1'" + " AND " + KEY_wrid
				+ " = '" + channelId + "'";
		boolean favorites = false;

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.getCount() > 0) {
			favorites = true;
		} else {
			favorites = false;
		}
		return favorites;
	}

	public boolean ifExist(String channelId) {
		boolean isChannel = false;

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_Public_channel + " WHERE " + KEY_wrid
				+ " = '" + channelId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			isChannel = true;

		} else {
			isChannel = false;
		}

		db.close(); // Closing database connection

		return isChannel;

	}

	public boolean ifChannelExist(String channelId) {
		boolean isChannel = false;

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_channel + " WHERE "
				+ KEY_channel_number + " = '" + channelId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			isChannel = true;

		} else {
			isChannel = false;
		}

		db.close(); // Closing database connection

		return isChannel;

	}

	public void updateFavorites(String channelId) {

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery1 = " Update "
				+ DatabaseHandler.TABLE_Public_channel + " set "
				+ DatabaseHandler.KEY_favorites + "='1'  where "
				+ DatabaseHandler.KEY_wrid + "='" + channelId + "'";

		Log.e("update ", ">>" + selectQuery1);

		// db.rawQuery(selectQuery1, null);
		db.execSQL(selectQuery1);

		db.close(); // Closing database connection
	}
	public void RemoveAllFavorites() {

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery1 = " Update "
				+ DatabaseHandler.TABLE_Public_channel + " set "
				+ DatabaseHandler.KEY_favorites + "='0'";

		Log.e("update ", ">>" + selectQuery1);

		// db.rawQuery(selectQuery1, null);
		db.execSQL(selectQuery1);

		db.close(); // Closing database connection
	}

	public void removeFavorites(String channelId) {

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery1 = " Update "
				+ DatabaseHandler.TABLE_Public_channel + " set "
				+ DatabaseHandler.KEY_favorites + "='0'  where "
				+ DatabaseHandler.KEY_wrid + "='" + channelId + "'";

		Log.e("update ", ">>" + selectQuery1);

		// db.rawQuery(selectQuery1, null);
		db.execSQL(selectQuery1);

		db.close(); // Closing database connection
	}

	// public Vector<ChannelInfo> getAllChannelByGenre(String genreId) {
	// final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
	// ExternalLinkList.removeAllElements();
	// // Select All Queryb
	// final String selectQuery = "SELECT * FROM " + TABLE_allChannel+" WHERE "
	// + KEY_genre + " = "+genreId;
	//
	//
	// final SQLiteDatabase db = getWritableDatabase();
	// final Cursor cursor = db.rawQuery(selectQuery, null);
	//
	// // looping through all rows and adding to list
	// if (cursor.moveToFirst()) {
	// do {
	// ChannelInfo eInfo = new ChannelInfo();
	// eInfo.setAdded_by(cursor.getString(1));
	// eInfo.setAdmin_status(cursor.getString(2));
	// eInfo.setId(cursor.getString(3));
	// eInfo.setChannel_name(cursor.getString(4));
	// eInfo.setChannel_number(cursor.getString(5));
	// eInfo.setCountry(cursor.getString(6));
	// eInfo.setCreated_date(cursor.getString(7));
	// eInfo.setDescription(cursor.getString(8));
	// eInfo.setFormat(cursor.getString(9));
	// eInfo.setGenre(cursor.getString(10));
	// eInfo.setGroup(cursor.getString(11));
	// eInfo.setIcon_image(cursor.getString(12));
	// eInfo.setLanguage(cursor.getString(13));
	// eInfo.setLong_desc(cursor.getString(14));
	// eInfo.setStatus(cursor.getString(15));
	// eInfo.setStream(cursor.getString(16));
	// eInfo.setStreaming_link(cursor.getString(17));
	// eInfo.setStreaming_url(cursor.getString(18));
	// eInfo.setType(cursor.getString(19));
	// eInfo.setVisibility_type(cursor.getString(20));
	// eInfo.setWebsite_link(cursor.getString(21));
	//
	//
	//
	// ExternalLinkList.addElement(eInfo);
	// eInfo = null;
	// } while (cursor.moveToNext());
	// }
	//
	// // return contact list
	// return ExternalLinkList;
	// }
	//
	// public Vector<ChannelInfo> getAllChannelByLanguage(String language) {
	// final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
	// ExternalLinkList.removeAllElements();
	// // Select All Queryb
	// final String selectQuery = "SELECT * FROM " + TABLE_allChannel+" WHERE "
	// + KEY_language + " = '"+ language +"'";
	//
	//
	// final SQLiteDatabase db = getWritableDatabase();
	// final Cursor cursor = db.rawQuery(selectQuery, null);
	//
	// // looping through all rows and adding to list
	// if (cursor.moveToFirst()) {
	// do {
	// ChannelInfo eInfo = new ChannelInfo();
	// eInfo.setAdded_by(cursor.getString(1));
	// eInfo.setAdmin_status(cursor.getString(2));
	// eInfo.setId(cursor.getString(3));
	// eInfo.setChannel_name(cursor.getString(4));
	// eInfo.setChannel_number(cursor.getString(5));
	// eInfo.setCountry(cursor.getString(6));
	// eInfo.setCreated_date(cursor.getString(7));
	// eInfo.setDescription(cursor.getString(8));
	// eInfo.setFormat(cursor.getString(9));
	// eInfo.setGenre(cursor.getString(10));
	// eInfo.setGroup(cursor.getString(11));
	// eInfo.setIcon_image(cursor.getString(12));
	// eInfo.setLanguage(cursor.getString(13));
	// eInfo.setLong_desc(cursor.getString(14));
	// eInfo.setStatus(cursor.getString(15));
	// eInfo.setStream(cursor.getString(16));
	// eInfo.setStreaming_link(cursor.getString(17));
	// eInfo.setStreaming_url(cursor.getString(18));
	// eInfo.setType(cursor.getString(19));
	// eInfo.setVisibility_type(cursor.getString(20));
	// eInfo.setWebsite_link(cursor.getString(21));
	//
	//
	//
	// ExternalLinkList.addElement(eInfo);
	// eInfo = null;
	// } while (cursor.moveToNext());
	// }
	//
	// // return contact list
	// return ExternalLinkList;
	// }
	//
	// public Vector<ChannelInfo> getAllChannelByCountry(String country) {
	// final Vector<ChannelInfo> ExternalLinkList = new Vector<ChannelInfo>();
	// ExternalLinkList.removeAllElements();
	// // Select All Queryb
	// final String selectQuery = "SELECT * FROM " + TABLE_allChannel+" WHERE "
	// + KEY_country + " = '"+country+"'";
	//
	//
	// final SQLiteDatabase db = getWritableDatabase();
	// final Cursor cursor = db.rawQuery(selectQuery, null);
	//
	// // looping through all rows and adding to list
	// if (cursor.moveToFirst()) {
	// do {
	// ChannelInfo eInfo = new ChannelInfo();
	// eInfo.setAdded_by(cursor.getString(1));
	// eInfo.setAdmin_status(cursor.getString(2));
	// eInfo.setId(cursor.getString(3));
	// eInfo.setChannel_name(cursor.getString(4));
	// eInfo.setChannel_number(cursor.getString(5));
	// eInfo.setCountry(cursor.getString(6));
	// eInfo.setCreated_date(cursor.getString(7));
	// eInfo.setDescription(cursor.getString(8));
	// eInfo.setFormat(cursor.getString(9));
	// eInfo.setGenre(cursor.getString(10));
	// eInfo.setGroup(cursor.getString(11));
	// eInfo.setIcon_image(cursor.getString(12));
	// eInfo.setLanguage(cursor.getString(13));
	// eInfo.setLong_desc(cursor.getString(14));
	// eInfo.setStatus(cursor.getString(15));
	// eInfo.setStream(cursor.getString(16));
	// eInfo.setStreaming_link(cursor.getString(17));
	// eInfo.setStreaming_url(cursor.getString(18));
	// eInfo.setType(cursor.getString(19));
	// eInfo.setVisibility_type(cursor.getString(20));
	// eInfo.setWebsite_link(cursor.getString(21));
	//
	//
	//
	// ExternalLinkList.addElement(eInfo);
	// eInfo = null;
	// } while (cursor.moveToNext());
	// }
	//
	// // return contact list
	// return ExternalLinkList;
	// }

	public void deleteAllChannelInfo() {

		final SQLiteDatabase db = getWritableDatabase();
		final String query1 = "DELETE " + " FROM "
				+ DatabaseHandler.TABLE_Public_channel + " where "
				+ DatabaseHandler.KEY_public_id + ">-1";

		db.execSQL(query1);
		// db.delete(DatabaseHandler.TABLE_CONTACTS,
		// DatabaseHandler.KEY_ID + " = ?", new String[] { String
		// .valueOf(mylib.getId()) });
		db.close();
	}

}
