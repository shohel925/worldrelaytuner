package com.aapbd.worldrelaytune;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AllViewImage extends PagerAdapter {

	private LayoutInflater inflater;
	public Context con;
	int[] imageRSC;
	String[] titleArray, desArray;

	// constructor
	public AllViewImage(Context con, int[] imageRSC, String[] titleArray,
			String[] desArray) {
		this.con = con;
		this.imageRSC = imageRSC;
		this.titleArray = titleArray;
		this.desArray = desArray;
	}

	@Override
	public int getCount() {
		return this.imageRSC.length;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == (RelativeLayout) object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView detailsImageView;
		TextView slideTitle, slideDes;

		// AppConstant.fitnesspost= position;

		inflater = (LayoutInflater) con
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View viewLayout = inflater.inflate(R.layout.pagerlayout,
				container, false);

		detailsImageView = (ImageView) viewLayout
				.findViewById(R.id.detailsImageView);

		slideTitle = (TextView) viewLayout.findViewById(R.id.slideTitle);
		slideDes = (TextView) viewLayout.findViewById(R.id.slideDes);

		Typeface tf = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Bold.ttf");
		Typeface tf2 = Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Light.ttf");

		slideTitle.setTypeface(tf);
		slideDes.setTypeface(tf2);

		try {
			detailsImageView.setImageResource(imageRSC[position]);
			slideTitle.setText(titleArray[position]);
			slideDes.setText(desArray[position]);
			// Picasso.with(con)
			// .load(imageRSC[position])
			// .error(R.drawable.ic_launcher).into(detailsImageView);
		} catch (final Exception e) {
			e.printStackTrace();
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}