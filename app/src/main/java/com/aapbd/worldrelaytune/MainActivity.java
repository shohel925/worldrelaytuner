package com.aapbd.worldrelaytune;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.MediaRouteButton;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.AllChannelInfo;
import com.aapbd.worldrelaytune.model.AllFavoritesInfo;
import com.aapbd.worldrelaytune.model.ChannelInfo;
import com.aapbd.worldrelaytune.model.PublicChannelInfo;
import com.aapbd.worldrelaytune.radio.StaticRadioArray;
import com.aapbd.worldrelaytune.response.AllChannelResponse;
import com.aapbd.worldrelaytune.response.AllFavoritesResponse;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.CastPreference;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.KeyHashManager;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.aapbd.worldrelaytune.utils.Utils;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.libraries.cast.companionlibrary.cast.VideoCastManager;
import com.google.android.libraries.cast.companionlibrary.cast.callbacks.VideoCastConsumer;
import com.google.android.libraries.cast.companionlibrary.cast.callbacks.VideoCastConsumerImpl;
import com.google.android.libraries.cast.companionlibrary.widgets.MiniController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
	Context con;
	TextView welcomeTv, name, userEmail, accountTv, createChannel, aboutTv,
			logoutTv, allChannel;
	private ViewFlipper favoritesFlipper;
	Activity act;
	GridAdapter gGridAdapter;
	Dialog dd;
	Typeface tf3;
	EditText et_search;
	JSONArray jArray;
	AllFavoritesResponse allFavoritesResponse;
	BusyDialog busyNow;
	AllChannelResponse allChannelResponse;
	BusyDialog busy;
	String result, public_result, channel_result;
	TextView searchByChannel;
	ImageView favoritesImg, categoryImg, discoverImg;
	TextView favoritesTv, categoryTv, discoverTv;
	LinearLayout favoritesLay, categoriesLay, discoverLay;
	TextView mainTitile;
	List<PublicChannelInfo> allPublicChannelList;
	DatabaseHandler db;
	boolean flag = false;
	boolean audioFlag=false;
	public static MainActivity instance;

	public static MainActivity getInstance() {
		return instance;
	}

	// ======== for ChormeCast =================
	MediaRouteButton mMediaRouteButton;
	private VideoCastManager mCastManager;
	private VideoCastConsumer mCastConsumer;
	private MiniController mMini;
	private boolean mIsHoneyCombOrAbove = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	View googleCastView;
	ListView fevoritList;
	private VideoCastManager mCastManager2;
	private VideoCastConsumerImpl mCastConsumer2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		VideoCastManager.checkGooglePlayServices(this);
		setContentView(R.layout.favorites);
		con = this;
		act = this;
		instance = this;
		AnalyticsTracker.sendTrackData(this, "Favorites", "");
		AnalyticsTracker.sendEventData(this, "Favorites", "action", "label");
		db = new DatabaseHandler(con);

		favoritesFlipper = (ViewFlipper) findViewById(R.id.favoritesFlipper);
		favoritesFlipper.setDisplayedChild(0);
		initUI();

	}

	private void initUI() {
		KeyHashManager.getKeyHash(con);
		fevoritList=(ListView)findViewById(R.id.fevoritList);
		googleCastView = (View)findViewById(R.id.googleCastView);
		mainTitile = (TextView) findViewById(R.id.mainTitile);
		favoritesLay = (LinearLayout) findViewById(R.id.favoritesLay);
		categoriesLay = (LinearLayout) findViewById(R.id.categoriesLay);
		discoverLay = (LinearLayout) findViewById(R.id.discoverLay);
		favoritesTv = (TextView) findViewById(R.id.favoritesTv);
		categoryTv = (TextView) findViewById(R.id.categoryTv);
		discoverTv = (TextView) findViewById(R.id.discoverTv);

		favoritesImg = (ImageView) findViewById(R.id.favoritesImg);
		categoryImg = (ImageView) findViewById(R.id.categoryImg);
		discoverImg = (ImageView) findViewById(R.id.discoverImg);

		mCastManager = VideoCastManager.getInstance();

		// -- Adding MiniController
		mMini = (MiniController) findViewById(R.id.miniController);
		mCastManager.addMiniController(mMini);

		mCastConsumer = new VideoCastConsumerImpl() {

			@Override
			public void onFailed(int resourceId, int statusCode) {
				String reason = "Not Available";
				if (resourceId > 0) {
					reason = getString(resourceId);
				}
				Log.e("Status", "Action failed, reason:  " + reason
						+ ", status code: " + statusCode);
			}

			@Override
			public void onApplicationConnected(ApplicationMetadata appMetadata,
					String sessionId, boolean wasLaunched) {
				invalidateOptionsMenu();
			}

			@Override
			public void onDisconnected() {
				invalidateOptionsMenu();
			}

			@Override
			public void onConnectionSuspended(int cause) {
				Log.d("Status",
						"onConnectionSuspended() was called with cause: "
								+ cause);
				Utils.showToast(con, R.string.connection_temp_lost);
			}

			@Override
			public void onConnectivityRecovered() {
				Utils.showToast(con, R.string.connection_recovered);
			}

			@Override
			public void onCastDeviceDetected(final RouteInfo info) {
				if (!CastPreference.isFtuShown(con) && mIsHoneyCombOrAbove) {
					CastPreference.setFtuShown(con);

					Log.d("Status", "Route is visible: " + info);
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {

							Log.e("Status", "Cast Icon is visible: " + info.getName());
							showFtu();

						}
					}, 1000);
				}
			}
		};

		mCastManager.reconnectSessionIfPossible();

		favoritesLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				favoritesFlipper.setDisplayedChild(0);
				mainTitile.setText("Favorites");
				changeBg("Favorites");
				getFavoritesInfo(AllURL.getFavoritesUrl(PersistData
						.getStringData(con, AppConstant.urerId)));
			}
		});
		categoriesLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				new CategoryFlipper(favoritesFlipper, con, act);
				changeBg("Categories");
			}
		});

		discoverLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DiscoverFlipper(favoritesFlipper, con, act);
				changeBg("Discover");
			}
		});

		final String time = "" + System.currentTimeMillis();

		if (!PersistData.getStringData(con, AppConstant.firstHead)
				.equalsIgnoreCase("Yes")) {
			getPublicData(AllURL.getAllPublicChannelUrl());
			getAllChannelData(AllURL.getAllChannelUrl(PersistData
					.getStringData(con, AppConstant.urerId)));
			PersistData.setStringData(con, AppConstant.previousTime, time);

		} else {
			flag = true;
			final long timecurrent = Long.parseLong(time);
			final long previousTime = Long.parseLong(PersistData.getStringData(
					con, AppConstant.previousTime));

			if (timecurrent - previousTime > 24 * 60 * 60 * 1000) {
				flag = false;
				PersistData.setStringData(con, AppConstant.previousTime, time);

				getPublicData(AllURL.getAllPublicChannelUrl());
				getAllChannelData(AllURL.getAllChannelUrl(PersistData
						.getStringData(con, AppConstant.urerId)));

			}
		}
		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");

		Typeface tf2 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-LightItalic.ttf");

		tf3 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
		mainTitile.setTypeface(tf);
		if (flag) {
			getFavoritesInfo2(AllURL.getFavoritesUrl(PersistData.getStringData(con, AppConstant.urerId)));
		}

		 setChromecast();
	}
	private void setChromecast(){
		mCastManager2 = VideoCastManager.getInstance();
		mCastConsumer2 = new VideoCastConsumerImpl() {
			@Override
			public void onConnected() {
				super.onConnected();
			}

			@Override
			public void onDisconnected() {
				super.onDisconnected();
			}
		};
		mCastManager2.addVideoCastConsumer(mCastConsumer2);
	}


	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void showFtu() {
		if (googleCastView != null && googleCastView instanceof MediaRouteButton) {
			 new ShowcaseView.Builder(this)
			 .setTarget(new ViewTarget(googleCastView))
			 .setContentTitle(R.string.touch_to_cast)
			 .build();
		}
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (mCastManager.onDispatchVolumeKeyEvent(event,
				MyApplication.VOLUME_INCREMENT)) {
			return true;
		}
		return super.dispatchKeyEvent(event);
	}

	@Override
	protected void onResume() {
		Log.d("Status", "onResume() was called");
		mCastManager = VideoCastManager.getInstance();
		if (null != mCastManager) {
			mCastManager.addVideoCastConsumer(mCastConsumer);
			mCastManager.incrementUiCounter();
		}

		super.onResume();
	}

	@Override
	protected void onPause() {
		mCastManager.decrementUiCounter();
		mCastManager.removeVideoCastConsumer(mCastConsumer);
		mCastManager2.removeVideoCastConsumer(mCastConsumer2);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Log.d("Status", "onDestroy is called");
		if (null != mCastManager) {
			mMini.removeOnMiniControllerChangedListener(mCastManager);
			mCastManager.removeMiniController(mMini);
		}
		super.onDestroy();
	}

	public void settingImg(View v) {
		StartActivity.toActivity(con, SettingsActivity.class);

	}

	public void searchImg(View v) {
		searchDialoge();
	}

	public void searchDialoge() {
		dd = new Dialog(con);
		dd.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd.setContentView(R.layout.search_dialoge_cancel_search);
		dd.show();

		et_search = (EditText) dd.findViewById(R.id.et_search);
		ViewGroup btn_cancel = (ViewGroup) dd.findViewById(R.id.btn_cancel);
		ViewGroup btn_search = (ViewGroup) dd.findViewById(R.id.btn_search);
		TextView btn_sign = (TextView) dd.findViewById(R.id.btn_sign);
		TextView searchTv = (TextView) dd.findViewById(R.id.searchTv);

		btn_sign.setTypeface(tf3);
		et_search.setTypeface(tf3);
		searchTv.setTypeface(tf3);
		btn_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (AppConstant.searchList != null) {
					AppConstant.searchList.clear();
				}
				String searchTXT = "";
				searchTXT = et_search.getText().toString();
				Log.e("searchTXT", ">>" + searchTXT);
				if (TextUtils.isEmpty(searchTXT)) {
					StartActivity.toActivity(con, SearchResultActivity.class);
					// AlertMessage.showMessage(con,
					// getString(R.string.app_name),
					// "Please enter channel name or number");
					dd.dismiss();
				} else {
					AppConstant.searchList = db.getAllSearchData(searchTXT);
					Log.e("searchVector Size",
							">>" + AppConstant.searchList.size());

					if (AppConstant.searchList.size() > 0) {
						StartActivity.toActivity(con,
								SearchResultActivity.class);
					} else {

						AlertMessage
								.showMessage(con, "Nothing Found",
										"The search did not find any matching channels.");
					}

					dd.dismiss();
				}

			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dd.dismiss();
			}
		});

	}

	private void getPublicData(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					public_result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + public_result);

					Gson g = new Gson();
					Type fooType = new TypeToken<ArrayList<PublicChannelInfo>>() {
					}.getType();
					allPublicChannelList = g.fromJson(
							new String(public_result), fooType);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (allPublicChannelList.size() > 0) {
							PersistData.setStringData(con,
									AppConstant.firstHead, "Yes");

							for (PublicChannelInfo pinfo : allPublicChannelList) {
								if (!(db.ifExist(pinfo.getWrid()))) {
									db.addChannelInfo(pinfo);
								}

							}
							getFavoritesInfo2(AllURL
									.getFavoritesUrl(PersistData.getStringData(
											con, AppConstant.urerId)));

						} else {

							AlertMessage.showMessage(con, "Status",
									"Server Problem Please Retry");

						}

					}

				});
			}
		});

		thread.start();

	}

	private void getAllChannelData(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		// busy = new BusyDialog(con, true,false);
		// busy.show();

		final Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					channel_result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + channel_result);

					Gson g = new Gson();
					allChannelResponse = g.fromJson(new String(channel_result),
							AllChannelResponse.class);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						// if (busy != null) {
						// busy.dismis();
						// }

						if (allChannelResponse.getPosts().size() > 0) {

							for (AllChannelInfo pinfo : allChannelResponse.getPosts()) {
								if (!(db.ifChannelExist(pinfo.getPost().getChannel_number()))) {
									db.addChannelInfo2(pinfo.getPost());
								}

							}
							for (ChannelInfo pinfo : db.getAllChannel()) {
								Log.e("db Name", ">>" + pinfo.getChannel_name());
								Log.e("db Number", ">>" + pinfo.getChannel_number());
							}

						} else {

							AlertMessage.showMessage(con, "Status",
									"Server Problem Please Retry");

						}

					}

				});
			}
		});

		thread.start();

	}

	protected void changeBg(String buttonName) {
		// TODO Auto-generated method stub
		if (buttonName.equalsIgnoreCase("Favorites")) {
			favoritesTv.setTextColor(Color.parseColor("#007AFF"));
			favoritesImg.setImageResource(R.drawable.favorites_03);
			categoryTv.setTextColor(Color.BLACK);
			categoryImg.setImageResource(R.drawable.favoritesaa_03);
			discoverTv.setTextColor(Color.BLACK);
			discoverImg.setImageResource(R.drawable.categorie_07);
			return;

		} else if (buttonName.equalsIgnoreCase("Discover")) {
			favoritesTv.setTextColor(Color.BLACK);
			favoritesImg.setImageResource(R.drawable.categorie_03);
			categoryTv.setTextColor(Color.BLACK);
			categoryImg.setImageResource(R.drawable.favoritesaa_03);
			discoverTv.setTextColor(Color.parseColor("#007AFF"));
			discoverImg.setImageResource(R.drawable.discovera_03);

			return;

		} else if (buttonName.equalsIgnoreCase("Categories")) {
			favoritesTv.setTextColor(Color.BLACK);
			favoritesImg.setImageResource(R.drawable.categorie_03);
			categoryTv.setTextColor(Color.parseColor("#007AFF"));
			categoryImg.setImageResource(R.drawable.categorie_05);
			discoverTv.setTextColor(Color.BLACK);
			discoverImg.setImageResource(R.drawable.categorie_07);

			return;

		}

	}

	private class GridAdapter extends ArrayAdapter<PublicChannelInfo> {
		Context context;
		List<PublicChannelInfo> temp;

		GridAdapter(Context context) {
			super(context, R.layout.category_item_rows, db.getAllFavorites());

			this.context = context;
			temp = db.getAllFavorites();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.category_item_rows, null);

			}

			if (position < temp.size()) {

				final PublicChannelInfo query = temp.get(position);
				Log.e("ChannelNumber", ">>" + query.getWrid());

				final ImageView cat_item_Img = (ImageView) v.findViewById(R.id.cat_item_Img);
				final TextView catChannelName = (TextView) v.findViewById(R.id.catChannelName);
				final TextView cat_channelNumber = (TextView) v.findViewById(R.id.cat_channelNumber);

				catChannelName.setText(query.getName());
				catChannelName.setTypeface(tf3);
				cat_channelNumber.setText(query.getWrid());
				cat_channelNumber.setTypeface(tf3);
				Picasso.with(con).load(query.getChannel_image_file()).error(R.drawable.ic_launcher).into(cat_item_Img);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Log.e("temp.size", ">>" + temp.size());
					PublicChannelInfo publicChannel = temp.get(position);
//					if (publicChannel.getWrid().equalsIgnoreCase("11776")||publicChannel.getWrid().equalsIgnoreCase("11809")) {
//						AppConstant.channelInfo=publicChannel;
//						StartActivity.toActivity(con,TestActivity.class);
//						return;
//					}
					if (TextUtils.isEmpty(publicChannel.getAudio_location())) {

						if (publicChannel.getStream_type().equalsIgnoreCase("1")) {
							if (publicChannel.getVisual_content_location().startsWith("youtubelive")) {
								AppConstant.videoSource = "youtubelive";
								AppConstant.youtubeVideoUrl = publicChannel.getVisual_content_location().substring(publicChannel.getVisual_content_location().lastIndexOf("+") + 1);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							} else {
								AppConstant.videoSource = "normal";
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);

							}
						} else if (publicChannel.getStream_type().equalsIgnoreCase("2")) {
							if (TextUtils.isEmpty(publicChannel.getStream_vod_url())) {
								AppConstant.videoSource = "youtubelive";
								AppConstant.youtubeVideoUrl = publicChannel.getVisual_content_location().substring(publicChannel.getVisual_content_location().lastIndexOf("+") + 1);
								Log.e("Youtube video Live Name", ">>" + AppConstant.youtubeVideoUrl);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							} else {
								if ((publicChannel.getStream_vod_url().startsWith("http://vimeo")) || (publicChannel.getStream_vod_url().startsWith("https://vimeo"))) {
									AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
									Log.e("Vimeo Name", ">>" + AppConstant.youtubeName);
									AppConstant.videoSource = "vimeo";
									AppConstant.channelInfo = publicChannel;
									AppConstant.channelName = publicChannel.getName();
									StartActivity.toActivity(con, VideoPlayActivity.class);
								} else if (publicChannel.getStream_vod_url().startsWith("youtube")) {
									AppConstant.videoSource = "youtubeUser";
									if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube User", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

									} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										StartActivity.toActivity(con, VideoPlayActivity.class);
									}else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube User", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

									}

								} else if (publicChannel.getStream_vod_url().startsWith("vevo")) {
									// ===== This condition for Vevo ======
									AppConstant.youtubeName = publicChannel.getStream_vod_url();
									Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
									Toast.makeText(con, "Vevo channels are currently not supported on Android", Toast.LENGTH_SHORT).show();
								}
							}

						} else if (publicChannel.getStream_type().equalsIgnoreCase("3")) {
							AppConstant.videoSource = "normalAndDemand";
							if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube User", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

							} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								StartActivity.toActivity(con, VideoPlayActivity.class);
							} else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube Channel", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

							}else {
								AppConstant.ondemendSOurce = "vimeo";
								AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							}

						}

					} else {
						if (publicChannel.getStream_type().equalsIgnoreCase("3")) {
							AppConstant.videoSource = "audio";
							flag=true;
							AppConstant.fromAudio = true;
							StaticRadioArray.urls[0] = publicChannel.getAudio_location();
							AppConstant.channelName = publicChannel.getName();
							AppConstant.channelInfo = publicChannel;
							AppConstant.visual_content_location = publicChannel.getVisual_content_location();
							getMaxImage(publicChannel.getVisual_content_location() + "maxNumOfImages");

							if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube User", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName2(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

							} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								//StartActivity.toActivity(con, VideoPlayActivity.class);
							} else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube Channel", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName2(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

							}else {
								AppConstant.ondemendSOurce = "vimeo";

								AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								//StartActivity.toActivity(con, VideoPlayActivity.class);
							}

						} else {
							AppConstant.videoSource = "audio";
							AppConstant.fromAudio = true;
							StaticRadioArray.urls[0] = publicChannel.getAudio_location();
							AppConstant.channelName = publicChannel.getName();
							AppConstant.channelInfo = publicChannel;
							AppConstant.visual_content_location = publicChannel.getVisual_content_location();
							getMaxImage(publicChannel.getVisual_content_location() + "maxNumOfImages");
						}


					}
				}
			});

			return v;

		}

	}

	private void getChannelName(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				try {
					JSONObject job = new JSONObject(new String(response));
					jArray = job.getJSONArray("items");
					JSONObject jholder = jArray.getJSONObject(0);
					JSONObject jholder2 = jholder
							.getJSONObject("contentDetails");
					JSONObject jholder3 = jholder2
							.getJSONObject("relatedPlaylists");
					AppConstant.youtubeName = jholder3.optString("uploads");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jArray.length() > 0) {

					if(audioFlag){
						audioFlag=false;
					}else{
						StartActivity.toActivity(con, VideoPlayActivity.class);
					}
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getChannelName2(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
								  byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				try {
					JSONObject job = new JSONObject(new String(response));
					jArray = job.getJSONArray("items");
					JSONObject jholder = jArray.getJSONObject(0);
					JSONObject jholder2 = jholder
							.getJSONObject("contentDetails");
					JSONObject jholder3 = jholder2
							.getJSONObject("relatedPlaylists");
					AppConstant.youtubeName = jholder3.optString("uploads");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jArray.length() > 0) {

//					if(audioFlag){
//						audioFlag=false;
//					}else{
//						StartActivity.toActivity(con, VideoPlayActivity.class);
//					}
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
								  byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}
	private void getMaxImage(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));
				String imageCount = new String(response);
				AppConstant.imageSize = Integer.parseInt(imageCount);
				Log.e("Image Size", ">>" + AppConstant.imageSize);
				StartActivity.toActivity(con, VideoPlayActivity.class);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getFavoritesInfo(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("Favorite response", ">>" + result);

					Gson g = new Gson();
					allFavoritesResponse = g.fromJson(new String(result), AllFavoritesResponse.class);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (!(result.toString().trim().equalsIgnoreCase("0"))) {
							Log.e("favorite Size", ">>" + allFavoritesResponse.getPosts().size());
							for (AllFavoritesInfo fInfo : allFavoritesResponse.getPosts()) {
								db.updateFavorites(fInfo.getPost().getChannel_id());
							}
							gGridAdapter = new GridAdapter(con);
							fevoritList.setAdapter(gGridAdapter);
							gGridAdapter.notifyDataSetChanged();

						} else {
							AlertMessage.showMessage(con, "Information",
									"Favorites List is Empty");

						}

					}

				});
			}
		});

		thread.start();

	}

	private void getFavoritesInfo2(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("Favorite response", ">>" + result);

					Gson g = new Gson();
					allFavoritesResponse = g.fromJson(new String(result),
							AllFavoritesResponse.class);

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (!(result.toString().trim().equalsIgnoreCase("0"))) {
							Log.e("favorite Size", ">>"
									+ allFavoritesResponse.getPosts().size());
							for (AllFavoritesInfo fInfo : allFavoritesResponse
									.getPosts()) {
								db.updateFavorites(fInfo.getPost()
										.getChannel_id());
							}
							gGridAdapter = new GridAdapter(con);
							fevoritList.setAdapter(gGridAdapter);
							gGridAdapter.notifyDataSetChanged();

						} else {
							new CategoryFlipper(favoritesFlipper, con, act);
							changeBg("Categories");

						}

					}

				});
			}
		});

		thread.start();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			exitFromApp();

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	public void exitFromApp() {
		final CharSequence[] items = { "Yes", "No" };
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Exit?");
		builder.setIcon(R.drawable.ic_launcher);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0:
					MainActivity.this.finish();
					System.exit(0);
					System.gc();
					android.os.Process.killProcess(android.os.Process.myPid());
					GoogleAnalytics.getInstance(con).reportActivityStop(MainActivity.this);
					//stopTask();
					return;

				case 1:
					// onStopRecording();

					break;

				default:

					return;
				}

			}
		});
		builder.show();
		builder.create();

	}

	public  void stopTask(){
		List<ApplicationInfo> packages;
		PackageManager pm;
		pm = getPackageManager();
		//get a list of installed apps.
		packages = pm.getInstalledApplications(0);

		ActivityManager mActivityManager = (ActivityManager)con.getSystemService(Context.ACTIVITY_SERVICE);

		for (ApplicationInfo packageInfo : packages) {
			if((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM)==1)continue;
			if(packageInfo.packageName.equals("com.aapbd.worldrelaytune")) continue;
			mActivityManager.killBackgroundProcesses(packageInfo.packageName);
		}
	}

}
