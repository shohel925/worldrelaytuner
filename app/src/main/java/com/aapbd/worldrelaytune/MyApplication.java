/**
 *
 */
package com.aapbd.worldrelaytune;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.cast.CastStatusCodes;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.libraries.cast.companionlibrary.cast.BaseCastManager;
import com.google.android.libraries.cast.companionlibrary.cast.VideoCastManager;
import com.google.android.libraries.cast.companionlibrary.cast.callbacks.VideoCastConsumerImpl;
import com.google.android.libraries.cast.companionlibrary.cast.exceptions.NoConnectionException;
import com.google.android.libraries.cast.companionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.android.libraries.cast.companionlibrary.cast.player.VideoCastControllerActivity;

import java.util.HashMap;

/**
 * @author Ashraful
 *
 */
public class MyApplication extends Application {
//==== for client id ================
	public static final String PROPERTY_ID = "UA-62137521-1";
	//public static final String PROPERTY_ID = "UA-63145393-1";
	private static MyApplication mInstance;

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
						// roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
							// company.
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			analytics.setLocalDispatchPeriod(10);
			/*
			 * Tracker t = (trackerId == TrackerName.APP_TRACKER) ?
			 * analytics.newTracker(PROPERTY_ID) : (trackerId ==
			 * TrackerName.GLOBAL_TRACKER) ?
			 * analytics.newTracker(R.xml.global_tracker) :
			 * analytics.newTracker(R.xml.ecommerce_tracker);
			 */

			Tracker t = null;
			if (trackerId == TrackerName.APP_TRACKER) {
				t = analytics.newTracker(PROPERTY_ID);
			}

			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}

	private static final String TAG = "CastApplication";
	private static String APPLICATION_ID;
	public static final double VOLUME_INCREMENT = 0.05;
	public static final int PRELOAD_TIME_S = 20;

	/*
	 * (non-Javadoc)
	 *
	 * @see android.app.Application#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;

		APPLICATION_ID = getString(R.string.app_id);



		// initialize VideoCastManager
		VideoCastManager
				.initialize(this, APPLICATION_ID,
						VideoCastControllerActivity.class, null)
				.setVolumeStep(VOLUME_INCREMENT)
				.enableFeatures(
						BaseCastManager.FEATURE_NOTIFICATION
								| BaseCastManager.FEATURE_LOCKSCREEN
								| BaseCastManager.FEATURE_WIFI_RECONNECT
								| BaseCastManager.FEATURE_CAPTIONS_PREFERENCE
								| BaseCastManager.FEATURE_DEBUGGING);

	}

	public static synchronized MyApplication getInstance() {
		return mInstance;
	}



	/**
	 * Loading queue items. The only reason we are using this instead of using
	 * the VideoCastManager directly is to get around an issue on receiver side
	 * for HLS + VTT for a queue; this will be addressed soon and the following
	 * workaround will be removed.
	 */
	public void loadQueue(MediaQueueItem[] items, int startIndex)
			throws TransientNetworkDisconnectionException,
			NoConnectionException {
		final VideoCastManager castManager = VideoCastManager.getInstance();
		castManager.addVideoCastConsumer(new VideoCastConsumerImpl() {
			@Override
			public void onMediaQueueOperationResult(int operationId,
					int statusCode) {
				if (operationId == VideoCastManager.QUEUE_OPERATION_LOAD) {
					if (statusCode == CastStatusCodes.SUCCESS) {
						castManager.setActiveTrackIds(new long[] {});
					}
					castManager.removeVideoCastConsumer(this);
				}
			}
		});
		castManager.queueLoad(items, startIndex,
				MediaStatus.REPEAT_MODE_REPEAT_OFF, null);
	}

	 @Override
	 protected void attachBaseContext(Context base) {
	 super.attachBaseContext(base);
	 Log.e("Call", "Call");
	 MultiDex.install(this);
	 }
}
