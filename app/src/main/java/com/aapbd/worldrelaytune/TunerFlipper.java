package com.aapbd.worldrelaytune;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.aapbd.worldrelaytune.radio.RadioMainActivity;
import com.aapbd.worldrelaytune.radio.StaticRadioArray;
import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.AllChannelInfo;
import com.aapbd.worldrelaytune.model.ChannelInfo;
import com.aapbd.worldrelaytune.model.PublicChannelInfo;
import com.aapbd.worldrelaytune.response.AllChannelResponse;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

public class TunerFlipper {

	ViewFlipper viewFlipper;
	Context con;
	Activity newActivty;
	ListView tunerList;

	CustomAdapter customAdapter;
	LinearLayout tunerBtn, tunerSearchLayout, tunerDownBtn;
	Typeface tf3;
	TextView tunerName, tunerName2, channelSearchNane;
	AllChannelResponse allChannelResponse;
	List<AllChannelInfo> allList;
	List<ChannelInfo> tempList;
	DatabaseHandler db;
	AutoCompleteTextView autoTxt;
	List<PublicChannelInfo> allPublicChannelList;
	JSONArray jArray;
	List<String> channelNumber = new ArrayList<String>();

	public TunerFlipper(ViewFlipper viewFlipper, Context con,
			Activity newActivty) {

		this.viewFlipper = viewFlipper;
		this.con = con;
		this.newActivty = newActivty;

		viewFlipper.setDisplayedChild(2);
		db = new DatabaseHandler(con);
		initUi();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		autoTxt = (AutoCompleteTextView) newActivty.findViewById(R.id.autoTxt);
		channelSearchNane = (TextView) newActivty
				.findViewById(R.id.channelSearchNane);
		tunerName2 = (TextView) newActivty.findViewById(R.id.tunerName2);
		tunerName = (TextView) newActivty.findViewById(R.id.tunerName);
		tunerList = (ListView) newActivty.findViewById(R.id.tunerList);
		tunerBtn = (LinearLayout) newActivty.findViewById(R.id.tunerBtns);
		tunerDownBtn = (LinearLayout) newActivty
				.findViewById(R.id.tunerDownBtn);
		tunerSearchLayout = (LinearLayout) newActivty
				.findViewById(R.id.tunerSearchLayout);
		tf3 = Typeface.createFromAsset(con.getAssets(),
				"fonts/OpenSans-Light.ttf");
		channelSearchNane.setTypeface(tf3);
		Typeface tf = Typeface.createFromAsset(con.getAssets(),
				"fonts/OpenSans-Bold.ttf");
		tunerName2.setTypeface(tf);
		tunerName.setTypeface(tf);
		if (AppConstant.searchChannelPress) {
			tunerBtn.setVisibility(View.GONE);
			tunerSearchLayout.setVisibility(View.VISIBLE);
			AppConstant.searchChannelPress = false;
		}

		getChannelInfo(AllURL.getAllPublicChannelUrl());

		tunerBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tunerBtn.setVisibility(View.GONE);
				tunerSearchLayout.setVisibility(View.VISIBLE);
			}
		});

		tunerDownBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tunerBtn.setVisibility(View.VISIBLE);
				tunerSearchLayout.setVisibility(View.GONE);
			}
		});

		autoTxt.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.e("position", ">> " + position);
				// Toast.makeText(MainActivity.this,
				// adapter.getItem(position).toString(),
				// Toast.LENGTH_SHORT).show();
				final PublicChannelInfo query = allPublicChannelList
						.get(position);
				if (TextUtils.isEmpty(query.getAudio_location())) {

					if (query.getStream_type().equalsIgnoreCase("1")) {
						if (query.getVisual_content_location().startsWith(
								"youtubelive")) {
							AppConstant.youtubeVideoUrl = query
									.getVisual_content_location().substring(
											query.getVisual_content_location()
													.lastIndexOf("+") + 1);
							AppConstant.channelName = query.getName();
							AppConstant.videoDescription = query
									.getChannel_info();
							StartActivity.toActivity(con,
									YoutubePlayActivity.class);
							AppConstant.channelInfo = null;
						} else {
							AppConstant.youtubeName = query
									.getVisual_content_location();
							AppConstant.channelName = query.getName();
							AppConstant.videoDescription = query
									.getChannel_info();
							Log.e("Normal Video Name", ">>"
									+ AppConstant.youtubeName);
							AppConstant.channelInfo = query;
							StartActivity.toActivity(con,
									VideoPlayActivity.class);

						}
					} else if (query.getStream_type().equalsIgnoreCase("2")) {
						if (TextUtils.isEmpty(query.getStream_vod_url())) {
							AppConstant.youtubeName = query
									.getVisual_content_location().substring(
											query.getVisual_content_location()
													.lastIndexOf("+") + 1);
							Log.e("Youtube video Live Name", ">>"
									+ AppConstant.youtubeName);
							AppConstant.videoDescription = query
									.getChannel_info();
						} else {
							if ((query.getStream_vod_url()
									.startsWith("http://vimeo"))
									|| (query.getStream_vod_url()
											.startsWith("https://vimeo"))) {
								AppConstant.youtubeName = query
										.getStream_vod_url().substring(
												query.getStream_vod_url()
														.lastIndexOf("/") + 1);
								Log.e("Vimeo Name", ">>"
										+ AppConstant.youtubeName);
								AppConstant.channelInfo = null;
								StartActivity.toActivity(con,
										VimeoPlayListActivity.class);
							} else if (query.getStream_vod_url().startsWith(
									"youtube")) {

								if (query.getStream_vod_url().startsWith(
										"youtubeuser")) {
									AppConstant.youtubeName = query
											.getStream_vod_url()
											.substring(
													query.getStream_vod_url()
															.lastIndexOf("+") + 1);
									Log.e("Youtube User", ">>"
											+ AppConstant.youtubeName);
									AppConstant.channelName = query.getName();
									AppConstant.channelInfo = null;
									getChannelName(AllURL
											.getAllYoutubeChannelName(AppConstant.youtubeName));

								} else if (query.getStream_vod_url()
										.startsWith("youtubeplaylist")) {
									AppConstant.youtubeName = query
											.getStream_vod_url()
											.substring(
													query.getStream_vod_url()
															.lastIndexOf("+") + 1);
									Log.e("Youtube PlayList", ">>"
											+ AppConstant.youtubeName);
									AppConstant.channelName = query.getName();
									AppConstant.channelInfo = null;
									StartActivity.toActivity(con,
											YoutubePlayListActivity.class);
								}

							} else if (query.getStream_vod_url().startsWith(
									"vevo")) {
								// ===== This condition for Vevo ======
								AppConstant.youtubeName = query
										.getStream_vod_url();
								Log.e("Youtube PlayList", ">>"
										+ AppConstant.youtubeName);
								Toast.makeText(con, "Working letter", 3000)
										.show();
							}
						}

					} else if (query.getStream_type().equalsIgnoreCase("3")) {
						// Normal M3u3 video in video view
						AppConstant.youtubeName = query
								.getVisual_content_location();
						AppConstant.channelName = query.getName();
						AppConstant.videoDescription = query.getChannel_info();
						Log.e("Normal Video Name", ">>"
								+ AppConstant.youtubeName);
						AppConstant.channelInfo = query;
						StartActivity.toActivity(con, VideoPlayActivity.class);
					}

				} else {
					AppConstant.channelInfo = null;
					StaticRadioArray.urls[0] = query.getAudio_location();
					AppConstant.channelName = query.getName();
					AppConstant.visual_content_location = query
							.getVisual_content_location();
					getMaxImage(query.getVisual_content_location()
							+ "maxNumOfImages");

				}
			}
		});
	}

	private void getChannelInfo(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));
				Gson g = new Gson();

				Type fooType = new TypeToken<ArrayList<PublicChannelInfo>>() {
				}.getType();
				allPublicChannelList = g
						.fromJson(new String(response), fooType);

				if (allPublicChannelList.size() > 0) {
					if (channelNumber != null) {
						channelNumber.clear();
					}

					for (PublicChannelInfo pinfo : allPublicChannelList) {
						channelNumber.add(pinfo.getWrid());
						Log.e("Channel Number", ">>" + channelNumber);
					}

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							con, android.R.layout.simple_list_item_1,
							channelNumber);
					autoTxt.setAdapter(adapter);

					customAdapter = new CustomAdapter(con);
					tunerList.setAdapter(customAdapter);
					customAdapter.notifyDataSetChanged();
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private class CustomAdapter extends ArrayAdapter<PublicChannelInfo> {
		Context context;

		CustomAdapter(Context context) {
			super(context, R.layout.tuner_row, allPublicChannelList);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.tuner_row, null);

			}

			if (position < allPublicChannelList.size()) {

				final PublicChannelInfo query = allPublicChannelList
						.get(position);

				final ImageView channalImg = (ImageView) v
						.findViewById(R.id.channalImg);
				final TextView chanalNumber = (TextView) v
						.findViewById(R.id.chanalNumber);
				final TextView chanalName = (TextView) v
						.findViewById(R.id.chanalName);
				chanalName.setSelected(true);
				chanalName.setText(Html.fromHtml(query.getName()));
				chanalNumber.setText(query.getWrid());
				chanalNumber.setTypeface(tf3);
				chanalName.setTypeface(tf3);

				Picasso.with(con).load(query.getChannel_image_file())
						.error(R.drawable.ic_launcher).into(channalImg);

			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					final PublicChannelInfo query = allPublicChannelList
							.get(position);
					if (TextUtils.isEmpty(query.getAudio_location())) {

						if (query.getStream_type().equalsIgnoreCase("1")) {
							if (query.getVisual_content_location().startsWith(
									"youtubelive")) {
								AppConstant.youtubeVideoUrl = query
										.getVisual_content_location()
										.substring(
												query.getVisual_content_location()
														.lastIndexOf("+") + 1);
								AppConstant.channelName = query.getName();
								AppConstant.videoDescription = query
										.getChannel_info();
								StartActivity.toActivity(con,
										YoutubePlayActivity.class);
							} else {
								AppConstant.youtubeName = query
										.getVisual_content_location();
								AppConstant.channelName = query.getName();
								AppConstant.videoDescription = query
										.getChannel_info();
								Log.e("Normal Video Name", ">>"
										+ AppConstant.youtubeName);
								AppConstant.channelInfo = query;
								StartActivity.toActivity(con,
										VideoPlayActivity.class);

							}
						} else if (query.getStream_type().equalsIgnoreCase("2")) {
							if (TextUtils.isEmpty(query.getStream_vod_url())) {
								AppConstant.videoDescription = query
										.getChannel_info();
								AppConstant.youtubeName = query
										.getVisual_content_location()
										.substring(
												query.getVisual_content_location()
														.lastIndexOf("+") + 1);
								Log.e("Youtube video Live Name", ">>"
										+ AppConstant.youtubeName);
							} else {
								if ((query.getStream_vod_url()
										.startsWith("http://vimeo"))
										|| (query.getStream_vod_url()
												.startsWith("https://vimeo"))) {
									AppConstant.youtubeName = query
											.getStream_vod_url()
											.substring(
													query.getStream_vod_url()
															.lastIndexOf("/") + 1);
									Log.e("Vimeo Name", ">>"
											+ AppConstant.youtubeName);
									StartActivity.toActivity(con,
											VimeoPlayListActivity.class);
								} else if (query.getStream_vod_url()
										.startsWith("youtube")) {

									if (query.getStream_vod_url().startsWith(
											"youtubeuser")) {
										AppConstant.youtubeName = query
												.getStream_vod_url()
												.substring(
														query.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube User", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = query
												.getName();
										getChannelName(AllURL
												.getAllYoutubeChannelName(AppConstant.youtubeName));

									} else if (query.getStream_vod_url()
											.startsWith("youtubeplaylist")) {
										AppConstant.youtubeName = query
												.getStream_vod_url()
												.substring(
														query.getStream_vod_url()
																.lastIndexOf(
																		"+") + 1);
										Log.e("Youtube PlayList", ">>"
												+ AppConstant.youtubeName);
										AppConstant.channelName = query
												.getName();
										StartActivity.toActivity(con,
												YoutubePlayListActivity.class);
									}

								} else if (query.getStream_vod_url()
										.startsWith("vevo")) {
									// ===== This condition for Vevo ======
									AppConstant.youtubeName = query
											.getStream_vod_url();
									Log.e("Youtube PlayList", ">>"
											+ AppConstant.youtubeName);
									AppConstant.videoDescription = query
											.getChannel_info();
									Toast.makeText(con, "Working letter", 3000)
											.show();
								}
							}

						} else if (query.getStream_type().equalsIgnoreCase("3")) {
							// Normal M3u3 video in video view
							AppConstant.youtubeName = query
									.getVisual_content_location();
							AppConstant.channelName = query.getName();
							Log.e("Normal Video Name", ">>"
									+ AppConstant.youtubeName);
							AppConstant.channelInfo = query;
							StartActivity.toActivity(con,
									VideoPlayActivity.class);
						}

					} else {
						StaticRadioArray.urls[0] = query.getAudio_location();
						AppConstant.channelName = query.getName();
						AppConstant.visual_content_location = query
								.getVisual_content_location();
						getMaxImage(query.getVisual_content_location()
								+ "maxNumOfImages");

					}

				}
			});

			return v;

		}

	}

	private void getChannelName(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));

				try {
					JSONObject job = new JSONObject(new String(response));
					jArray = job.getJSONArray("items");
					JSONObject jholder = jArray.getJSONObject(0);
					JSONObject jholder2 = jholder
							.getJSONObject("contentDetails");
					JSONObject jholder3 = jholder2
							.getJSONObject("relatedPlaylists");
					AppConstant.youtubeName = jholder3.optString("uploads");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jArray.length() > 0) {

					StartActivity
							.toActivity(con, YoutubePlayListActivity.class);
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getMaxImage(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.Status),
					con.getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));
				String imageCount = new String(response);
				AppConstant.imageSize = Integer.parseInt(imageCount);
				Log.e("Image Size", ">>" + AppConstant.imageSize);
				StartActivity.toActivity(con, RadioMainActivity.class);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}
}
