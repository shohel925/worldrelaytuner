package com.aapbd.worldrelaytune;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.model.PublicChannelInfo;
import com.aapbd.worldrelaytune.radio.StaticRadioArray;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends Activity {

	Context con;

	EditText first_name, last_name, eamil, passworad, confirm_password;

	TextView btn_sin_up;
	String responses = "";
	ListView searchListview;
	GridAdapter gGridAdapter;
	Typeface tf3;
	List<PublicChannelInfo> temp;
	DatabaseHandler db;
	JSONArray jArray;
	public static SearchResultActivity instance;

	public static SearchResultActivity getInstance() {
		return instance;
	}
	TextView searchTitle,searchBackTv;
	boolean flag=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_result);
		instance = this;
		con = this;
		AnalyticsTracker.sendTrackData(this, "Search", "");
		AnalyticsTracker.sendEventData(this, "Search", "action", "label");
//		MyApplication.getInstance().trackEvent("Search", "", "");
		db = new DatabaseHandler(con);
		initUI();

	}

	private void initUI() {
		// TODO Auto-generated method stub
		searchBackTv=(TextView)findViewById(R.id.searchBackTv);
		searchBackTv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		searchTitle=(TextView)findViewById(R.id.searchTitle);
		searchTitle.setText("Search Results");
		searchListview = (ListView) findViewById(R.id.searchListview);
		//actionBarSetup();
		temp = new ArrayList<PublicChannelInfo>();
		temp = AppConstant.searchList;
		tf3 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
		if (AppConstant.searchList.size() > 0) {
			gGridAdapter = new GridAdapter(con);
			searchListview.setAdapter(gGridAdapter);
			gGridAdapter.notifyDataSetChanged();
		}

	}

	private void actionBarSetup() {
		// TODO Auto-generated method stub

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.login_top);

		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
				R.id.signtitle);
		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
				R.id.backImg);
		signtitle.setText("Search Results");

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");

		signtitle.setTypeface(tf);

		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	private class GridAdapter extends ArrayAdapter<PublicChannelInfo> {
		Context context;

		GridAdapter(Context context) {
			super(context, R.layout.category_item_rows, temp);

			this.context = context;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				final LayoutInflater vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.category_item_rows, null);

			}

			if (position < temp.size()) {

				final PublicChannelInfo query = temp.get(position);

				final ImageView cat_item_Img = (ImageView) v.findViewById(R.id.cat_item_Img);
				final TextView catChannelName = (TextView) v.findViewById(R.id.catChannelName);
				final TextView cat_channelNumber = (TextView) v.findViewById(R.id.cat_channelNumber);
				cat_channelNumber.setText(query.getWrid());
				catChannelName.setText(query.getName());

				catChannelName.setTypeface(tf3);
				cat_channelNumber.setTypeface(tf3);
				Picasso.with(con).load(query.getChannel_image_file()).error(R.drawable.ic_launcher).into(cat_item_Img);
			}

			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					PublicChannelInfo publicChannel = temp.get(position);
//					if (publicChannel.getWrid().equalsIgnoreCase("11776")||publicChannel.getWrid().equalsIgnoreCase("11809")) {
//						AppConstant.channelInfo=publicChannel;
//						StartActivity.toActivity(con,TestActivity.class);
//						return;
//					}
					if (TextUtils.isEmpty(publicChannel.getAudio_location())) {

						if (publicChannel.getStream_type().equalsIgnoreCase("1")) {
							if (publicChannel.getVisual_content_location().startsWith("youtubelive")) {
								AppConstant.videoSource = "youtubelive";
								AppConstant.youtubeVideoUrl = publicChannel.getVisual_content_location().substring(publicChannel.getVisual_content_location().lastIndexOf("+") + 1);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							} else {
								AppConstant.videoSource = "normal";
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);

							}
						} else if (publicChannel.getStream_type().equalsIgnoreCase("2")) {
							if (TextUtils.isEmpty(publicChannel.getStream_vod_url())) {
								AppConstant.videoSource = "youtubelive";
								AppConstant.youtubeVideoUrl = publicChannel.getVisual_content_location().substring(publicChannel.getVisual_content_location().lastIndexOf("+") + 1);
								Log.e("Youtube video Live Name", ">>" + AppConstant.youtubeVideoUrl);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							} else {
								if ((publicChannel.getStream_vod_url().startsWith("http://vimeo")) || (publicChannel.getStream_vod_url().startsWith("https://vimeo"))) {
									AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
									Log.e("Vimeo Name", ">>" + AppConstant.youtubeName);
									AppConstant.videoSource = "vimeo";
									AppConstant.channelInfo = publicChannel;
									AppConstant.channelName = publicChannel.getName();
									StartActivity.toActivity(con, VideoPlayActivity.class);
								} else if (publicChannel.getStream_vod_url().startsWith("youtube")) {
									AppConstant.videoSource = "youtubeUser";
									if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube User", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

									} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										StartActivity.toActivity(con, VideoPlayActivity.class);
									}else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
										AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
										Log.e("Youtube User", ">>" + AppConstant.youtubeName);
										AppConstant.channelName = publicChannel.getName();
										AppConstant.channelInfo = publicChannel;
										getChannelName(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

									}

								} else if (publicChannel.getStream_vod_url().startsWith("vevo")) {
									// ===== This condition for Vevo ======
									AppConstant.youtubeName = publicChannel.getStream_vod_url();
									Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
									Toast.makeText(con, "Vevo channels are currently not supported on Android", Toast.LENGTH_SHORT).show();
								}
							}

						} else if (publicChannel.getStream_type().equalsIgnoreCase("3")) {
							AppConstant.videoSource = "normalAndDemand";
							if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube User", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

							} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								StartActivity.toActivity(con, VideoPlayActivity.class);
							}
							else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube Channel", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

							}else {
								AppConstant.ondemendSOurce = "vimeo";
								AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								StartActivity.toActivity(con, VideoPlayActivity.class);
							}

						}

					} else {
						if (publicChannel.getStream_type().equalsIgnoreCase("3")) {
							AppConstant.videoSource = "audio";
							flag=true;
							AppConstant.fromAudio = true;
							StaticRadioArray.urls[0] = publicChannel.getAudio_location();
							AppConstant.channelName = publicChannel.getName();
							AppConstant.channelInfo = publicChannel;
							AppConstant.visual_content_location = publicChannel.getVisual_content_location();
							getMaxImage(publicChannel.getVisual_content_location() + "maxNumOfImages");

							if (publicChannel.getStream_vod_url().startsWith("youtubeuser")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube User", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelName(AppConstant.youtubeName));

							} else if (publicChannel.getStream_vod_url().startsWith("youtubeplaylist")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube PlayList", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								//StartActivity.toActivity(con, VideoPlayActivity.class);
							} else if (publicChannel.getStream_vod_url().startsWith("youtubechannel")) {
								AppConstant.ondemendSOurce = "youtube";
								AppConstant.youtubeName = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("+") + 1);
								Log.e("Youtube Channel", ">>" + AppConstant.youtubeName);
								AppConstant.channelName = publicChannel.getName();
								AppConstant.channelInfo = publicChannel;
								getChannelName(AllURL.getAllYoutubeChannelNameFromUserId(AppConstant.youtubeName));

							}else {
								AppConstant.ondemendSOurce = "vimeo";

								AppConstant.vimeoChannelId = publicChannel.getStream_vod_url().substring(publicChannel.getStream_vod_url().lastIndexOf("/") + 1);
								AppConstant.youtubeName = publicChannel.getVisual_content_location();
								AppConstant.channelName = publicChannel.getName();
								Log.e("Normal Video Name", ">>" + AppConstant.youtubeName);
								AppConstant.channelInfo = publicChannel;
								//StartActivity.toActivity(con, VideoPlayActivity.class);
							}

						} else {
							AppConstant.videoSource = "audio";
							AppConstant.fromAudio = true;
							StaticRadioArray.urls[0] = publicChannel.getAudio_location();
							AppConstant.channelName = publicChannel.getName();
							AppConstant.channelInfo = publicChannel;
							AppConstant.visual_content_location = publicChannel.getVisual_content_location();
							getMaxImage(publicChannel.getVisual_content_location() + "maxNumOfImages");
						}


					}

				}
			});

			return v;

		}

	}

	private void getMaxImage(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", new String(response));
				String imageCount = new String(response);
				AppConstant.imageSize = Integer.parseInt(imageCount);
				Log.e("Image Size", ">>" + AppConstant.imageSize);
				StartActivity.toActivity(con, VideoPlayActivity.class);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	private void getChannelName(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		// Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}
				Log.e("response", ""+new String(response));

				try {
					JSONObject job = new JSONObject(new String(response));
					jArray = job.getJSONArray("items");
					JSONObject jholder = jArray.getJSONObject(0);
					JSONObject jholder2 = jholder.getJSONObject("contentDetails");
					JSONObject jholder3 = jholder2.getJSONObject("relatedPlaylists");
					AppConstant.youtubeName = jholder3.optString("uploads");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jArray.length() > 0) {

					if(flag){
						flag=false;
					}else{
						StartActivity.toActivity(con, VideoPlayActivity.class);
					}
				} else {

					AlertMessage.showMessage(con, "Status",
							"Server Problem Please Retry");

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				if (busyNow != null) {
					busyNow.dismis();
				}

				// Log.e("response",new String(errorResponse));
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

}
