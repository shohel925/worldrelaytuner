package com.aapbd.worldrelaytune;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AnalyticsTracker;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.PersistentUser;
import com.aapbd.worldrelaytune.utils.StartActivity;
import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;

import io.fabric.sdk.android.Fabric;

public class SinUpActivity extends Activity implements OnClickListener {

	Context con;

	EditText first_name, last_name, eamil, passworad, confirm_password;

	TextView btn_sin_up, tv_signUpto;
	String responses = "";
	String email, pass, firstname, lastname;
	BusyDialog busyNow;
	LinearLayout twitter_signUp;
	TwitterLoginButton login_twitter;
	String twitterEmail = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TwitterAuthConfig authConfig = new TwitterAuthConfig(
				SinInActivity.TWITTER_KEY, SinInActivity.TWITTER_SECRET);
		Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
		setContentView(R.layout.sign_up);
		con = this;
		AnalyticsTracker.sendTrackData(this, "Sign Up", "");
		AnalyticsTracker.sendEventData(this, "Sign Up", "action", "label");
		initUi();
	}

	private void initUi() {
		// TODO Auto-generated method stub
		twitter_signUp = (LinearLayout) findViewById(R.id.twitter_signUp);
		first_name = (EditText) findViewById(R.id.first_name);
		last_name = (EditText) findViewById(R.id.last_name);
		eamil = (EditText) findViewById(R.id.eamil);
		passworad = (EditText) findViewById(R.id.passworad);
		confirm_password = (EditText) findViewById(R.id.confirm_password);
		btn_sin_up = (TextView) findViewById(R.id.btn_sin_up);
		tv_signUpto = (TextView) findViewById(R.id.tv_signUpto);
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");
		Typeface tf2 = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");
		setCursorDrawableColor(first_name,R.color.black);
		setCursorDrawableColor(last_name,R.color.black);
		setCursorDrawableColor(eamil,R.color.black);
		setCursorDrawableColor(passworad,R.color.black);
		setCursorDrawableColor(confirm_password,R.color.black);
		first_name.setTypeface(tf2);
		last_name.setTypeface(tf2);
		eamil.setTypeface(tf2);

		passworad.setTypeface(tf2);
		confirm_password.setTypeface(tf2);
		btn_sin_up.setTypeface(tf2);
		tv_signUpto.setTypeface(tf2);
		btn_sin_up.setOnClickListener(this);
	//	actionBarSetup();

		login_twitter = (TwitterLoginButton) findViewById(R.id.login_twitter);
		login_twitter.setCallback(new Callback<TwitterSession>() {
			@Override
			public void success(Result<TwitterSession> result) {
				// Do something with result, which provides a
				// TwitterSession for making API calls

				TwitterSession session = Twitter.getSessionManager()
						.getActiveSession();
				TwitterAuthToken authToken = session.getAuthToken();
				String token = authToken.token;
				String secret = authToken.secret;
				PersistData.setLongData(con, AppConstant.twitterId,
						session.getUserId());

				Log.e("my ID", ">>>  " + session.getUserId());
				Log.e("my ID", ">>>  " + session.getUserName());
				getTwitterData(session);
			}

			@Override
			public void failure(TwitterException arg0) {
				// TODO Auto-generated method stub

			}
		});
		twitter_signUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				login_twitter.performClick();
			}
		});
		eamil.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

				// you can call or do what you want with your EditText here

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {

				Log.e("onTextChange", ">>" + s);
				boolean isSpace = s.toString().contains(" ");
				if (isSpace) {
					isSpace = false;
					s = s.toString().trim();
					Toast.makeText(con,
							"Space is not allowed at E-Mail Address",
							Toast.LENGTH_SHORT).show();
					// email_sign_in.setText(s.toString().trim());
				}
			}
		});

		passworad.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

				// you can call or do what you want with your EditText here

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				Log.e("onTextChange", ">>" + s);
				boolean isSpace = s.toString().contains(" ");
				if (isSpace) {
					isSpace = false;
					s = s.toString().trim();
					Toast.makeText(con, "Space is not allowed at passcode",
							Toast.LENGTH_SHORT).show();
					// email_sign_in.setText(s.toString().trim());
				}
			}
		});
		confirm_password.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

				// you can call or do what you want with your EditText here

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				Log.e("onTextChange", ">>" + s);
				boolean isSpace = s.toString().contains(" ");
				if (isSpace) {
					isSpace = false;
					s = s.toString().trim();
					Toast.makeText(con,
							"Space is not allowed at confirm passcode",
							Toast.LENGTH_SHORT).show();
					// email_sign_in.setText(s.toString().trim());
				}
			}
		});

		TextView backImg =(TextView)findViewById(R.id.backImg);
		backImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	public static void setCursorDrawableColor(EditText editText, int color) {
		try {
			Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
			fCursorDrawableRes.setAccessible(true);
			int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);
			Field fEditor = TextView.class.getDeclaredField("mEditor");
			fEditor.setAccessible(true);
			Object editor = fEditor.get(editText);
			Class<?> clazz = editor.getClass();
			Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
			fCursorDrawable.setAccessible(true);
			Drawable[] drawables = new Drawable[2];
			drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
			drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
			drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_IN);
			drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_IN);
			fCursorDrawable.set(editor, drawables);
		} catch (final Throwable ignored) {
		}
	}

	public void getTwitterData(final TwitterSession session) {
		MyTwitterApiClient tapiclient = new MyTwitterApiClient(session);
		tapiclient.getCustomService().show(session.getUserId(),
				new Callback<User>() {
					@Override
					public void success(Result<User> result) {

						TwitterAuthToken authToken = session.getAuthToken();
						String token = authToken.token;
						String secret = authToken.secret;

						Log.e("Name", result.data.name);
						// Log.e("Email", result.data.email);
						// Log.e("profileImg", result.data.profileImageUrl);
						twitterEmail = session.getUserId() + "@twitter.com";
						//
						String twitterName = result.data.name;
						String[] names = twitterName.split(" ");
						System.out.println("" + names[0] + " "
								+ names[names.length - 1]);
						PersistData.setStringData(con,
								AppConstant.twitterUserFirstName, names[0]);
						PersistData.setStringData(con,
								AppConstant.twitterUserLastName,
								names[names.length - 1]);
						getTwitterSignIn(AllURL.getLoginUrl(twitterEmail,
								"555555"));
					}

					@Override
					public void failure(TwitterException exception) {
						// TODO Auto-generated method stub
						exception.printStackTrace();
					}

				});
	}

	public void backBtn(View v) {
		finish();

	}

	public void twitterSubmit(View v) {

	}

	private void getTwitterSignIn(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					JSONObject job;

					try {
						job = new JSONObject(new String(result));
						responses = job.optString("response");
						if (responses.equalsIgnoreCase("1")) {
							PersistData.setStringData(con, AppConstant.urerId,
									job.optString("userId"));
							PersistData.setStringData(con,
									AppConstant.user_name,
									job.optString("user_name"));
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (responses.equalsIgnoreCase("1")) {
							PersistentUser.setLogin(con);
							Toast toast=Toast.makeText(con, "Welcome to WorldRelay, " + PersistData.getStringData(con,
									AppConstant.user_name) + "!", Toast.LENGTH_LONG);

							LinearLayout toastLayout = (LinearLayout) toast.getView();
							TextView toastTV = (TextView) toastLayout.getChildAt(0);
							toastTV.setTextSize(18);
							toast.show();
							if (BeforloginActivity.getInstance() != null) {
								BeforloginActivity.getInstance().finish();
							}
							StartActivity.toActivity(con, MainActivity.class);
							finish();
						} else if (responses.equalsIgnoreCase("2")) {
							getTwitterRegister(AllURL.getRegisterUrl(
									twitterEmail, "555555",
									PersistData.getStringData(con,
											AppConstant.twitterUserFirstName),
									AppConstant.twitterUserLastName));
							return;
						} else if (responses.equalsIgnoreCase("3")) {
							AlertMessage.showMessage(con, "Sign up Error",
									"Incorrect Passcode.");

						} else {
							AlertMessage
									.showMessage(con, "Sign up Error",
											"Incorrect E-Mail and Passcode combination.");

						}

					}

				});
			}
		});

		thread.start();

	}

	private void getTwitterRegister(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					JSONObject job;

					try {
						job = new JSONObject(new String(result));
						responses = job.optString("response");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (responses.equalsIgnoreCase("OK")) {

							getTwitterSignIn(AllURL.getLoginUrl(twitterEmail,
									"555555"));
						} else {

							AlertMessage.showMessage(con, "Sign up Error",
									responses);
							return;
						}

					}

				});
			}
		});

		thread.start();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		login_twitter.onActivityResult(requestCode, resultCode, data);

	}

	private void actionBarSetup() {
		// TODO Auto-generated method stub

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.login_top);

		TextView signtitle = (TextView) actionBar.getCustomView().findViewById(
				R.id.signtitle);
		TextView backImg = (TextView) actionBar.getCustomView().findViewById(
				R.id.backImg);
		// signtitle.setText("Sign Up");
		signtitle.setVisibility(View.GONE);
		// Typeface tf = Typeface.createFromAsset(getAssets(),
		// "fonts/OpenSans-Bold.ttf");
		//
		// signtitle.setTypeface(tf);
		//
		backImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	public void checkInformation() {
		if (TextUtils.isEmpty(first_name.getText().toString())) {
			AlertMessage.showMessage(con, getString(R.string.singUpError),
					getString(R.string.PleaseEnterUserFirstName));
			return;
		} else if (TextUtils.isEmpty(last_name.getText().toString())) {
			AlertMessage.showMessage(con, getString(R.string.singUpError),
					getString(R.string.PleaseEnterLastname));
			return;
		} else if (TextUtils.isEmpty(eamil.getText().toString())) {
			AlertMessage.showMessage(con, getString(R.string.singUpError),
					getString(R.string.PleaseEnterEmail));
			return;
		} else if (TextUtils.isEmpty(passworad.getText().toString())) {
			AlertMessage.showMessage(con, getString(R.string.singUpError),
					getString(R.string.PleaseEnterPassword));
			return;
		} else if (TextUtils.isEmpty(confirm_password.getText().toString())) {
			AlertMessage.showMessage(con, getString(R.string.singUpError),
					getString(R.string.PleaseEnterconfrimPassword));
			return;
		} else if (passworad.getText().toString().length() != 6) {
			AlertMessage.showMessage(con, getString(R.string.singUpError),
					getString(R.string.PasswordMustBeCharacter));
			return;
		} else if (!(passworad.getText().toString().equals(confirm_password
				.getText().toString()))) {
			AlertMessage.showMessage(con, getString(R.string.singUpError),
					getString(R.string.PasswordAndConfirmPassword));
			return;
		} else {
			email = eamil.getText().toString();
			firstname = first_name.getText().toString();
			lastname = last_name.getText().toString();
			pass = passworad.getText().toString();
			getRegisterInfo(AllURL.getRegisterUrl(email, pass, firstname,
					lastname));
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btn_sin_up) {
			checkInformation();

		}

	}

	private void getRegisterInfo(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					JSONObject job;

					try {
						job = new JSONObject(new String(result));
						responses = job.optString("response");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (responses.equalsIgnoreCase("OK")) {
							// Toast.makeText(con, "SignUp Succsessful",
							// Toast.LENGTH_LONG)
							// .show();

							getSignInInfo(AllURL.getLoginUrl(email, pass));
						} else {

							AlertMessage.showMessage(con,
									getString(R.string.singUpError), responses);
							return;
						}

					}

				});
			}
		});

		thread.start();

	}

	private void getSignInInfo(final String url) {
		// TODO Auto-generated method stub

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.app_name),
					getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					final String result = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("result", ">>" + result);

					JSONObject job;

					try {
						job = new JSONObject(new String(result));
						responses = job.optString("response");
						if (responses.equalsIgnoreCase("1")) {
							PersistData.setStringData(con, AppConstant.urerId,
									job.optString("userId"));
							PersistData.setStringData(con,
									AppConstant.user_name,
									job.optString("user_name"));
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (responses.equalsIgnoreCase("1")) {
							PersistentUser.setLogin(con);
							Toast toast=Toast.makeText(con, "Welcome to WorldRelay, " + PersistData.getStringData(con,
									AppConstant.user_name) + "!", Toast.LENGTH_LONG);

							LinearLayout toastLayout = (LinearLayout) toast.getView();
							TextView toastTV = (TextView) toastLayout.getChildAt(0);
							toastTV.setTextSize(18);
							toast.show();
							if (BeforloginActivity.getInstance() != null) {
								BeforloginActivity.getInstance().finish();
							}
							StartActivity.toActivity(con, MainActivity.class);
							finish();
						} else if (responses.equalsIgnoreCase("2")) {
							AlertMessage
									.showMessage(con,
											getString(R.string.singUpError),
											"Incorrect E-Mail and Passcode combination.");
							return;
						} else if (responses.equalsIgnoreCase("3")) {
							AlertMessage.showMessage(con,
									getString(R.string.singUpError),
									"Incorrect Passcode.");

						} else {
							AlertMessage
									.showMessage(con,
											getString(R.string.singUpError),
											"Incorrect E-Mail and Passcode combination.");

						}

					}

				});
			}
		});

		thread.start();

	}

}
