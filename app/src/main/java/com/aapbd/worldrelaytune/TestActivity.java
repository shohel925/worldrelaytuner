package com.aapbd.worldrelaytune;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.aapbd.worldrelaytune.db.DatabaseHandler;
import com.aapbd.worldrelaytune.utils.AlertMessage;
import com.aapbd.worldrelaytune.utils.AllURL;
import com.aapbd.worldrelaytune.utils.AppConstant;
import com.aapbd.worldrelaytune.utils.BusyDialog;
import com.aapbd.worldrelaytune.utils.HTTPHandler;
import com.aapbd.worldrelaytune.utils.NetInfo;
import com.aapbd.worldrelaytune.utils.PersistData;
import com.aapbd.worldrelaytune.utils.ShareUtils;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphObject;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class TestActivity extends Activity implements OnClickListener {
	Context con;

	LinearLayout video_parent;
	VideoView video;
	TextView backImg,channelName,channelId,description;
	ImageView videoFavorites,videoShare_img;
	DatabaseHandler db;
	BusyDialog busy;
	Dialog dialog;
	String  shareText="";
	private boolean isShare = false;
	TwitterLoginButton twitter_button;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.video_play2);
		con = this;
		db=new DatabaseHandler(con);
		initUi();

	}

	private void initUi() {
		// TODO Auto-generated method stub
		videoFavorites=(ImageView)findViewById(R.id.videoFavorites);
		videoShare_img=(ImageView)findViewById(R.id.videoShare_img);
		description=(TextView)findViewById(R.id.description);
		video_parent = (LinearLayout)findViewById(R.id.video_parent);
		description.setText(Html.fromHtml(AppConstant.channelInfo.getChannel_info()));
		backImg=(TextView)findViewById(R.id.backImg);
		channelName = (TextView) findViewById(R.id.channelName);
		channelId=(TextView)findViewById(R.id.channelId);
		channelId.setText(AppConstant.channelInfo.getWrid());
		channelName.setText(AppConstant.channelInfo.getName());
		if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {
			videoFavorites.setImageResource(R.drawable.star_fill);
		} else {
			videoFavorites.setImageResource(R.drawable.star_fillas);
		}
		shareText ="I am watching channel number "+AppConstant.channelInfo.getWrid()+" "+AppConstant.channelInfo.getName()+" on the WorldRelay Tuner. Check it out! https://www.worldrelay.tv/tuner "+" http://player.vimeo.com/external/129006668.sd.mp4?s=ea813e9f34f567ada996179167073f7c&profile_id=112";
		videoShare_img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog = new Dialog(con);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialogue_share);
				LinearLayout faceBook = (LinearLayout) dialog.findViewById(R.id.faceBook);
				LinearLayout mail = (LinearLayout) dialog.findViewById(R.id.mail);
				LinearLayout twitter = (LinearLayout) dialog.findViewById(R.id.twitter);
				LinearLayout message = (LinearLayout) dialog.findViewById(R.id.message);
				TextView cancelShare = (TextView) dialog.findViewById(R.id.cancelShare);
				cancelShare.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();

					}
				});

				faceBook.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fbDialoge("Facebook", shareText);
						dialog.dismiss();
					}
				});

				mail.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						ShareUtils.byMail(con, "", "", shareText);
					}
				});

				twitter.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						fbDialoge("Twitter", shareText);
						dialog.dismiss();
					}
				});

				message.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						ShareUtils.shareBySMS(con, shareText);
					}
				});

				dialog.show();

			}

		});

		videoFavorites.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (db.ifFavorites(AppConstant.channelInfo.getWrid())) {

					removeFavoritesInfo(AllURL.removeFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				} else {
					addFavoritesInfo(AllURL.addFavoritesUrl(
							PersistData.getStringData(con, AppConstant.urerId),
							AppConstant.channelInfo.getWrid()));
				}
			}
		});

		backImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		twitter_button = (TwitterLoginButton) findViewById(R.id.twitter_button);
		twitter_button.setCallback(new Callback<TwitterSession>() {
			@Override
			public void success(Result<TwitterSession> result) {
				// Do something with result, which provides a
				// TwitterSession for making API calls
				AppConstant.videoTwitterButtonClick = true;
				TwitterSession session = Twitter.getSessionManager().getActiveSession();
				TwitterAuthToken authToken = session.getAuthToken();
				String token = authToken.token;
				String secret = authToken.secret;
				PersistData.setLongData(con, AppConstant.twitterId, session.getUserId());

				Log.e("my ID", ">>>  " + session.getUserId());
				Log.e("my ID", ">>>  " + session.getUserName());

			}

			@Override
			public void failure(TwitterException arg0) {
				// TODO Auto-generated method stub

			}
		});
		createVideoView();
	}
Dialog dd2;
	public void fbDialoge(final String shareTitle, final String searchTxt) {
		dd2 = new Dialog(con);
		dd2.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dd2.setContentView(R.layout.dialogue_fb);
		dd2.show();

		TextView dialogeFbCancel = (TextView) dd2.findViewById(R.id.dialogeFbCancel);
		TextView fbPost = (TextView) dd2.findViewById(R.id.fbPost);
		TextView title = (TextView) dd2.findViewById(R.id.title);
		final EditText fbPostTxt = (EditText) dd2.findViewById(R.id.fbPostTxt);
		title.setText(shareTitle);
		fbPostTxt.setText(searchTxt);
		fbPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isShare = true;
				shareText = fbPostTxt.getText().toString();
				if (shareTitle.equalsIgnoreCase("Facebook")) {
					Session session = Session.getActiveSession();
					if (!session.isOpened() && !session.isClosed()) {
						session.openForRead(new Session.OpenRequest(
								TestActivity.this).setPermissions(
								Arrays.asList("email")).setCallback(callback));
					} else {
						Session.openActiveSession(TestActivity.this, true,
								callback);
					}
				} else {
					if (Twitter.getSessionManager().getActiveSession() != null) {
						final BusyDialog busyDialog= new BusyDialog(con,false,false);
						TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient(Twitter.getSessionManager().getActiveSession());
						StatusesService statusesService = twitterApiClient.getStatusesService();
						statusesService.update(shareText, PersistData.getLongData(con, AppConstant.twitterId),
								null, null, null, null, null, null, new Callback<Tweet>() {
									@Override
									public void success(Result<Tweet> result) {
										// Do something with result, which
										// provides a Tweet inside of
										// result.l
										if (busyDialog != null) {
											busyDialog.dismis();
										}
										if (dd2 != null) {
											dd2.dismiss();
										}

										Log.e("result", "Success");
										Toast.makeText(con, "Twitter Post Successful!", Toast.LENGTH_SHORT).show();

									}

									@Override
									public void failure(
											TwitterException exception) {
										// Do something on failure
										if (busyDialog != null) {
											busyDialog.dismis();
										}
										Log.e("result", "Failed");
										Toast.makeText(con, "Twitter Post Failed", Toast.LENGTH_SHORT).show();
									}
								});
					} else {
						twitter_button.performClick();
					}
				}


			}
		});

		dialogeFbCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dd2.dismiss();
			}
		});

	}



	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
						 Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private void onSessionStateChange(final Session session,
									  SessionState state, Exception exception) {
		if (state.isOpened()) {

			publishStoryByFb();

		} else if (state.isClosed()) {
			Log.e("Status", "Logged out...");
		}
	}
	String addFavoritesResponse="";
	private void addFavoritesInfo(final String url) {
		// TODO Auto-generated method stub
		Log.e("url", ">>" + url);
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					addFavoritesResponse = HTTPHandler.GetText(HTTPHandler
							.getInputStreamGET(url));

					Log.e("response", ">>" + addFavoritesResponse);


				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (addFavoritesResponse.trim().equalsIgnoreCase("1")) {
							db.updateFavorites(AppConstant.channelInfo.getWrid());
							videoFavorites.setImageResource(R.drawable.star_fill);
							Toast.makeText(con, "Channel added successfully", Toast.LENGTH_LONG).show();

						} else if (addFavoritesResponse.trim().equalsIgnoreCase("2")) {

							Toast.makeText(con, "Channel  already a Favorite. Did not add it again", Toast.LENGTH_LONG).show();
						} else if (addFavoritesResponse.trim().equalsIgnoreCase("3")) {

							Toast.makeText(con, "10-Channel limit reached. Could not add more. ", Toast.LENGTH_LONG).show();
						}

					}

				});
			}
		});

		thread.start();

	}

	private void publishStoryByFb() {
		Session session = Session.getActiveSession();

		if (session != null) {

			// Check for publish permissions
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(AppConstant.PERMISSIONS, permissions)) {
				Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
						this, AppConstant.PERMISSIONS);
				session.requestNewPublishPermissions(newPermissionsRequest);
				isShare = true;
				return;
			}

			isShare = false;

			Bundle postParams = new Bundle();
			// postParams.putString("message", "Checkout this business: " +
			// businessUrl);
			postParams.putString("message", shareText);

			busy = new BusyDialog(con, false, "Sharing listing...");
			busy.show();

			Request.Callback callback = new Request.Callback() {
				@Override
				public void onCompleted(Response response) {
					if (busy != null) {
						busy.dismis();
					}
					GraphObject graphObj = response.getGraphObject();
					if (graphObj == null) {
						// Toast.makeText(con, "Already posted",
						// Toast.LENGTH_SHORT).show();
						FacebookRequestError error = response.getError();
						if (error != null) {
							Toast.makeText(con.getApplicationContext(), error.getErrorMessage(), Toast.LENGTH_SHORT).show();
						}
					} else {
						JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
						String postId = null;
						try {
							postId = graphResponse.getString("id");
							if (postId != null && postId.length() > 0) {
								Toast.makeText(con, "Successfully posted on facebook", Toast.LENGTH_SHORT).show();
								try {
									dd2.dismiss();
								} catch (Exception ex) {
								}
								;
							}
						} catch (JSONException e) {
							Log.i("Status", "JSON error " + e.getMessage());
						}
						FacebookRequestError error = response.getError();
						if (error != null) {
							Toast.makeText(con.getApplicationContext(),
									error.getErrorMessage(), Toast.LENGTH_SHORT)
									.show();
						} else {
							// Toast.makeText(con.getApplicationContext(),
							// postId, Toast.LENGTH_LONG).show();
						}
					}
				}
			};

			Request request = new Request(session, "me/feed", postParams,
					HttpMethod.POST, callback);

			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
		}

	}

	private boolean isSubsetOf(Collection<String> subset,
							   Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	String removeFavoritesResponse="";
	private void removeFavoritesInfo(final String url) {
		// TODO Auto-generated method stub
		Log.e("url", ">>" + url);
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, con.getString(R.string.app_name),
					con.getString(R.string.checkInternet));
			return;
		}

		busy = new BusyDialog(con, true, false);
		busy.show();

		final Thread thread = new Thread(new Runnable() {

			/*
			 * re(non-Javadoc)
			 *
			 * @see java.lang.Runnable#run()
			 */

			@Override
			public void run() {
				// TODO Auto-generated method stub

				/*
				 * remove all old data's
				 */

				try {
					removeFavoritesResponse = HTTPHandler.GetText(HTTPHandler.getInputStreamGET(url));

					Log.e("response", ">>" + removeFavoritesResponse);


				} catch (final ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (busy != null) {
							busy.dismis();
						}

						if (removeFavoritesResponse.trim().equalsIgnoreCase("1")) {
							db.removeFavorites(AppConstant.channelInfo.getWrid());
							videoFavorites.setImageResource(R.drawable.star_fillas);
							Toast.makeText(con, "Channel removed  successfully",
									Toast.LENGTH_LONG).show();

						} else if (removeFavoritesResponse.trim().equalsIgnoreCase("0")) {

							Toast.makeText(con, "Channel not removed (not on the list of Favorites).", Toast.LENGTH_LONG).show();
						}

					}

				});
			}
		});

		thread.start();

	}

	private void createVideoView()
	{
		video_parent.removeAllViews();
		video = new VideoView(this);
		video.setFocusable(false);
		video.setFocusableInTouchMode(false);
		//video.setOnTouchListener(this);
		// Do this for each view added to the grid
		video.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//video.setOnTouchListener(gestureListener);
		video_parent.addView(video);
		video.setVideoURI(Uri.parse(AppConstant.channelInfo.getVisual_content_location()));
		video.start();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
